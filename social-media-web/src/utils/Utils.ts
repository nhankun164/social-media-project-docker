import queryString from 'query-string';

export const isNullOrUndefined = (value: any) => {
    return (value === null || value === undefined)
}

export const paramsToQueryString = (params: any) => {
    const query = queryString.stringify(params, { arrayFormat: 'comma', skipNull: true })
    return query;
};