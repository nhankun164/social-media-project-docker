import React from 'react';
import { Table, Image, Card } from 'antd';
import { IComment, IRssTable } from '../../../redux/store/main/main.types';
import moment from 'moment';
import Highlighter from "react-highlight-words";


interface ITableByTopicProps {
    data: any;
    selectedFieldName: string;
    selectedTopic: string;
    // loading: boolean;
}



interface IKeyExtendComment extends IComment {
    key: number;
}

const TableByTopic = (props: ITableByTopicProps) => {
    const [dataTable, setDataTable] = React.useState<IKeyExtendComment[]>([]);
    const [columns, setColumns] = React.useState<any>([]);

    React.useEffect(() => {
        const columns = [
            {
              title: 'Created Date',
              dataIndex: 'create_date_unix',
              width: 150,
              key: 'create_date_unix',
              render: (time: number) => <span>{moment(time).format('YYYY-MM-DD h:mm:ss')}</span>
            },
            // {
            //   title: 'Title',
            //   dataIndex: 'title',
            //   key: 'title',
            // //   width: 150
            // },
            {
                title: 'Content',
                dataIndex:  props.selectedFieldName,
                key: props.selectedFieldName,
            //   render: (text: string) => <a href={text} target="_blank" rel='noreferrer'>{text}</a>
                render: (text: string, record: any) => {
                    return (
                        <Highlighter
                            searchWords={props.selectedTopic.split(' ')}
                            autoEscape={true}
                            textToHighlight={text}
                        />
                    )
                }
            //   width: 200
            },
        ];
        setColumns(columns);
    }, [props.selectedFieldName, props.selectedTopic])
    

    React.useEffect(() => {
            const dataTable: IKeyExtendComment[] = props.data.map((element: any) => {
                return {
                    ...element,
                    key: element.id
                }
            })
            setDataTable(dataTable);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.data])

    return (
        <Table 
            className='rss-table' 
            // loading={props.loading} 
            bordered 
            dataSource={dataTable} 
            columns={columns} 
            size="small" scroll={{ x: 1000, y: 400 }}/>
    );
}

export default TableByTopic;