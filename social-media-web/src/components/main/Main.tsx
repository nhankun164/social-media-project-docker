import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { AppState } from '../../redux/store';
import { FETCH_RSS_BY_DATETIME_FAILED, FETCH_RSS_BY_DATETIME_SUCCESS, FETCH_TRENDS_FAILED, FETCH_TRENDS_SUCCESS, FETCH_TWITTER_FAILED, FETCH_TWITTER_SUCCESS, IMainState, IPeriod, IRssTable } from '../../redux/store/main/main.types';
import { thunkSetErrorMessage, thunkSetShowMessage } from '../../redux/store/messages/messages.thunks';
import { thunkFetchTwitterData, 
  thunkFetchRssDataByTime, 
  thunkFetchCommentData, 
  thunkFetchTrendsData, 
  thunkFetchTwitterDataByFilterDate,
  thunkFetchCommentDataByFilterDate, 
  thunkFetchTrendsDataByFilterDate,
  thunkSetAllDisableMovingValue,
  thunkSetAutoMovingValue } from '../../redux/store/main/main.thunks';
import { Layout, Row, Col, Avatar, Button, Input, DatePicker, notification, Spin, Tabs, Select, Radio, RadioChangeEvent, Tag } from 'antd';
import {
  BellFilled,
  SearchOutlined,
  DotChartOutlined
} from '@ant-design/icons';
import './Main.css';
import SearchBuilderModal from './search-builder-modal/SearchBuilderModal';
import { RuleGroupType } from 'react-querybuilder';
import TwitterChart from './twitter-chart/TwitterChart';
import FrequencyRssTable from './frequency-rss-table/FrequencyRssTable';
import { ChartNameEnum, PeriodTabs } from '../../constants/enum';
import TrendsChartWrapper from './trends-chart-wrapper/TrendsChartWrapper';
import CommentsChart from './comment-chart/CommentsChart';
import { thunkExecuteSearch, thunkExecuteSearchByTopic } from '../../redux/store/search/search.thunks';
import { EXECUTE_SEARCH_BY_TOPIC_FAILED, EXECUTE_SEARCH_BY_TOPIC_SUCCESS, EXECUTE_SEARCH_FAILED, EXECUTE_SEARCH_SUCCESS, ISearchObject, ISearchState } from '../../redux/store/search/search.types';
import TopicChartModal from '../topic-chart-modal/TopicChartModal';
import TableByTopic from './table-by-topic/TableByTopic';
import OverallEmotionPieChart from './sub-charts/OverallEmotionPieChart';
import EmotionCountBarChart from './sub-charts/EmotionCountBarChart';
import WordClouds from './sub-charts/WordClouds';



const { Header, Content } = Layout;
const { TabPane } = Tabs;

const { RangePicker } = DatePicker;
const { Option } = Select;


interface IFieldObj {
  name: string;
  label: string;
}

const commentFields: IFieldObj[] = [
  {
    name: 'content',
    label: 'Contents'
  }
]

const postFields: IFieldObj[] = [
  {
    name: 'description',
    label: 'Description'
  },
  {
    name: 'title',
    label: 'Title'
  }
]

interface IMainProps extends RouteComponentProps {
  main: IMainState;
  search: ISearchState;
  thunkFetchTwitterData: any;
  thunkFetchRssDataByTime: any;
  thunkFetchCommentData: any;
  thunkFetchTrendsData: any;
  thunkFetchCommentDataByFilterDate: any;
  thunkExecuteSearchByTopic: any;
  thunkFetchTrendsDataByFilterDate: any;
  thunkFetchTwitterDataByFilterDate: any;
  thunkSetAllDisableMovingValue: any;
  thunkSetAutoMovingValue: any;
  thunkExecuteSearch: any;
}

const MainComponent: React.FC<IMainProps> = (props: IMainProps) => {
  const [openSearchModal, setOpenSearchModal] = React.useState(false);
  const [queryObject, setQueryObject] = React.useState<RuleGroupType>();
  const [queryString, setQueryString] = React.useState<string>('()');
  // const [twitterList, setTwitterList] = React.useState<ITwitter[]>([]);
  const [rssDataSource, setRssDataSource] = React.useState<IRssTable[]>([]);
  const [loadingChart, setLoadingChart] = React.useState(false);
  const [fieldName, setFieldName] = React.useState<string>('content');
  const [fieldList, setFieldList] = React.useState<IFieldObj[]>(commentFields);
  const [openTopicChartModal, setOpenTopicChartModal] = React.useState(false);
  const [searchLoading, setSearchLoading] = React.useState(false);
  const [isShowRssTable, setIsShowRssTable] = React.useState(true);
  // const [loadingTable, setLoadingTable] = React.useState(false);
  const [tab, setTab] = React.useState<string>(PeriodTabs.History);
  const [chartName, setChartName] = React.useState<ChartNameEnum>(ChartNameEnum.Emotion);
  const [tableSearchName, setTableSearchName] = React.useState<string>('Comment');
  const [autoMovingRadioValue, setAutoMovingRadioValue] = React.useState<number>(1);
  const [selectedTopic, setSelectedTopic] = React.useState<string>('');
  const [period, setPeriod] = React.useState<IPeriod>({
    startDate: null,
    endDate: null
  });



  // React.useEffect(() => {
    
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [])

  const onOpenSearchBuilderModal = () => {
    setOpenSearchModal(true);
  }

  const onCloseSearchModal = () => {
    setOpenSearchModal(false);
  }

  const onOpenTopicChartModal = () => {
    setOpenTopicChartModal(true);
  }

  const onCloseTopicChartModal = () => {
    setOpenTopicChartModal(false);
  }

  const handleSaveSearchQuery = (queryObject: RuleGroupType | undefined, queryString: string) => {
    setQueryObject(queryObject);
    setQueryString(queryString);
    onCloseSearchModal();
  }

  const onTimeRangeChange = (value: any, dateString: [string, string]) => {
    console.log('ssdasdas-------- ', value);
    console.log('date -------- ', dateString)
    setPeriod({
      startDate: value[0].unix(),
      endDate: value[1].unix()
    })
  }

  const getRssByTime = (selectedDate: number) => {
      // setLoadingTable(true);
      props.thunkFetchRssDataByTime(
        selectedDate
      ).then((response: any) => {
        switch (response.type) {
          case FETCH_RSS_BY_DATETIME_FAILED:
              // props.thunkSetShowMessage(true);
              // props.thunkSetErrorMessage(response.payload);   
              notification.error({
                message: 'Error Fetch',
                description: response.payload
              }); 
              break;
          case FETCH_RSS_BY_DATETIME_SUCCESS:
              setRssDataSource(response.payload);
              // if (response.payload.length === 0) {
              //   notification.warning({
              //     message: 'Rss Data',
              //     description: 'Data Not Found'
              //   })
              // } 
              break;
        }
        // setLoadingTable(false);
      })
    
  }

  const handleChangeTab = (newTab: string) => {
    setTab(newTab);
  }

  

  React.useEffect(() => {
    setLoadingChart(true);
    console.log('1');
    if (chartName === ChartNameEnum.Emotion) {
      fetchTrendsData();
    }
    else if (chartName === ChartNameEnum.Sentiment) {
      fetchTrendsData();
    } 
    else if (chartName === ChartNameEnum.Comment) {
      fetchCommentsData();
    }
    else {
      fetchTwitterData();
    }
    setPeriod({
      startDate: null,
      endDate: null
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tab])

  React.useEffect(() => {
    if (period.startDate && period.endDate) {
      setLoadingChart(true);
      if (chartName === ChartNameEnum.Emotion) {
        fetchTrendsDataByFilterDate(period);
      }
      else if (chartName === ChartNameEnum.Sentiment) {
        fetchTrendsDataByFilterDate(period);
      } 
      else if (chartName === ChartNameEnum.Comment) {
        fetchCommentsDataByFilterDate(period);
      }
      else {
        fetchTwitterDataByFilterDate(period);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [period])

  const fetchTrendsData = () => {
    props.thunkFetchTrendsData({
      periodType: tab
    }).then((response: any) => {
      switch (response.type) {
        case FETCH_TRENDS_FAILED:
            // props.thunkSetShowMessage(true);
            // props.thunkSetErrorMessage(response.payload);   
            notification.error({
              message: 'Error Fetch',
              description: response.payload
            }); 
            break;
        case FETCH_TRENDS_SUCCESS:
            // setTwitterList(response.payload);
            break;
            
      }
      setLoadingChart(false);
    })
  }

  const fetchTrendsDataByFilterDate = (period: IPeriod) => {
    props.thunkFetchTrendsDataByFilterDate(period).then((response: any) => {
      switch (response.type) {
        case FETCH_TRENDS_FAILED:
            // props.thunkSetShowMessage(true);
            // props.thunkSetErrorMessage(response.payload);   
            notification.error({
              message: 'Error Fetch',
              description: response.payload
            }); 
            break;
        case FETCH_TRENDS_SUCCESS:
            // setTwitterList(response.payload);
            break;
            
      }
      setLoadingChart(false);
    })
  }

  const fetchCommentsData = () => {
    props.thunkFetchCommentData({
      periodType: tab
    }).then((response: any) => {
      switch (response.type) {
        case FETCH_TRENDS_FAILED:
            // props.thunkSetShowMessage(true);
            // props.thunkSetErrorMessage(response.payload);   
            notification.error({
              message: 'Error Fetch',
              description: response.payload
            }); 
            break;
        case FETCH_TRENDS_SUCCESS:
            // setTwitterList(response.payload);
            break;
            
      }
      setLoadingChart(false);
    })
  }
  

  const fetchCommentsDataByFilterDate = (period: IPeriod) => {
    props.thunkFetchCommentDataByFilterDate(period).then((response: any) => {
      switch (response.type) {
        case FETCH_TRENDS_FAILED:
            // props.thunkSetShowMessage(true);
            // props.thunkSetErrorMessage(response.payload);   
            notification.error({
              message: 'Error Fetch',
              description: response.payload
            }); 
            break;
        case FETCH_TRENDS_SUCCESS:
            // setTwitterList(response.payload);
            break;
            
      }
      setLoadingChart(false);
    })
  }

  const fetchTwitterData = () => {
    props.thunkFetchTwitterData({
      periodType: tab
    }).then((response: any) => {
      switch (response.type) {
        case FETCH_TWITTER_FAILED:
            // props.thunkSetShowMessage(true);
            // props.thunkSetErrorMessage(response.payload);   
            notification.error({
              message: 'Error Fetch',
              description: response.payload
            }); 
            break;
        case FETCH_TWITTER_SUCCESS:
            // setTwitterList(response.payload);
            break;
            
      }
      setLoadingChart(false);
    })
  }

  const fetchTwitterDataByFilterDate = (period: IPeriod) => {
    props.thunkFetchTwitterDataByFilterDate(period).then((response: any) => {
      switch (response.type) {
        case FETCH_TWITTER_FAILED:
            // props.thunkSetShowMessage(true);
            // props.thunkSetErrorMessage(response.payload);   
            notification.error({
              message: 'Error Fetch',
              description: response.payload
            }); 
            break;
        case FETCH_TWITTER_SUCCESS:
            // setTwitterList(response.payload);
            break;
            
      }
      setLoadingChart(false);
    })
  }

  const handleChangeTableSearchName = (newValue: string) => {
    setTableSearchName(newValue);
    if (newValue === 'Comment') {
      setFieldList(commentFields);
      setFieldName(commentFields[0].name);
    }
    if (newValue === 'Post') {
      setFieldList(postFields);
      setFieldName(postFields[0].name);
    }
    setQueryObject(undefined);
    setQueryString('');
  }

  const handleChangeChartName = (newValue: ChartNameEnum) => {
    setChartName(newValue);
    setLoadingChart(true);
    if (period.startDate === null && period.endDate === null) {
      if (newValue === ChartNameEnum.Emotion) {
        setAutoMovingRadioValue(props.main.autoMovingRadio.emotion);
        if (props.main.trends.emotion.length === 0) {
          fetchTrendsData();
        }
      }
      else if (newValue === ChartNameEnum.Sentiment) {
        setAutoMovingRadioValue(props.main.autoMovingRadio.sentiment);
        if (props.main.trends.sentiment.length === 0) {
          fetchTrendsData();
        }
      } 
      else if (newValue === ChartNameEnum.Comment) {
        setAutoMovingRadioValue(props.main.autoMovingRadio.comment);
        if (props.main.commentList.data.length === 0) {
          fetchCommentsData();
        }
      }
      else {
        setAutoMovingRadioValue(props.main.autoMovingRadio.twitter);
        if (props.main.twitterList.length === 0) {
          fetchTwitterData();
        }
      }
    } 
    else {
      if (newValue === ChartNameEnum.Emotion) {
        fetchTrendsDataByFilterDate(period);
      }
      else if (newValue === ChartNameEnum.Sentiment) {
        fetchTrendsDataByFilterDate(period);
      } 
      else if (newValue === ChartNameEnum.Comment) {
        fetchCommentsDataByFilterDate(period);
      }
      else {
        fetchTwitterDataByFilterDate(period);
      }
    }

    setLoadingChart(false);

    
  }

  const onChangeRadioAutoMoving = (e: RadioChangeEvent) => {
    setAutoMovingRadioValue(Number(e.target.value));
    if (Number(e.target.value) === 1) {
      setIsShowRssTable(true);
    }
    props.thunkSetAutoMovingValue({
      type: chartName,
      value: Number(e.target.value)
    })
  }


  const handleChangeFieldName = (newValue: string) => {
    setFieldName(newValue);
  }

  const executeSearch = () => {
    const data: ISearchObject = {
      table: tableSearchName,
      field: fieldName,
      statement: queryString === "()" || queryString === "" ? "()" : queryString,
      startDate: period.startDate ? period.startDate : "null",
      endDate: period.endDate ? period.endDate : "null"
    } 
    setSearchLoading(true);
    props.thunkExecuteSearch(data).then((response: any) => {
      switch (response.type) {
        case EXECUTE_SEARCH_FAILED:
            // props.thunkSetShowMessage(true);
            // props.thunkSetErrorMessage(response.payload);   
            notification.error({
              message: 'Error Fetch',
              description: response.payload
            }); 
            break;
        case EXECUTE_SEARCH_SUCCESS:
          if (response.payload && response.payload.series && response.payload.series.length === 0) {
            notification.info({
              message: 'Search Result',
              description: 'Data Not Found'
            }); 
          }
            // setTwitterList(response.payload);
            break;
        
      }
      setSearchLoading(false);
    })
  }

  const executeSearchByKeyword = (keyword: string) => {
    const data: ISearchObject = {
      table: tableSearchName,
      field: fieldName,
      statement: queryString === "()" || queryString === "" ? "()" : queryString,
      keyword: keyword,
      startDate: period.startDate ? period.startDate : "null",
      endDate: period.endDate ? period.endDate : "null"
    } 
    props.thunkExecuteSearchByTopic(data).then((response: any) => {
      switch (response.type) {
        case EXECUTE_SEARCH_BY_TOPIC_FAILED:
            // props.thunkSetShowMessage(true);
            // props.thunkSetErrorMessage(response.payload);   
            notification.error({
              message: 'Error Fetch',
              description: response.payload
            }); 
            break;
        case EXECUTE_SEARCH_BY_TOPIC_SUCCESS:
          if (response.payload && response.payload.series && response.payload.series.length === 0) {
            notification.info({
              message: 'Search Result',
              description: 'Data Not Found'
            }); 
          }
            // setTwitterList(response.payload);
            break;
        
      }
      setSelectedTopic(keyword);
      setIsShowRssTable(false);
      setAutoMovingRadioValue(0);
      props.thunkSetAllDisableMovingValue();
    })
  }
  const style = { background: '#0092ff', padding: '8px 0' };
  
  const generateMainContent = (
    <div>

    
      <Content style={{ marginTop: 24 }}>
        <Header className="site-layout-background" style={{ borderBottom: '1px solid rgba(0, 0, 0, 0.3)'}}>
          <Col span={24}>
              <Row gutter={16} align='middle'>
                <Col span={3}>
                  <Select value={tableSearchName} onChange={handleChangeTableSearchName} style={{ width: '90%'}}>
                    <Option value={'Comment'}>Comment</Option>
                    <Option value={'Post'}>Post</Option>
                    
                  </Select>
                </Col>
                <Col span={13}>
                  <div className='middle-container'>
                    <Input addonBefore={<SearchOutlined />} value={queryString} onClick={onOpenSearchBuilderModal} style={{ width: '90%'}} />
                  </div>
                </Col>
                <Col span={6}>
                  <RangePicker
                    format={'MMM DD, YYYY'}
                    onChange={onTimeRangeChange}
                  />
                  
                </Col>
                <Col span={2}>
                  <Button type="primary" onClick={executeSearch} loading={searchLoading}>
                    Search
                  </Button>
                </Col>
              </Row>

              <Row gutter={16} align='middle'>
                <Col span={3}>

                </Col>
                <Col span={13}>
                  <span>
                    Topics From:
                  </span>
                  <Select value={fieldName} onChange={handleChangeFieldName} style={{ width: 200, marginLeft: 20, marginRight: 30 }}>
                    {
                      fieldList.map((field, index) => (
                        <Option key={index} value={field.name}>{field.label}</Option>
                      ))
                    }
                  </Select>
                  {/* {
                    props.search && props.search.data && props.search.data.series && props.search.data.series.length > 0
                    && <Button type="default" icon={<DotChartOutlined />} onClick={onOpenTopicChartModal}>
                          Open Topic Chart
                        </Button>
                  } */}
                  
                </Col>
              </Row>
            </Col>
        </Header>
        {
          props.search.data.topic_labeling
          && 
          <Row>
            <Col className="gutter-row" span={16} style={{ background: '#fff', padding: 16, marginLeft: 16 }}>
              <Row>
                <Col span={3}>
                  Topics:
                </Col>
                <Col span={21}>
                  {
                    props.search.data.topic_labeling.map((topic: any, index: number) => {
                      return <Button key={index} style={{ marginRight: 8, marginBottom: 12}} shape="round" size="small" onClick={() => executeSearchByKeyword(topic.label)}>
                        {topic.label}
                      </Button>
                      // return <Tag key={index} color={'blue'} onClick={() => executeSearchByKeyword(topic.label)}>{topic.label}</Tag>
                    })
                  }
                </Col>
              </Row>
            </Col>
          </Row>
        }
        
        <Row justify='center'>
          <Col className="gutter-row" span={16} style={{ background: '#fff', padding: 16}}>
            <div>
              <Row gutter={16} align='middle'>
                <Col span={12}>
                  <Select value={chartName} onChange={handleChangeChartName} style={{ width: 300 }}>
                    <Option value={ChartNameEnum.Emotion}>Emotion</Option>
                    <Option value={ChartNameEnum.Sentiment}>Sentiment</Option>
                    <Option value={ChartNameEnum.Comment}>Comment</Option>
                    <Option value={ChartNameEnum.Twitter}>Twitter</Option>
                  </Select>
                </Col>
                <Col span={12}>
                  <Radio.Group onChange={onChangeRadioAutoMoving} value={autoMovingRadioValue}>
                    <Radio value={1}>Enable Auto Moving Plot Line</Radio>
                    <Radio value={0}>Disable Auto Moving Plot Line</Radio>
                  </Radio.Group>
                </Col>
              </Row> 
              
            </div>
            <Spin spinning={loadingChart}>
              {
                chartName === ChartNameEnum.Comment
                ? <CommentsChart 
                  data={props.main.commentList.data}
                  isAutoMovingPlotLine={props.main.autoMovingRadio.comment}
                  getRssByTime={getRssByTime}
                />
                : chartName === ChartNameEnum.Twitter
                  ? <TwitterChart 
                    data={props.main.twitterList}
                    isAutoMovingPlotLine={props.main.autoMovingRadio.twitter}
                    getRssByTime={getRssByTime}
                  />
                  : <TrendsChartWrapper 
                    type={chartName}
                    data={chartName === ChartNameEnum.Emotion ? props.main.trends.emotion : props.main.trends.sentiment}
                    isAutoMovingPlotLine={chartName === ChartNameEnum.Emotion ? props.main.autoMovingRadio.emotion : props.main.autoMovingRadio.sentiment}
                    getRssByTime={getRssByTime}
                  />
              }
              
            </Spin>
            
          </Col>
          <Col className="gutter-row" span={7} style={{ background: '#fff', padding: 16, marginLeft: 16 }}>
            {
              isShowRssTable
              ?  <FrequencyRssTable 
                    data={rssDataSource} 
                    // loading={loadingTable}
              />
              : <TableByTopic 
                  data={props.search.contentList}
                  selectedFieldName={fieldName}
                  selectedTopic={selectedTopic}
              />
            }
          </Col>
        </Row>
        
        
      </Content>
      
  </div>
);


  const trendsSubChartContent = (
    <div>
      <Row gutter={[30, 30]}>
        <Col className="gutter-row" span={12}>
          <div className="sub-container">
            <h2>Overall Emotions</h2>
            <OverallEmotionPieChart 
              data={props.main.trends.emotion_count}
              type={ChartNameEnum.Emotion}
            />
          </div>
        </Col>
        <Col className="gutter-row" span={12}>
          <div className="sub-container">
            <h2>Overall Sentiments</h2>
            <OverallEmotionPieChart 
              data={props.main.trends.sentiment_count}
              type={ChartNameEnum.Sentiment}
            />
          </div>
        </Col>
        <Col className="gutter-row" span={12}>
          <div className="sub-container">
            <h2>Comparison between Emotions</h2>
            <EmotionCountBarChart 
              data={props.main.trends.emotion_count}
              type={ChartNameEnum.Emotion}
            />
          </div>
        </Col>
        <Col className="gutter-row" span={12}>
          <div className="sub-container">
            <h2>Comparison between Sentiments</h2>
            <EmotionCountBarChart 
              data={props.main.trends.sentiment_count}
              type={ChartNameEnum.Sentiment}
            />
          </div>
        </Col>
      </Row>
    </div>
  );

  const commentsSubChartContent = (
    <div>
      <Row gutter={[30, 30]}>
        <Col className="gutter-row" span={12}>
          <div className="sub-container">
            <h2>Overall Emotions</h2>
            <OverallEmotionPieChart 
              data={props.main.commentList.emotion_count}
              type={ChartNameEnum.Emotion}
            />
          </div>
        </Col>
        <Col className="gutter-row" span={12}>
          <div className="sub-container">
            <h2>Overall Sentiments</h2>
            <OverallEmotionPieChart 
              data={props.main.commentList.sentiment_count}
              type={ChartNameEnum.Sentiment}
            />
          </div>
        </Col>
        <Col className="gutter-row" span={12}>
          <div className="sub-container">
            <h2>Comparison between Emotions</h2>
            <EmotionCountBarChart 
              data={props.main.commentList.emotion_count}
              type={ChartNameEnum.Emotion}
            />
          </div>
        </Col>
        <Col className="gutter-row" span={12}>
          <div className="sub-container">
            <h2>Comparison between Sentiments</h2>
            <EmotionCountBarChart 
              data={props.main.commentList.sentiment_count}
              type={ChartNameEnum.Sentiment}
            />
          </div>
        </Col>
        <Col className="gutter-row" span={12}>
          <div className="sub-container">
            <h2>Popular Positive Words</h2>
            <WordClouds 
              data={props.main.commentList.data.map(comment => comment.pos_words)}
            />
          </div>
        </Col>
        <Col className="gutter-row" span={12}>
          <div className="sub-container">
            <h2>Popular Negative Words</h2>
            <WordClouds 
              data={props.main.commentList.data.map(comment => comment.neg_words)}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
  

  return (
    <div>
      <Layout className='site-layout'>
      <Header className="site-layout-background">
        <Row align='middle' justify='space-around'>
          <Col span={24}>
            <Row gutter={16} align='middle'>
              <Col span={3}>
                <div className='logo-container'>
                  <img src='/images/logo.png' alt='logo' width={30} />
                  <span style={{ marginLeft: 10 }}> Logo </span>
                </div>
                
              </Col>
              <Col span={19} style={{ borderLeft: '1px solid rgba(0, 0, 0, 0.2)'}} >
                <div className='middle-container'>
                  Homepage
                </div>
              </Col>
              <Col span={2}>
                <div className='account-container'>
                    <Button shape="circle" icon={<BellFilled />} style={{ marginRight: 10 }}/>
                    <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                </div>
                
              </Col>
            </Row>
          </Col>
        </Row>
      </Header>
      <Row className="card-container">
        <Col className="gutter-row" span={24} style={{ background: '#fff', padding: 16}}>
          <Tabs defaultActiveKey="1" activeKey={tab} type="card" size='large' onChange={handleChangeTab}>
            <TabPane tab={PeriodTabs.History} key={PeriodTabs.History}>
              {
                tab === PeriodTabs.History
                && 
                generateMainContent
              }
            </TabPane>
            <TabPane tab={PeriodTabs.Current} key={PeriodTabs.Current}>
              {
                tab === PeriodTabs.Current
                && 
                generateMainContent
              }
            </TabPane>
            <TabPane tab={PeriodTabs.Prediction} key={PeriodTabs.Prediction} disabled>

            </TabPane>
          </Tabs>
        </Col>
      
      </Row>
      
          
      

      <TopicChartModal 
        open={openTopicChartModal}
        dataSeries={props.search.data}
        onClose={onCloseTopicChartModal}
      />
      <SearchBuilderModal 
        open={openSearchModal}
        tableName={tableSearchName}
        query={queryObject}
        onClose={onCloseSearchModal}
        handleSaveSearchQuery={handleSaveSearchQuery}
      />
    </Layout>
    
    <Layout>
      <Content style={{ marginTop: 30, padding: '0 16px', background: '#f0f2f5'}}>
        {
          chartName === ChartNameEnum.Comment
          ? commentsSubChartContent
          : (chartName === ChartNameEnum.Emotion || chartName === ChartNameEnum.Sentiment)
              ? trendsSubChartContent
              : null
        }
      </Content>
    </Layout>
    </div>
    
  );
};


const mapStateToProps = (state: AppState) => ({
  main: state.main,
  search: state.search,
  messages: state.messages
});

export default connect(
  mapStateToProps, {
    thunkSetShowMessage,
    thunkSetErrorMessage,
    thunkFetchTwitterData,
    thunkFetchRssDataByTime,
    thunkFetchCommentData,
    thunkFetchTrendsData,
    thunkFetchCommentDataByFilterDate,
    thunkFetchTrendsDataByFilterDate,
    thunkFetchTwitterDataByFilterDate,
    thunkSetAutoMovingValue,
    thunkSetAllDisableMovingValue,
    thunkExecuteSearch,
    thunkExecuteSearchByTopic
}
)(MainComponent);
