import React from 'react';
import { Modal } from 'antd';
import './SearchBuilderModal.css';
import QueryBuilder, { Classnames, Controls, Field, formatQuery, RuleGroupType } from 'react-querybuilder';
import { AntDActionElement, AntDNotToggle, AntDValueEditor, AntDValueSelector } from '../../../styles/SearchBuilderCustomTheme';


const commentFields: Field[] = [
  { name: 'content', label: 'content' },
  // { name: 'lastName', label: 'Last Name' },
  // { name: 'age', label: 'Age' },
  // { name: 'address', label: 'Address' },
  // { name: 'phone', label: 'Phone' },
  // { name: 'email', label: 'Email' },
  // { name: 'twitter', label: 'Twitter' },
  // { name: 'isDev', 
  //   label: 'Is a Developer?', 
  //   operators: [{ name: '=', label: 'is' }], 
  //   valueEditorType: 'checkbox', 
  //   defaultValue: false }
];

const postFields: Field[] = [
  { name: 'description', label: 'description'},
  { name: 'title', label: 'title'}
]

type StyleName = 'default' | 'bootstrap' | 'antd' | 'material';

const controlClassnames: { [k in StyleName]: Partial<Classnames> } = {
  default: {},
  bootstrap: {
    addGroup: 'btn btn-secondary btn-sm',
    addRule: 'btn btn-primary btn-sm',
    removeGroup: 'btn btn-danger btn-sm',
    removeRule: 'btn btn-danger btn-sm',
    combinators: 'form-control form-control-sm',
    fields: 'form-control form-control-sm',
    operators: 'form-control form-control-sm',
    value: 'form-control form-control-sm'
  },
  antd: {},
  material: {}
};

const controlElements: { [k in StyleName]: Partial<Controls> } = {
  default: {},
  bootstrap: {},
  antd: {
    addGroupAction: AntDActionElement,
    addRuleAction: AntDActionElement,
    combinatorSelector: AntDValueSelector,
    fieldSelector: AntDValueSelector,
    notToggle: AntDNotToggle,
    operatorSelector: AntDValueSelector,
    removeGroupAction: AntDActionElement,
    removeRuleAction: AntDActionElement,
    valueEditor: AntDValueEditor
  },
  material: {}
};



interface ISearchBuilderModalProps {
    open: boolean;
    tableName: string;
    query: RuleGroupType | undefined;
    onClose: () => void;
    handleSaveSearchQuery: (queryObject: RuleGroupType | undefined, queryString: string) => void;
}


const SearchBuilderModal = (props: ISearchBuilderModalProps) => {
    // const [format, setFormat] = React.useState<ExportFormat>('json');
    const [queryObject, setQueryObject] = React.useState<RuleGroupType>();
    const [queryString, setQueryString] = React.useState<string>('');

    React.useEffect(() => {
      setQueryObject(props.query);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.query])


    const logQuery = (query: RuleGroupType) => {
      setQueryObject(query)
    }

    const handleClose = () => {
      props.onClose();
    }

    React.useEffect(() => {
      if (queryObject) {
        const string = formatQuery(queryObject, 'sql');
        setQueryString(string as string);
      }
      else {
        setQueryString('()')
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [queryObject])

    const handleSaveSearchQuery = () => {
      props.handleSaveSearchQuery(queryObject, queryString);
    }

    return (
        <Modal
          title='Search builder'
          centered
          width={700}
          visible={props.open}
          okText='Save'
          cancelButtonProps={undefined}
          onOk={handleSaveSearchQuery}
          onCancel={handleClose}
        >
          <div className='with-antd'>
            <QueryBuilder
              showNotToggle
              query={queryObject}
              controlClassnames={controlClassnames.antd}
              controlElements={controlElements.antd}
              fields={props.tableName === 'Comment' ? commentFields : postFields} 
              onQueryChange={logQuery} />
          </div>
          <div>
            Result:
          </div>
          <div className='query-result'>
            {queryString}
          </div>
        </Modal>
    );
}

export default SearchBuilderModal;