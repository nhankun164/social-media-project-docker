import React from 'react';
import { ITrendCountObject, ITrendData } from '../../../redux/store/main/main.types';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { AutoMovingDuration } from '../../../constants/common';
import NoDataToDisplay from 'highcharts/modules/no-data-to-display';
import { ChartNameEnum } from '../../../constants/enum';


NoDataToDisplay(Highcharts);

interface IEmotionCountBarChartProps {
    type: ChartNameEnum;
    data?: ITrendCountObject;
    // isAutoMovingPlotLine: number;
    // getRssByTime: (selectedDate: number) => void;
}


var emotionColorPoint: any = {
    'joy': '#12557a',
    'fear': '#D06CFF',
    'sadness': '#BC611E',
    'anger': '#90ed7d',
};

var sentimentColorPoint: any = {
    'total': '#12557a',
    'positive': '#D06CFF',
    'negative': '#BC611E',
    'neutral': '#90ed7d',
};

const EmotionCountBarChart = (props: IEmotionCountBarChartProps) => {
    // const [emotionChartOption, setEmotionChartOption] = React.useState<Highcharts.Options>();
    // const [sentimentChartOption, setSentimentChartOption] = React.useState<Highcharts.Options>();
    // const [selectedIdx, setSelectedIdx] = React.useState<number>(0);
    // let emotionChartRef = React.useRef<Highcharts.Chart>();
    // let sentimentChartRef = React.useRef<Highcharts.Chart>();
    const [barChartOption, setBarChartOption] = React.useState<Highcharts.Options>();
    let chartRef = React.useRef<Highcharts.Chart>();

    


    React.useEffect(() => {
        // setSelectedIdx(0);
        if (props.data) {
            const chartOptions = generateBarChartOption(props.data, props.type);
            setBarChartOption(chartOptions);
        }
        else {
            const chartOptions = generateChartNoData(props.type);
            setBarChartOption(chartOptions);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.data])



    const generateBarChartOption = (dataChart: ITrendCountObject, type: ChartNameEnum) => {
        const seriesBarChart = prepareSeriesDataChart(dataChart);
        let totalCount = 0;
        Object.keys(dataChart).forEach((obj) => {
            totalCount += dataChart[obj]
        })
        const highchartOptions: Highcharts.Options = {
            chart: {
                zoomType: 'xy',
                panning: {
                    enabled: true,
                },
                panKey: 'shift',
                type: 'bar',
                height: 400,
                animation: false,
                style: {
                    fontFamily: 'roboto'
                }
            },
            title: {
                text: Highcharts.numberFormat(Number(totalCount), 0, undefined, ',') + ' Comments',
                style: {
                    fontSize: '15px'
                },
                // align: 'center',
                // verticalAlign: 'middle',
                
            },
            xAxis: {
                type: 'category'
            },
            legend: {
                enabled: false
            },
            yAxis: {
                title: {
                    text: 'The number of Comments'
                },
                
            },
            tooltip: {
                formatter: function () {
                    return  `<br/> Value: <b> ${Highcharts.numberFormat(Number(this.y), 0, '.', ',')} </b>`;
                }
            },
            credits: {
                enabled: false
            },
            subtitle: {
                text: ''
            },
            series: seriesBarChart
        }
        return highchartOptions;
    }

    const generateChartNoData = (type: ChartNameEnum) => {
        const colorObj = type === ChartNameEnum.Emotion ? emotionColorPoint : sentimentColorPoint;
        const noDataSeries = Object.keys(colorObj).map(prod => {
            return {
                type: 'bar',
                name: prod,
                data: [],
                color: colorObj[prod],
                //visible: (key === 'actual' || key === 'p50') ? true : (isWhatIfData ? true : false)
            } as Highcharts.SeriesBarOptions
        })
        const noDataChartOptions: Highcharts.Options = {
            chart: {
                zoomType: 'xy',
                panning: {
                    enabled: true,
                },
                panKey: 'shift',
                type: 'bar',
                height: 500,
                animation: false,
                style: {
                    fontFamily: 'roboto'
                }
            },
            title: {
                text: Highcharts.numberFormat(0, 0, undefined, ',') + ' Comments',
                style: {
                    fontSize: '15px'
                },
                // align: 'center',
                // verticalAlign: 'middle',
                // y: -5,
            },
            xAxis: {
                type: 'category'
            },
            tooltip: {
                formatter: function () {
                    return  `<br/> Value: <b> ${Highcharts.numberFormat(Number(this.y), 0, '.', ',')} <br/> <b> ${Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x)}</b>`;
                }
            },
            credits: {
                enabled: false
            },
            subtitle: {
                text: ''
            },
            series: noDataSeries
        }
        return noDataChartOptions;
    }

    const renderChart = (chart: Highcharts.Chart) => {
        chartRef.current = chart;
        console.log('chartRef.current------------ ', chartRef.current)
        var axis = chartRef.current.xAxis[0];
        var series = chartRef.current.series[0];
    }

    const prepareSeriesDataChart = (dataChart: ITrendCountObject) => {
        // const colorObj = props.type === ChartNameEnum.Emotion ? emotionColorPoint : sentimentColorPoint;
        let seriesDataList = Object.keys(dataChart).map((categoryObj, index) => {
            return [categoryObj, dataChart[categoryObj]];
        })
        return [{
            type: 'bar',
            name: 'category',
            data: seriesDataList
        }] as Highcharts.SeriesBarOptions[];
        
    } 


    return (
        <div>
            <HighchartsReact
                highcharts={Highcharts}
                options={barChartOption}
                callback={renderChart}
            />
            {/* <HighchartsReact
                highcharts={Highcharts}
                options={sentimentChartOption}
                callback={renderSentimentChart}
            /> */}
        </div>
            
        
    );
}

export default EmotionCountBarChart;