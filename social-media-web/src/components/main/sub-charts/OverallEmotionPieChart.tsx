import React from 'react';
import { ITrendCountObject, ITrendData } from '../../../redux/store/main/main.types';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { AutoMovingDuration } from '../../../constants/common';
import NoDataToDisplay from 'highcharts/modules/no-data-to-display';
import { ChartNameEnum } from '../../../constants/enum';


NoDataToDisplay(Highcharts);

interface IOverallEmotionPieChartProps {
    type: ChartNameEnum;
    data?: ITrendCountObject;
    // isAutoMovingPlotLine: number;
    // getRssByTime: (selectedDate: number) => void;
}


var emotionColorPoint: any = {
    'joy': '#12557a',
    'fear': '#D06CFF',
    'sadness': '#BC611E',
    'anger': '#90ed7d',
};

var sentimentColorPoint: any = {
    'total': '#12557a',
    'positive': '#D06CFF',
    'negative': '#BC611E',
    'neutral': '#90ed7d',
};

const OverallEmotionPieChart = (props: IOverallEmotionPieChartProps) => {
    // const [emotionChartOption, setEmotionChartOption] = React.useState<Highcharts.Options>();
    // const [sentimentChartOption, setSentimentChartOption] = React.useState<Highcharts.Options>();
    // const [selectedIdx, setSelectedIdx] = React.useState<number>(0);
    // let emotionChartRef = React.useRef<Highcharts.Chart>();
    // let sentimentChartRef = React.useRef<Highcharts.Chart>();
    const [pieChartOption, setPieChartOption] = React.useState<Highcharts.Options>();
    let chartRef = React.useRef<Highcharts.Chart>();

    


    React.useEffect(() => {
        // setSelectedIdx(0);
        if (props.data) {
            const chartOptions = generatePieChartOption(props.data, props.type);
            setPieChartOption(chartOptions);
        }
        else {
            const chartOptions = generateChartNoData(props.type);
            setPieChartOption(chartOptions);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.data])



    const generatePieChartOption = (dataChart: ITrendCountObject, type: ChartNameEnum) => {
        const seriesPieData = prepareSeriesDataChart(dataChart);
        let totalCount = 0;
        Object.keys(dataChart).forEach((obj) => {
            totalCount += dataChart[obj]
        })
        const highchartOptions: Highcharts.Options = {
            chart: {
                zoomType: 'xy',
                panning: {
                    enabled: true,
                },
                panKey: 'shift',
                type: 'pie',
                height: 400,
                animation: false,
                style: {
                    fontFamily: 'roboto'
                }
            },
            title: {
                text: Highcharts.numberFormat(Number(totalCount), 0, undefined, ',') + ' Comments',
                style: {
                    fontSize: '15px'
                },
                align: 'center',
                verticalAlign: 'middle',
                
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return Highcharts.numberFormat(Number(this.point.percentage), 2, '.', ',')  + '%'
                        }
                    },
                    
                    showInLegend: true,
                    startAngle: -360,
                    endAngle: 0,
                    center: ['50%', '50%'],
                    size: '85%'
                }
            },
            
            tooltip: {
                formatter: function () {
                    return  `<br/> Value: <b> ${Highcharts.numberFormat(Number(this.y), 0, '.', ',')} </b>`;
                }
            },
            credits: {
                enabled: false
            },
            subtitle: {
                text: ''
            },
            series: seriesPieData
        }
        return highchartOptions;
    }

    const generateChartNoData = (type: ChartNameEnum) => {
        const colorObj = type === ChartNameEnum.Emotion ? emotionColorPoint : sentimentColorPoint;
        const noDataSeries = Object.keys(colorObj).map(prod => {
            return {
                type: 'pie',
                name: prod,
                data: [],
                color: colorObj[prod],
                //visible: (key === 'actual' || key === 'p50') ? true : (isWhatIfData ? true : false)
            } as Highcharts.SeriesPieOptions
        })
        const noDataChartOptions: Highcharts.Options = {
            chart: {
                zoomType: 'xy',
                panning: {
                    enabled: true,
                },
                panKey: 'shift',
                type: 'pie',
                height: 400,
                animation: false,
                style: {
                    fontFamily: 'roboto'
                }
            },
            title: {
                text: Highcharts.numberFormat(0, 0, undefined, ',') + ' Comments',
                style: {
                    fontSize: '15px'
                },
                align: 'center',
                verticalAlign: 'middle',
                y: -5,
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return this.point.percentage + '%'
                        }
                    },
                    
                    showInLegend: true,
                    startAngle: -360,
                    endAngle: 0,
                    center: ['50%', '50%'],
                    size: '85%'
                }
            },
            
            tooltip: {
                formatter: function () {
                    return  `<br/> Value: <b> ${Highcharts.numberFormat(Number(this.y), 0, '.', ',')} <br/> <b> ${Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x)}</b>`;
                }
            },
            credits: {
                enabled: false
            },
            subtitle: {
                text: ''
            },
            series: noDataSeries
        }
        return noDataChartOptions;
    }

    const renderChart = (chart: Highcharts.Chart) => {
        chartRef.current = chart;
        console.log('chartRef.current------------ ', chartRef.current)
        var axis = chartRef.current.xAxis[0];
        var series = chartRef.current.series[0];
    }

    const prepareSeriesDataChart = (dataChart: ITrendCountObject) => {
        // const colorObj = props.type === ChartNameEnum.Emotion ? emotionColorPoint : sentimentColorPoint;
        let seriesDataList = Object.keys(dataChart).map((categoryObj, index) => {
            return [categoryObj, dataChart[categoryObj]];
        })
        return [{
            name: 'Overall ' + props.type,
            type: 'pie',
            innerSize: '50%',
            data: seriesDataList
        }] as Highcharts.SeriesPieOptions[];
        
    } 


    return (
        <div>
            <HighchartsReact
                highcharts={Highcharts}
                options={pieChartOption}
                callback={renderChart}
            />
            {/* <HighchartsReact
                highcharts={Highcharts}
                options={sentimentChartOption}
                callback={renderSentimentChart}
            /> */}
        </div>
            
        
    );
}

export default OverallEmotionPieChart;