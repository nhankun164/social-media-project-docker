import React from 'react';
import { ITrendCountObject, ITrendData } from '../../../redux/store/main/main.types';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { AutoMovingDuration, CommaSeparator } from '../../../constants/common';
import NoDataToDisplay from 'highcharts/modules/no-data-to-display';
import { ChartNameEnum } from '../../../constants/enum';
import ReactWordcloud from 'react-wordcloud';

import "tippy.js/dist/tippy.css";
import "tippy.js/animations/scale.css";

NoDataToDisplay(Highcharts);

interface IWordCloudsProps {
    data: string[];
}

interface IWordCloudValue {
    text: string;
    value: number;
}


const WordClouds = (props: IWordCloudsProps) => {
    const [wordCloudArr, setWordCloudArr] = React.useState<IWordCloudValue[]>([]);
    


    React.useEffect(() => {
        // setSelectedIdx(0);
        if (props.data.length > 0) {
            let wordCloudMapper: Map<string, number> = new Map();
            props.data.forEach(string => {
                if (string === '-') {
                    return;
                }
                const words = string.split(',');
                words.forEach(word => {
                    if (wordCloudMapper.has(word)) {
                        let value = Number(wordCloudMapper.get(word));
                        wordCloudMapper.set(word, value + 1);
                    }
                    else {
                        wordCloudMapper.set(word, 1)
                    }
                })
            })
            const wordCloudArr: IWordCloudValue[] = Array.from(wordCloudMapper, ([text, value]) => ({ text, value }));
            setWordCloudArr(wordCloudArr);
        }
        
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.data])




    return (
        <div>
            <ReactWordcloud 
                words={wordCloudArr} 
                options={{
                    enableTooltip: true,
                    deterministic: false,
                    // enableOptimizations: true,
                    rotations: 2,
                    rotationAngles: [-90, 0],
                    fontSizes: [15, 55],
                    spiral: "rectangular",
                    transitionDuration: 1000
                  }}
                size={[700, 400]}
                minSize={[700, 400]}
                
            />
        </div>
            
        
    );
}

export default WordClouds;