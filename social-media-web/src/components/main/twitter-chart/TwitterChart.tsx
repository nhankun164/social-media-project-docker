import React from 'react';
import './TwitterChart.css';
import { ITwitter } from '../../../redux/store/main/main.types';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { AutoMovingDuration } from '../../../constants/common';
import NoDataToDisplay from 'highcharts/modules/no-data-to-display';


NoDataToDisplay(Highcharts);

var interval: NodeJS.Timeout;

interface ITwitterChartProps {
    data: ITwitter[];
    isAutoMovingPlotLine: number;
    getRssByTime: (selectedDate: number) => void;
}


const TwitterChart = (props: ITwitterChartProps) => {
    const [splineChartOption, setSplineChartOption] = React.useState<Highcharts.Options>();
    // const [selectedIdx, setSelectedIdx] = React.useState<number>(0);
    let chartRef = React.useRef<Highcharts.Chart>();


    React.useEffect(() => {
        if (props.data.length > 0) {
            const chartOptions = generateSplineChartOption(props.data);
            setSplineChartOption(chartOptions);
        }
        else {
            const chartOptions = generateChartNoData();
            setSplineChartOption(chartOptions);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.data])


    React.useEffect(() => {
        
        if (chartRef.current) {
            if (props.isAutoMovingPlotLine) {
                if (props.data.length > 0) {
                    let axis = chartRef.current.xAxis[0];
                    let series = chartRef.current.series[0];
                    clearInterval(interval);
                    interval = setInterval(() => {  
                        // set up the updating of the chart each second
                        if ((axis as any).plotLinesAndBands && (axis as any).plotLinesAndBands.length > 0) {
                            let plotLine = (axis as any).plotLinesAndBands[0];
                            if (plotLine && axis.options.plotLines) {
                                let plotLineOptions = plotLine.options;
                                let id = Number(plotLineOptions.id);
                                if (id !== series.data.length - 1) {
                                    let newValue = series.data[id + 1].x;
                                    let newPlotLineOption = {
                                        ...plotLineOptions,
                                        id: (id + 1).toString(),
                                        value: newValue
                                    }
                                    // setSelectedIdx(id + 1);
                                    axis.removePlotLine(id.toString());
                                    axis.addPlotLine(newPlotLineOption);
                                    // axis.update(axis.options, true);
                                    props.getRssByTime(newValue/1000);
                                }
                            }
                        }
                    }, AutoMovingDuration);
                }
                else {
                    const chartOptions = generateChartNoData();
                    setSplineChartOption(chartOptions);
                }
            }
            else {
                clearInterval(interval);
            }
            
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.isAutoMovingPlotLine])



    const generateSplineChartOption = (dataChart: ITwitter[]) => {
        const seriesSplineData = prepareSeriesDataChart(dataChart);
        const startPlotLine: Highcharts.AxisPlotLinesOptions = {
            color: 'orange',
            dashStyle: 'Solid',
            width: 1,
            value: dataChart[0].create_date_unix * 1000,
            id: '0',
            zIndex: 9999,
        };
        const highchartOptions: Highcharts.Options = {
            chart: {
                zoomType: 'xy',
                panning: {
                    enabled: true,
                },
                panKey: 'shift',
                type: 'spline',
                height: 500,
                animation: false,
                style: {
                    fontFamily: 'roboto'
                }
            },
            plotOptions: {
                series: {
                    animation: false,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                // var chartRef = type === 'Emotion' ? emotionChartRef : sentimentChartRef;
                                if (chartRef && chartRef.current && chartRef.current.xAxis) {
                                    var axis = chartRef.current.xAxis[0];
                                    var series = chartRef.current.series[0];
                                    var plotLine = (axis as any).plotLinesAndBands[0];
                                    if (plotLine) {
                                        var plotLineOptions = plotLine.options;
                                        var id = Number(plotLineOptions.id);
                                        // var selectedPointIndex = series.data.findIndex((_, index) => index === plotLineOptions.id);
                                        if (id !== series.data.length - 1) {
                                            var newValue = this.x;
                                            
                                            var newPlotLineOption = {
                                                ...plotLineOptions,
                                                id: this.index.toString(),
                                                value: newValue
                                            }
                                            axis.removePlotLine(id.toString());
                                            axis.addPlotLine(newPlotLineOption);
                                            props.getRssByTime(newPlotLineOption.value/1000);
                                        }
                                    }
                                }
                            }
                        }
                    },
                },

            },
            title: {
                text: `Twitter User Follower Chart`,
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%b. %e',
                    week: '%b. %e',
                    month: '%b. %Y',
                    year: '%Y'
                },
                plotLines: [startPlotLine]
            },
            yAxis: {
                title: {
                    text: 'The number of Users',
                },
                labels: {
                    formatter: function () {
                        // Use thousands separator for four-digit numbers too
                        return Highcharts.numberFormat(Number(this.value), 3, undefined, ',');
                    }
                }
            },
            
            tooltip: {
                formatter: function () {
                    return  `<br/> Value: <b> ${Highcharts.numberFormat(Number(this.y), 0, '.', ',')} <br/> <b> ${Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x)}</b>`;
                }
            },
            credits: {
                enabled: false
            },
            subtitle: {
                text: ''
            },
            series: seriesSplineData
        }
        return highchartOptions;
    }

    // const loadFunction = () => {
    //     if (chartRef && chartRef.current && chartRef.current.xAxis) {
    //         var axis = chartRef.current.xAxis[0];
    //         var series = chartRef.current.series[0];
    //         setInterval(function () {  
    //             // set up the updating of the chart each second
    //             if ((axis as any).plotLinesAndBands && (axis as any).plotLinesAndBands.length > 0) {
    //                 var plotLine = (axis as any).plotLinesAndBands[0];
    //                 if (plotLine && axis.options.plotLines) {
    //                     var plotLineOptions = plotLine.options;
    //                     var id = Number(plotLineOptions.id);
    //                     // const id = Number(axis.options.plotLines[0].id);
    //                     // var selectedPointIndex = series.data.findIndex((_, index) => index === plotLineOptions.id);
    //                     if (id !== series.data.length - 1) {
    //                         var newValue = series.data[id + 1].x;
    //                         // var newPlotLineOption = {
    //                         //     ...plotLineOptions,
    //                         //     id: (id + 1).toString(),
    //                         //     value: newValue
    //                         // }
    //                         // plotLineOptions.id = (id + 1).toString();
    //                         // plotLineOptions.value = newValue;
    //                         axis.options.plotLines[0].id = (id + 1).toString();
    //                         axis.options.plotLines[0].value = newValue;
    //                         // axis.removePlotLine(id.toString());
    //                         // axis.addPlotLine(newPlotLineOption);
    //                         axis.update(axis.options, true);
    //                         props.getRssByTime(newValue/1000);
    //                     }
    //                 }
    //             }
                
            
            
    //         }, AutoMovingDuration);
    //     }
    // }

    const generateChartNoData = () => {
        const noDataSeries = [
            {
                type: 'spline',
                name: 'Twitter',
                data: [],
                color: '#12557a',
                //visible: (key === 'actual' || key === 'p50') ? true : (isWhatIfData ? true : false)
            } as Highcharts.SeriesSplineOptions
        ]
        const noDataChartOptions: Highcharts.Options = {
            lang: {
                thousandsSep: ',',
                decimalPoint: '.',
                noData: 'No Data Available'
            },
            chart: {
                type: 'spline',
                zoomType: 'xy',
                panning: {
                    enabled: true,
                },
                panKey: 'shift',
                animation: false,
                height: 500,
                // events: {
                //     load() {
                //       this.showLoading();
                //       setTimeout(this.hideLoading.bind(this), 2000);
                //     }
                // },
                style: {
                    fontFamily: 'roboto'
                }
            },
            plotOptions: {
                series: {
                    animation: false,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                // var chartRef = type === 'Emotion' ? emotionChartRef : sentimentChartRef;
                                if (chartRef && chartRef.current && chartRef.current.xAxis) {
                                    var axis = chartRef.current.xAxis[0];
                                    var series = chartRef.current.series[0];
                                    var plotLine = (axis as any).plotLinesAndBands[0];
                                    if (plotLine) {
                                        var plotLineOptions = plotLine.options;
                                        var id = Number(plotLineOptions.id);
                                        // var selectedPointIndex = series.data.findIndex((_, index) => index === plotLineOptions.id);
                                        if (id !== series.data.length - 1) {
                                            var newValue = this.x;
                                            
                                            var newPlotLineOption = {
                                                ...plotLineOptions,
                                                id: this.index.toString(),
                                                value: newValue
                                            }
                                            axis.removePlotLine(id.toString());
                                            axis.addPlotLine(newPlotLineOption);
                                            props.getRssByTime(newPlotLineOption.value/1000);
                                        }
                                    }
                                }
                            }
                        }
                    },
                },

            },
            credits: {
                enabled: false
            },
            title: {
                text: `Emotion Score Chart From Comments`,
            },
            xAxis: {
                //categories: xAxisMonthData.map(String)
            },
            yAxis: {
                title: {
                    text: 'Emotion Score',
                },
                labels: {
                    formatter: function () {
                        // Use thousands separator for four-digit numbers too
                        return Highcharts.numberFormat(Number(this.value), 0, undefined, ',');
                    }
                }
            },
            series: noDataSeries,
        };
        return noDataChartOptions;
    }


    const renderChart = (chart: Highcharts.Chart) => {
        chartRef.current = chart;
        var axis = chartRef.current.xAxis[0];
        var series = chartRef.current.series[0];
        clearInterval(interval);
        interval = setInterval(() => {  
            // set up the updating of the chart each second
            if ((axis as any).plotLinesAndBands && (axis as any).plotLinesAndBands.length > 0) {
                var plotLine = (axis as any).plotLinesAndBands[0];
                if (plotLine && axis.options.plotLines) {
                    var plotLineOptions = plotLine.options;
                    var id = Number(plotLineOptions.id);
                    // const id = Number(axis.options.plotLines[0].id);
                    // var selectedPointIndex = series.data.findIndex((_, index) => index === plotLineOptions.id);
                    if (id !== series.data.length - 1) {
                        var newValue = series.data[id + 1].x;
                        var newPlotLineOption = {
                            ...plotLineOptions,
                            id: (id + 1).toString(),
                            value: newValue
                        }
                        // setSelectedIdx(id + 1);
                        // plotLineOptions.id = (id + 1).toString();
                        // plotLineOptions.value = newValue;
                        // axis.options.plotLines[0].id = (id + 1).toString();
                        // axis.options.plotLines[0].value = newValue;
                        axis.removePlotLine(id.toString());
                        axis.addPlotLine(newPlotLineOption);
                        // axis.update(axis.options, true);
                        props.getRssByTime(newValue/1000);
                    }
                }
            }
            
        
        
        }, AutoMovingDuration);
    }

    const prepareSeriesDataChart = (dataChart: ITwitter[]) => {
        let seriesDataList: Highcharts.SeriesSplineOptions[] = [
            {
                type: 'spline',
                name: 'Twitter',
                color: '#12557a',  //#D06CFF
                data: dataChart.map(twitter => [twitter.create_date_unix * 1000, twitter.user_follower_count])
            } as Highcharts.SeriesSplineOptions
            
        ];
        return seriesDataList;
        
    } 


    return (
        <div>
            <HighchartsReact
                highcharts={Highcharts}
                options={splineChartOption}
                callback={renderChart}
            />
            {/* <HighchartsReact
                highcharts={Highcharts}
                options={sentimentChartOption}
                callback={renderSentimentChart}
            /> */}
        </div>
            
        
    );
}

export default TwitterChart;