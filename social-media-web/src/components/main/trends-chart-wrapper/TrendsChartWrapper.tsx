import React from 'react';
import './TrendsChartWrapper.css';
import { ITrendData } from '../../../redux/store/main/main.types';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { AutoMovingDuration } from '../../../constants/common';
import NoDataToDisplay from 'highcharts/modules/no-data-to-display';
import { ChartNameEnum } from '../../../constants/enum';


NoDataToDisplay(Highcharts);

interface ITrendsChartWrapperProps {
    type: ChartNameEnum;
    data: ITrendData[];
    isAutoMovingPlotLine: number;
    getRssByTime: (selectedDate: number) => void;
}
var interval: NodeJS.Timeout;


var emotionColorPoint: any = {
    'joy': '#12557a',
    'fear': '#D06CFF',
    'sadness': '#BC611E',
    'anger': '#90ed7d',
};

var sentimentColorPoint: any = {
    'total': '#12557a',
    'positive': '#D06CFF',
    'negative': '#BC611E',
    'neutral': '#90ed7d',
};

const TrendsChartWrapper = (props: ITrendsChartWrapperProps) => {
    // const [emotionChartOption, setEmotionChartOption] = React.useState<Highcharts.Options>();
    // const [sentimentChartOption, setSentimentChartOption] = React.useState<Highcharts.Options>();
    // const [selectedIdx, setSelectedIdx] = React.useState<number>(0);
    // let emotionChartRef = React.useRef<Highcharts.Chart>();
    // let sentimentChartRef = React.useRef<Highcharts.Chart>();
    const [splineChartOption, setSplineChartOption] = React.useState<Highcharts.Options>();
    let chartRef = React.useRef<Highcharts.Chart>();

    

    React.useEffect(() => {
        
        if (chartRef.current) {
            if (props.isAutoMovingPlotLine) {
                // chartRef.current.options.chart.events.load = loadFunction;
                // chartRef.current.update(chartRef.current.options, true, true);
                if (props.data.length > 0) {
                    var axis = chartRef.current.xAxis[0];
                    var series = chartRef.current.series[0];
                    clearInterval(interval);
                    interval = setInterval(() => {  
                        // set up the updating of the chart each second
                        if ((axis as any).plotLinesAndBands && (axis as any).plotLinesAndBands.length > 0) {
                            var plotLine = (axis as any).plotLinesAndBands[0];
                            if (plotLine && axis.options.plotLines) {
                                var plotLineOptions = plotLine.options;
                                var id = Number(plotLineOptions.id);
                                // const id = Number(axis.options.plotLines[0].id);
                                // var selectedPointIndex = series.data.findIndex((_, index) => index === plotLineOptions.id);
                                if (id !== series.data.length - 1) {
                                    var newValue = series.data[id + 1].x;
                                    var newPlotLineOption = {
                                        ...plotLineOptions,
                                        id: (id + 1).toString(),
                                        value: newValue
                                    }
                                    // setSelectedIdx(id + 1);
                                    // plotLineOptions.id = (id + 1).toString();
                                    // plotLineOptions.value = newValue;
                                    // axis.options.plotLines[0].id = (id + 1).toString();
                                    // axis.options.plotLines[0].value = newValue;
                                    axis.removePlotLine(id.toString());
                                    axis.addPlotLine(newPlotLineOption);
                                    // axis.update(axis.options, true);
                                    props.getRssByTime(newValue/1000);
                                }
                            }
                        }
                    }, AutoMovingDuration);
                }
                else {
                    const chartOptions = generateChartNoData(props.type);
                    setSplineChartOption(chartOptions);
                }
            }
            else {
                clearInterval(interval);
            }
            
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.isAutoMovingPlotLine])

    React.useEffect(() => {
        // setSelectedIdx(0);
        if (props.data.length > 0) {
            const chartOptions = generateSplineChartOption(props.data, props.type);
            setSplineChartOption(chartOptions);
        }
        else {
            const chartOptions = generateChartNoData(props.type);
            setSplineChartOption(chartOptions);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.data])



    const generateSplineChartOption = (dataChart: ITrendData[], type: ChartNameEnum) => {
        const seriesSplineData = prepareSeriesDataChart(dataChart);
        const startPlotLine: Highcharts.AxisPlotLinesOptions = {
            color: 'orange',
            dashStyle: 'Solid',
            width: 1,
            value: dataChart[0].data[0][0] * 1000,
            id: '0',
            zIndex: 9999,
        };
        const highchartOptions: Highcharts.Options = {
            chart: {
                zoomType: 'xy',
                panning: {
                    enabled: true,
                },
                panKey: 'shift',
                type: 'spline',
                height: 500,
                animation: false,
                style: {
                    fontFamily: 'roboto'
                }
            },
            plotOptions: {
                series: {
                    animation: false,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                // var chartRef = type === 'Emotion' ? emotionChartRef : sentimentChartRef;
                                if (chartRef && chartRef.current && chartRef.current.xAxis) {
                                    var axis = chartRef.current.xAxis[0];
                                    var series = chartRef.current.series[0];
                                    var plotLine = (axis as any).plotLinesAndBands[0];
                                    if (plotLine) {
                                        var plotLineOptions = plotLine.options;
                                        var id = Number(plotLineOptions.id);
                                        // var selectedPointIndex = series.data.findIndex((_, index) => index === plotLineOptions.id);
                                        if (id !== series.data.length - 1) {
                                            var newValue = this.x;
                                            
                                            var newPlotLineOption = {
                                                ...plotLineOptions,
                                                id: this.index.toString(),
                                                value: newValue
                                            }
                                            // setSelectedIdx(this.index);
                                            axis.removePlotLine(id.toString());
                                            axis.addPlotLine(newPlotLineOption);
                                            props.getRssByTime(newPlotLineOption.value/1000);
                                        }
                                    }
                                }
                            }
                        }
                    },
                },

            },
            title: {
                text: `${type} Trends Chart`,
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%b. %e',
                    week: '%b. %e',
                    month: '%b. %Y',
                    year: '%Y'
                },
                plotLines: [startPlotLine]
            },
            yAxis: {
                title: {
                    text: 'The number of Comments',
                },
                labels: {
                    formatter: function () {
                        // Use thousands separator for four-digit numbers too
                        return Highcharts.numberFormat(Number(this.value), 0, undefined, ',');
                    }
                }
            },
            
            tooltip: {
                formatter: function () {
                    return  `<br/> Value: <b> ${Highcharts.numberFormat(Number(this.y), 0, '.', ',')} <br/> <b> ${Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x)}</b>`;
                }
            },
            credits: {
                enabled: false
            },
            subtitle: {
                text: ''
            },
            series: seriesSplineData
        }
        return highchartOptions;
    }

    const generateChartNoData = (type: ChartNameEnum) => {
        const colorObj = type === ChartNameEnum.Emotion ? emotionColorPoint : sentimentColorPoint;
        const noDataSeries = Object.keys(colorObj).map(prod => {
            return {
                type: 'spline',
                name: prod,
                data: [],
                color: colorObj[prod],
                //visible: (key === 'actual' || key === 'p50') ? true : (isWhatIfData ? true : false)
            } as Highcharts.SeriesSplineOptions
        })
        const noDataChartOptions: Highcharts.Options = {
            lang: {
                thousandsSep: ',',
                decimalPoint: '.',
                noData: 'No Data Available'
            },
            chart: {
                type: 'spline',
                zoomType: 'xy',
                panning: {
                    enabled: true,
                },
                panKey: 'shift',
                animation: false,
                height: 500,
                style: {
                    fontFamily: 'roboto'
                }
            },
            credits: {
                enabled: false
            },
            title: {
                text: `${type} Trends Chart`,
            },
            xAxis: {
                //categories: xAxisMonthData.map(String)
            },
            yAxis: {
                title: {
                    text: 'The number of Comments',
                },
                labels: {
                    formatter: function () {
                        // Use thousands separator for four-digit numbers too
                        return Highcharts.numberFormat(Number(this.value), 0, undefined, ',');
                    }
                }
            },
            plotOptions: {
                series: {
                    animation: false,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                // var chartRef = type === 'Emotion' ? emotionChartRef : sentimentChartRef;
                                if (chartRef && chartRef.current && chartRef.current.xAxis) {
                                    var axis = chartRef.current.xAxis[0];
                                    var series = chartRef.current.series[0];
                                    var plotLine = (axis as any).plotLinesAndBands[0];
                                    if (plotLine) {
                                        var plotLineOptions = plotLine.options;
                                        var id = Number(plotLineOptions.id);
                                        // var selectedPointIndex = series.data.findIndex((_, index) => index === plotLineOptions.id);
                                        if (id !== series.data.length - 1) {
                                            var newValue = this.x;
                                            
                                            var newPlotLineOption = {
                                                ...plotLineOptions,
                                                id: this.index.toString(),
                                                value: newValue
                                            }
                                            axis.removePlotLine(id.toString());
                                            axis.addPlotLine(newPlotLineOption);
                                            props.getRssByTime(newPlotLineOption.value/1000);
                                        }
                                    }
                                }
                            }
                        }
                    },
                },

            },
            series: noDataSeries,
        };
        return noDataChartOptions;
    }

    const renderChart = (chart: Highcharts.Chart) => {
        chartRef.current = chart;
        console.log('chartRef.current------------ ', chartRef.current)
        var axis = chartRef.current.xAxis[0];
        var series = chartRef.current.series[0];
        clearInterval(interval);
        interval = setInterval(() => {  
            // set up the updating of the chart each second
            if ((axis as any).plotLinesAndBands && (axis as any).plotLinesAndBands.length > 0) {
                var plotLine = (axis as any).plotLinesAndBands[0];
                if (plotLine && axis.options.plotLines) {
                    var plotLineOptions = plotLine.options;
                    var id = Number(plotLineOptions.id);
                    // const id = Number(axis.options.plotLines[0].id);
                    // var selectedPointIndex = series.data.findIndex((_, index) => index === plotLineOptions.id);
                    if (id !== series.data.length - 1) {
                        var newValue = series.data[id + 1].x;
                        var newPlotLineOption = {
                            ...plotLineOptions,
                            id: (id + 1).toString(),
                            value: newValue
                        }
                        // setSelectedIdx(id + 1);
                        // plotLineOptions.id = (id + 1).toString();
                        // plotLineOptions.value = newValue;
                        // axis.options.plotLines[0].id = (id + 1).toString();
                        // axis.options.plotLines[0].value = newValue;
                        axis.removePlotLine(id.toString());
                        axis.addPlotLine(newPlotLineOption);
                        // axis.update(axis.options, true);
                        props.getRssByTime(newValue/1000);
                    }
                }
            }
            
        
        
        }, AutoMovingDuration);
    }

    const prepareSeriesDataChart = (dataChart: ITrendData[]) => {
        const colorObj = props.type === ChartNameEnum.Emotion ? emotionColorPoint : sentimentColorPoint;
        let seriesDataList = dataChart.map((categoryObj, index) => {
            const data = categoryObj.data.map(point => [point[0] * 1000, point[1]])
            const seriesLineData = {
                type: 'spline',
                name: categoryObj.categoryName,
                color: colorObj[categoryObj.category] ? colorObj[categoryObj.category] : 'black',  //#D06CFF
                data: data
            } as Highcharts.SeriesSplineOptions;
            return seriesLineData;
        })
        return seriesDataList;
        
    } 


    return (
        <div>
            <HighchartsReact
                highcharts={Highcharts}
                options={splineChartOption}
                callback={renderChart}
            />
            {/* <HighchartsReact
                highcharts={Highcharts}
                options={sentimentChartOption}
                callback={renderSentimentChart}
            /> */}
        </div>
            
        
    );
}

export default TrendsChartWrapper;