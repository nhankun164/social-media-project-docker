import React from 'react';
import {
    Route,
    Switch,
} from 'react-router-dom';
import { connect } from 'react-redux';
import MainComponent from './main/Main';
import { AppState } from '../redux/store';

import {
  IMainState
} from '../redux/store/main/main.types';
// import en from 'react-intl/locale-data/en';

interface IAppProps {
  main: IMainState;
}

// interface IPrivateRouteProps extends RouteProps {
//   isAuth: boolean;
// }

// const PrivateRoute = ({ component, isAuth, ...rest }: IPrivateRouteProps) => {
//   if (!component) {
//     throw Error('component is undefined');
//   }

//   const Component = component; // JSX Elements have to be uppercase.
//   const render = (props: RouteComponentProps<any>): React.ReactNode => {
//     if (isAuth) {
//       return <Component {...props} />;
//     }
//     return <Redirect to={{ pathname: '/login' }} />;
//   };

//   return (<Route {...rest} render={render} />);
// };

const Routes: React.FC<IAppProps> = (props: IAppProps) => {

  return (
      <Switch>
          <Route exact path='/' component={MainComponent}/>
          {/* <PrivateRoute path='/' component={MainComponent} isAuth={props.auth.result.data.token !== ''}/> */}
      </Switch>
  );
};

const mapStateToProps = (state: AppState) => {
  return {
      main: state.main
  };
};

export default connect(
  mapStateToProps
)(Routes);
