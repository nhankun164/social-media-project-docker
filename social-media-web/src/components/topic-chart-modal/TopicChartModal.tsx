import React from 'react';
import { Modal } from 'antd';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import HC_more from 'highcharts/highcharts-more' //module

HC_more(Highcharts) //init module


interface ITopicChartModalProps {
    open: boolean;
    dataSeries?: any;
    onClose: () => void;
}


const TopicChartModal = (props: ITopicChartModalProps) => {
    const [chartOption, setChartOption] = React.useState<Highcharts.Options>();

    React.useEffect(() => {
      if (props.dataSeries && props.dataSeries.series) {
          const chartOption = generateChartOption(props.dataSeries.series);
          setChartOption(chartOption);
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.dataSeries])


    const generateChartOption = (series: any) => {
        const highchartOptions: Highcharts.Options = {
            chart: {
                // zoomType: 'xy',
                // panning: {
                //     enabled: true,
                // },
                // panKey: 'shift',
                type: 'packedbubble',
                // height: '100%',
                height: 600,
                animation: false,
                style: {
                    fontFamily: 'roboto'
                },
            },
            plotOptions: {
                packedbubble: {
                    minSize: '20%',
                    maxSize: '100%',
                    layoutAlgorithm: {
                        gravitationalConstant: 0.05,
                        splitSeries: 'true',
                        seriesInteraction: false,
                        dragBetweenSeries: false,
                        parentNodeLimit: true
                    },
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}',
                        style: {
                            color: 'black',
                            textOutline: 'none',
                            fontWeight: 'normal'
                        }
                    }
                }
            },
            title: {
                text: 'Topics Chart',
            },
            tooltip: {
                useHTML: true,
                pointFormat: '<b>{point.name}:</b> {point.value}'
            },
            credits: {
                enabled: false
            },
            subtitle: {
                text: ''
            },
            series: series
        }
        return highchartOptions;
    }

    const handleClose = () => {
      props.onClose();
    }


    return (
        <Modal
          title='Topic Labeling Modal'
          centered
          width={800}
          visible={props.open}
          okButtonProps={undefined}
          cancelButtonProps={undefined}
          footer={null}
          onCancel={handleClose}
        >
            <div>
                <HighchartsReact
                    highcharts={Highcharts}
                    options={chartOption}
                />
            </div>
        </Modal>
    );
}

export default TopicChartModal;