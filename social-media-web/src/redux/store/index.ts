import {
  createStore,
  combineReducers,
  applyMiddleware
} from 'redux';
import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger';
import {
  composeWithDevTools
} from 'redux-devtools-extension';

import { mainReducer } from './main/main.reducers';
import { messageReducer } from './messages/messages.reducers';
import { searchReducer } from './search/search.reducers';

const appReducer = (state: any, action: any) => {
  return rootReducer(state, action)
}

const rootReducer = combineReducers({
  main: mainReducer,
  search: searchReducer,
  messages: messageReducer
});

export type AppState = ReturnType<typeof rootReducer>;

export default function configureStore() {
  const middlewares = [thunkMiddleware, logger];
  const middleWareEnhancer = applyMiddleware(...middlewares);

  const store = createStore(
    appReducer,
    composeWithDevTools(middleWareEnhancer)
  );

  return store;
}
