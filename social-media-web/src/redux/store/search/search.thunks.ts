import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { executeSearchByTopicFailed, executeSearchByTopicSuccess, executeSearchFailed, executeSearchSuccess } from './search.actions';
import { AppState } from '..';
import { ApiResponseType } from '../Types';
import { ISearchObject } from './search.types';
import axios from 'axios';
import { paramsToQueryString } from '../../../utils/Utils';
import { IComment } from '../main/main.types';


export const thunkExecuteSearch = (
    data: ISearchObject
  ): ThunkAction<void, AppState, null, Action<string>> => async (dispatch) => {
    const url = `http://`
      + `${process.env.NODE_ENV === 'development' ? process.env.REACT_APP_API_SERVER_HOST : window.location.hostname}`
      + `:${process.env.REACT_APP_API_SERVER_PORT}/api/search?`
      + `${paramsToQueryString(data)}`;
    return axios({
      method: 'GET',
      url: url,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      responseType: 'json'
    })
    .then(async (response) => {
      if (response.status === 200) {
        return response.data;
      } else {
        throw response.data
      }
    },
    (error) => {
      if (error.response && error.response.data) {
        throw error.response.data;
      }
      else {
        const errmess = new Error(error.message);
        throw errmess;
      }
    })
    .then((response: ApiResponseType<any>) => {
      return dispatch(
        executeSearchSuccess(response.data as any)
      );
    })
    .catch((error: ApiResponseType<any> | Error) => {
      return dispatch(
        executeSearchFailed(error)
      );
    });
};


export const thunkExecuteSearchByTopic = (
  data: ISearchObject
): ThunkAction<void, AppState, null, Action<string>> => async (dispatch) => {
  const url = `http://`
    + `${process.env.NODE_ENV === 'development' ? process.env.REACT_APP_API_SERVER_HOST : window.location.hostname}`
    + `:${process.env.REACT_APP_API_SERVER_PORT}/api/search/keyword?`
    + `${paramsToQueryString(data)}`;
  return axios({
    method: 'GET',
    url: url,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    responseType: 'json'
  })
  .then(async (response) => {
    if (response.status === 200) {
      return response.data;
    } else {
      throw response.data
    }
  },
  (error) => {
    if (error.response && error.response.data) {
      throw error.response.data;
    }
    else {
      const errmess = new Error(error.message);
      throw errmess;
    }
  })
  .then((response: ApiResponseType<any>) => {
    return dispatch(
      executeSearchByTopicSuccess(response.data as any)
    );
  })
  .catch((error: ApiResponseType<any> | Error) => {
    return dispatch(
      executeSearchByTopicFailed(error)
    );
  });
};