import { IComment } from '../main/main.types';
import { ApiResponseType } from '../Types';

export const EXECUTE_SEARCH_SUCCESS = 'EXECUTE_SEARCH_SUCCESS'
export const EXECUTE_SEARCH_FAILED = 'EXECUTE_SEARCH_FAILED'

export const EXECUTE_SEARCH_BY_TOPIC_SUCCESS = 'EXECUTE_SEARCH_BY_TOPIC_SUCCESS'
export const EXECUTE_SEARCH_BY_TOPIC_FAILED = 'EXECUTE_SEARCH_BY_TOPIC_FAILED'

export interface ISearchState {
    data: any;
    contentList: any;
    error?: ApiResponseType<any> | Error;
}

export interface ISearchObject {
    table: string;
    field: string;
    statement: string;
    keyword?: string;
    startDate: number | string | null;
    endDate: number | string | null;
}

interface ISearchExecuteSuccessAction {
    type: typeof EXECUTE_SEARCH_SUCCESS;
    payload: any;
}
interface ISearchExecuteFailedAction {
    type: typeof EXECUTE_SEARCH_FAILED;
    payload: ApiResponseType<any> | Error;
}

interface ISearchByTopicExecuteSuccessAction {
    type: typeof EXECUTE_SEARCH_BY_TOPIC_SUCCESS;
    payload: any;
}
interface ISearchByTopicExecuteFailedAction {
    type: typeof EXECUTE_SEARCH_BY_TOPIC_FAILED;
    payload: ApiResponseType<any> | Error;
}

export type SearchActionTypes = ISearchExecuteSuccessAction 
                                | ISearchExecuteFailedAction
                                | ISearchByTopicExecuteSuccessAction
                                | ISearchByTopicExecuteFailedAction;