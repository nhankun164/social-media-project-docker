import { IComment } from '../main/main.types';
import { ApiResponseType } from '../Types';
import { EXECUTE_SEARCH_BY_TOPIC_FAILED, EXECUTE_SEARCH_BY_TOPIC_SUCCESS, EXECUTE_SEARCH_FAILED, EXECUTE_SEARCH_SUCCESS } from './search.types';


export function executeSearchSuccess(data: any) {
    return {
        type: EXECUTE_SEARCH_SUCCESS,
        payload: data
    };
}
export function executeSearchFailed(error: ApiResponseType<any> | Error) {
    return {
        type: EXECUTE_SEARCH_FAILED,
        payload: error
    };
}

export function executeSearchByTopicSuccess(data: any) {
    return {
        type: EXECUTE_SEARCH_BY_TOPIC_SUCCESS,
        payload: data
    };
}
export function executeSearchByTopicFailed(error: ApiResponseType<any> | Error) {
    return {
        type: EXECUTE_SEARCH_BY_TOPIC_FAILED,
        payload: error
    };
}