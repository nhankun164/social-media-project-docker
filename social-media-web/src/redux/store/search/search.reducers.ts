import { IComment } from '../main/main.types';
import { EXECUTE_SEARCH_BY_TOPIC_FAILED, EXECUTE_SEARCH_BY_TOPIC_SUCCESS, EXECUTE_SEARCH_FAILED, EXECUTE_SEARCH_SUCCESS, ISearchState, SearchActionTypes } from './search.types';

const initialState: ISearchState = {
    data: [] as any[],
    contentList: [] as IComment[]
};

export function searchReducer(
    state = initialState,
    action: SearchActionTypes
): ISearchState {
    switch (action.type) {
        case EXECUTE_SEARCH_SUCCESS:
            return {
                ...state,
                data: action.payload
            }
        case EXECUTE_SEARCH_FAILED:
            return {
                ...state,
                error: action.payload
            }
        case EXECUTE_SEARCH_BY_TOPIC_SUCCESS:
            return {
                ...state,
                contentList: action.payload
            }
        case EXECUTE_SEARCH_BY_TOPIC_FAILED:
            return {
                ...state,
                error: action.payload
            }
        default:
            return state;
    }
}
