import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { fetchCommentsFailed, fetchCommentsSuccess, fetchRssByTimeFailed, fetchRssByTimeSuccess, fetchTrendsFailed, fetchTrendsSuccess, fetchTwitterFailed, fetchTwitterSuccess, setAllDisableAutoMovingRadio, setAutoMovingRadio } from './main.actions';
import { AppState } from '..';
import { ApiResponseType } from '../Types';
import axios from 'axios';
import { IComment, ICommentResponse, IPeriod, IRadioAutoMovingValue, IRssTable, ITrendsResponse, ITwitter } from './main.types';
import { paramsToQueryString } from '../../../utils/Utils';
import { PeriodTabs } from '../../../constants/enum';

export const thunkFetchTwitterData = (
  data: {
    periodType: PeriodTabs
  }
): ThunkAction<void, AppState, null, Action<string>> => async (dispatch) => {
  const url = `http://`
    + `${process.env.NODE_ENV === 'development' ? process.env.REACT_APP_API_SERVER_HOST : window.location.hostname}`
    + `:${process.env.REACT_APP_API_SERVER_PORT}/api/twitter?`
    + `${paramsToQueryString(data)}`;
  return axios({
    method: 'GET',
    url: url,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    responseType: 'json'
  })
  .then(async (response) => {
    if (response.status === 200) {
      return response.data;
    } else {
      throw response.data
    }
  },
  (error) => {
    if (error.response && error.response.data) {
      throw error.response.data;
    }
    else {
      const errmess = new Error(error.message);
      throw errmess;
    }
  })
  .then((response: ApiResponseType<ITwitter[]>) => {
    return dispatch(
      fetchTwitterSuccess(response.data as ITwitter[])
    );
  })
  .catch((error: ApiResponseType<any> | Error) => {
    return dispatch(
      fetchTwitterFailed(error)
    );
  });
};

export const thunkFetchTwitterDataByFilterDate = (
  period: IPeriod
): ThunkAction<void, AppState, null, Action<string>> => async (dispatch) => {
  const url = `http://`
    + `${process.env.NODE_ENV === 'development' ? process.env.REACT_APP_API_SERVER_HOST : window.location.hostname}`
    + `:${process.env.REACT_APP_API_SERVER_PORT}/api/twitter/filterDate?`
    + `${paramsToQueryString(period)}`;
  return axios({
    method: 'GET',
    url: url,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    responseType: 'json'
  })
  .then(async (response) => {
    if (response.status === 200) {
      return response.data;
    } else {
      throw response.data
    }
  },
  (error) => {
    if (error.response && error.response.data) {
      throw error.response.data;
    }
    else {
      const errmess = new Error(error.message);
      throw errmess;
    }
  })
  .then((response: ApiResponseType<ITwitter[]>) => {
    return dispatch(
      fetchTwitterSuccess(response.data as ITwitter[])
    );
  })
  .catch((error: ApiResponseType<any> | Error) => {
    return dispatch(
      fetchTwitterFailed(error)
    );
  });
};


export const thunkFetchCommentData = (
  data: {
    periodType: PeriodTabs
  }
): ThunkAction<void, AppState, null, Action<string>> => async (dispatch) => {
  const url = `http://`
    + `${process.env.NODE_ENV === 'development' ? process.env.REACT_APP_API_SERVER_HOST : window.location.hostname}`
    + `:${process.env.REACT_APP_API_SERVER_PORT}/api/comments?`
    + `${paramsToQueryString(data)}`;
  return axios({
    method: 'GET',
    url: url,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    responseType: 'json'
  })
  .then(async (response) => {
    if (response.status === 200) {
      return response.data;
    } else {
      throw response.data
    }
  },
  (error) => {
    if (error.response && error.response.data) {
      throw error.response.data;
    }
    else {
      const errmess = new Error(error.message);
      throw errmess;
    }
  })
  .then((response: ApiResponseType<ICommentResponse>) => {
    return dispatch(
      fetchCommentsSuccess(response.data as ICommentResponse)
    );
  })
  .catch((error: ApiResponseType<any> | Error) => {
    return dispatch(
      fetchCommentsFailed(error)
    );
  });
};

export const thunkFetchCommentDataByFilterDate = (
  period: IPeriod
): ThunkAction<void, AppState, null, Action<string>> => async (dispatch) => {
  const url = `http://`
    + `${process.env.NODE_ENV === 'development' ? process.env.REACT_APP_API_SERVER_HOST : window.location.hostname}`
    + `:${process.env.REACT_APP_API_SERVER_PORT}/api/comments/filterDate?`
    + `${paramsToQueryString(period)}`;
  return axios({
    method: 'GET',
    url: url,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    responseType: 'json'
  })
  .then(async (response) => {
    if (response.status === 200) {
      return response.data;
    } else {
      throw response.data
    }
  },
  (error) => {
    if (error.response && error.response.data) {
      throw error.response.data;
    }
    else {
      const errmess = new Error(error.message);
      throw errmess;
    }
  })
  .then((response: ApiResponseType<ICommentResponse>) => {
    return dispatch(
      fetchCommentsSuccess(response.data as ICommentResponse)
    );
  })
  .catch((error: ApiResponseType<any> | Error) => {
    return dispatch(
      fetchCommentsFailed(error)
    );
  });
};


export const thunkFetchTrendsData = (
  data: {
    periodType: PeriodTabs
  }
): ThunkAction<void, AppState, null, Action<string>> => async (dispatch) => {
  const url = `http://`
    + `${process.env.NODE_ENV === 'development' ? process.env.REACT_APP_API_SERVER_HOST : window.location.hostname}`
    + `:${process.env.REACT_APP_API_SERVER_PORT}/api/trends?`
    + `${paramsToQueryString(data)}`;
  return axios({
    method: 'GET',
    url: url,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    responseType: 'json'
  })
  .then(async (response) => {
    if (response.status === 200) {
      return response.data;
    } else {
      throw response.data
    }
  },
  (error) => {
    if (error.response && error.response.data) {
      throw error.response.data;
    }
    else {
      const errmess = new Error(error.message);
      throw errmess;
    }
  })
  .then((response: ApiResponseType<ITrendsResponse>) => {
    return dispatch(
      fetchTrendsSuccess(response.data as ITrendsResponse)
    );
  })
  .catch((error: ApiResponseType<any> | Error) => {
    return dispatch(
      fetchTrendsFailed(error)
    );
  });
};

export const thunkFetchTrendsDataByFilterDate = (
  period: IPeriod
): ThunkAction<void, AppState, null, Action<string>> => async (dispatch) => {
  const url = `http://`
    + `${process.env.NODE_ENV === 'development' ? process.env.REACT_APP_API_SERVER_HOST : window.location.hostname}`
    + `:${process.env.REACT_APP_API_SERVER_PORT}/api/trends/filterDate?`
    + `${paramsToQueryString(period)}`;
  return axios({
    method: 'GET',
    url: url,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    responseType: 'json'
  })
  .then(async (response) => {
    if (response.status === 200) {
      return response.data;
    } else {
      throw response.data
    }
  },
  (error) => {
    if (error.response && error.response.data) {
      throw error.response.data;
    }
    else {
      const errmess = new Error(error.message);
      throw errmess;
    }
  })
  .then((response: ApiResponseType<ITrendsResponse>) => {
    return dispatch(
      fetchTrendsSuccess(response.data as ITrendsResponse)
    );
  })
  .catch((error: ApiResponseType<any> | Error) => {
    return dispatch(
      fetchTrendsFailed(error)
    );
  });
};

export const thunkFetchRssDataByTime = (
  timestamp: number
): ThunkAction<void, AppState, null, Action<string>> => async (dispatch) => {
  const url = `http://`
    + `${process.env.NODE_ENV === 'development' ? process.env.REACT_APP_API_SERVER_HOST : window.location.hostname}`
    + `:${process.env.REACT_APP_API_SERVER_PORT}/api/rss/${timestamp}`;
  return axios({
    method: 'GET',
    url: url,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    responseType: 'json'
  })
  .then(async (response) => {
    if (response.status === 200) {
      return response.data;
    } else {
      throw response.data
    }
  },
  (error) => {
    if (error.response && error.response.data) {
      throw error.response.data;
    }
    else {
      const errmess = new Error(error.message);
      throw errmess;
    }
  })
  .then((response: ApiResponseType<IRssTable[]>) => {
    return dispatch(
      fetchRssByTimeSuccess(response.data as IRssTable[])
    );
  })
  .catch((error: ApiResponseType<any> | Error) => {
    return dispatch(
      fetchRssByTimeFailed(error)
    );
  });
};


export const thunkSetAutoMovingValue = (
  data: IRadioAutoMovingValue
): ThunkAction<void, AppState, null, Action<string>> => async (dispatch) => {
    return dispatch(
      setAutoMovingRadio(data)
    );
};

export const thunkSetAllDisableMovingValue = (
): ThunkAction<void, AppState, null, Action<string>> => async (dispatch) => {
    return dispatch(
      setAllDisableAutoMovingRadio()
    );
};