import { ApiResponseType } from '../Types';
import {
    FETCH_TWITTER_SUCCESS,
    FETCH_TWITTER_FAILED,
    ITwitter,
    IRssTable,
    FETCH_RSS_BY_DATETIME_SUCCESS,
    FETCH_RSS_BY_DATETIME_FAILED,
    IComment,
    FETCH_COMMENTS_SUCCESS,
    FETCH_COMMENTS_FAILED,
    ITrendsResponse,
    FETCH_TRENDS_SUCCESS,
    FETCH_TRENDS_FAILED,
    IRadioAutoMovingValue,
    SET_RADIO_AUTO_MOVING,
    SET_ALL_DISABLE_RADIO_AUTO_MOVING,
    ICommentResponse,
} from './main.types';


export function fetchTwitterSuccess(data: ITwitter[]) {
    return {
        type: FETCH_TWITTER_SUCCESS,
        payload: data
    };
}
export function fetchTwitterFailed(error: ApiResponseType<any> | Error) {
    return {
        type: FETCH_TWITTER_FAILED,
        payload: error
    };
}

export function fetchCommentsSuccess(data: ICommentResponse) {
    return {
        type: FETCH_COMMENTS_SUCCESS,
        payload: data
    };
}
export function fetchCommentsFailed(error: ApiResponseType<any> | Error) {
    return {
        type: FETCH_COMMENTS_FAILED,
        payload: error
    };
}

export function fetchTrendsSuccess(data: ITrendsResponse) {
    return {
        type: FETCH_TRENDS_SUCCESS,
        payload: data
    };
}
export function fetchTrendsFailed(error: ApiResponseType<any> | Error) {
    return {
        type: FETCH_TRENDS_FAILED,
        payload: error
    };
}

export function fetchRssByTimeSuccess(data: IRssTable[]) {
    return {
        type: FETCH_RSS_BY_DATETIME_SUCCESS,
        payload: data
    };
}
export function fetchRssByTimeFailed(error: ApiResponseType<any> | Error) {
    return {
        type: FETCH_RSS_BY_DATETIME_FAILED,
        payload: error
    };
}

export function setAutoMovingRadio(data: IRadioAutoMovingValue) {
    return {
        type: SET_RADIO_AUTO_MOVING,
        payload: data
    };
}

export function setAllDisableAutoMovingRadio() {
    return {
        type: SET_ALL_DISABLE_RADIO_AUTO_MOVING,
    };
}