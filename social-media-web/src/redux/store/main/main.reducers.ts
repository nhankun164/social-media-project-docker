import { ChartNameEnum } from '../../../constants/enum';
import {
    FETCH_COMMENTS_FAILED,
    FETCH_COMMENTS_SUCCESS,
    FETCH_RSS_BY_DATETIME_FAILED,
    FETCH_RSS_BY_DATETIME_SUCCESS,
    FETCH_TRENDS_FAILED,
    FETCH_TRENDS_SUCCESS,
    FETCH_TWITTER_FAILED,
    FETCH_TWITTER_SUCCESS,
    IComment,
    IMainState,
    IRssTable,
    ITrendsResponse,
    ITwitter,
    MainActionTypes,
    SET_ALL_DISABLE_RADIO_AUTO_MOVING,
    SET_RADIO_AUTO_MOVING,
} from './main.types';

const initialState: IMainState = {
    twitterList: [] as ITwitter[],
    commentList: {
        data: [] as IComment[]
    },
    rssByTimeList: [] as IRssTable[],
    autoMovingRadio: {
        emotion: 1,
        sentiment: 1,
        comment: 1,
        twitter: 1
    },
    trends: {
        emotion: [],
        sentiment: [],
        emotion_count: undefined,
        sentiment_count: undefined
    } as ITrendsResponse
};

export function mainReducer(
    state = initialState,
    action: MainActionTypes
): IMainState {
    switch (action.type) {
        case FETCH_TWITTER_SUCCESS:
            return {
                ...state,
                twitterList: action.payload
            }
        case FETCH_TWITTER_FAILED:
            return {
                ...state,
                error: action.payload
            }
        case FETCH_COMMENTS_SUCCESS:
            return {
                ...state,
                commentList: action.payload
            }
        case FETCH_COMMENTS_FAILED:
            return {
                ...state,
                error: action.payload
            }
        case FETCH_TRENDS_SUCCESS:
            return {
                ...state,
                trends: action.payload
            }
        case FETCH_TRENDS_FAILED:
            return {
                ...state,
                error: action.payload
            }
        case FETCH_RSS_BY_DATETIME_SUCCESS:
            return {
                ...state,
                rssByTimeList: action.payload
            }
        case FETCH_RSS_BY_DATETIME_FAILED:
            return {
                ...state,
                error: action.payload
            }
        case SET_RADIO_AUTO_MOVING:
            if (action.payload.type === ChartNameEnum.Twitter) {
                state.autoMovingRadio.twitter = action.payload.value;
            }
            else if (action.payload.type === ChartNameEnum.Comment) {
                state.autoMovingRadio.comment = action.payload.value;
            }
            else if (action.payload.type === ChartNameEnum.Emotion) {
                state.autoMovingRadio.emotion = action.payload.value;
            }
            else if (action.payload.type === ChartNameEnum.Sentiment) {
                state.autoMovingRadio.sentiment = action.payload.value;
            }
            return state
        case SET_ALL_DISABLE_RADIO_AUTO_MOVING:
            state.autoMovingRadio.comment = 0;
            state.autoMovingRadio.emotion = 0;
            state.autoMovingRadio.sentiment = 0;
            state.autoMovingRadio.twitter = 0;
            return state;
        default:
            return state;
    }
}
