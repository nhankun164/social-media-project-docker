import { ApiResponseType } from '../Types';

export const UPDATE_LOCALE = 'UPDATE_LOCALE';
export const ADD_COLUMN_SUCCESS = 'ADD_COLUMN_SUCCESS';
export const ADD_COLUMN_FAILED = 'ADD_COLUMN_FAILED';
export const FETCH_TWITTER_SUCCESS = 'FETCH_TWITTER_SUCCESS';
export const FETCH_TWITTER_FAILED = 'FETCH_TWITTER_FAILED';

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILED = 'FETCH_COMMENTS_FAILED';

export const FETCH_TRENDS_SUCCESS = 'FETCH_TRENDS_SUCCESS';
export const FETCH_TRENDS_FAILED = 'FETCH_TRENDS_FAILED';

export const FETCH_RSS_BY_DATETIME_SUCCESS = 'FETCH_RSS_BY_DATETIME_SUCCESS';
export const FETCH_RSS_BY_DATETIME_FAILED = 'FETCH_RSS_BY_DATETIME_FAILED';

export const SET_CURRENT_PERIOD_TIME = 'SET_CURRENT_PERIOD_TIME';
export const SET_RADIO_AUTO_MOVING = 'SET_RADIO_AUTO_MOVING';
export const SET_ALL_DISABLE_RADIO_AUTO_MOVING = 'SET_ALL_DISABLE_RADIO_AUTO_MOVING';
export interface IMainState {
    twitterList: ITwitter[];
    commentList: ICommentResponse;
    rssByTimeList: IRssTable[]; 
    autoMovingRadio: IRadioGroupAutoMoving;
    trends: ITrendsResponse;
    error?: ApiResponseType<any> | Error;
}

export interface ITrendsResponse {
    emotion: ITrendData[];
    sentiment: ITrendData[];
    emotion_count?: ITrendCountObject;
    sentiment_count?: ITrendCountObject;
}

export interface ICommentResponse {
    emotion_count?: ITrendCountObject;
    sentiment_count?: ITrendCountObject;
    data: IComment[];
}

export interface ITrendData {
    category: string;
    categoryName: string;
    data: number[][];
}

export interface ITrendCountObject {
    [key: string]: number;
}

export interface IPeriod {
    startDate: number | null;
    endDate: number | null;
}

export interface IRadioGroupAutoMoving {
    emotion: number;
    sentiment: number;
    comment: number;
    twitter: number;
    [key: string]: number;
}

export interface IRadioAutoMovingValue {
    type: string;
    value: number;
}

export interface ITwitter {
    id : number;
    tweet_id : string;
    create_date : string;
    create_date_unix : number;
    url : string;
    tweet_type : string;
    tweet_text : string;
    tweet_favorite_count : number;
    user_follower_count : number;
    quoted_count : number;
    topic_id : number;
    processed : boolean;
    country : string;
    locality : string;
    tweet_img : string;
    hash_tags : string;
    source : string;
    relevance : number;
    retweet_count : number;
    user_id : string;
    user_following_count : number;
    region : string;
    sub_region : string;
    geo_full_name : string;
    latitude : string;
    longitude : string;
}

export interface IRssTable {
    id: number;
    title: string;
    source_link: string;
    date_created: string;
    processed: boolean;
    topic_id: number;
    date_create_unix: number;
    image_src: string | null;
}


export interface IComment {
    id: number;
    post_id: string;
    create_date_unix: number;
    topic_id: number;
    relevance: number;
    content: string;
    source: string;
    source_link: string;
    ne_data: string;
    senti_type: number;
    senti_flag: number;
    senti_val: number;
    pos_words: string;
    neg_words: string;
    senti_word_flag: number;
    hash_tags: string;
    refined_text: string;
    is_refined: number;
    anger: number;
    anxiety: number;
    sadness: number;
    satisfaction: number;
    happiness: number;
    excitement: number;
    sarcasm_score: number;
    sarcasm_flag: number;
    crystalfeel_out: string;
    crystalfeel_flag: number;
    anger_score: number;
    fear_score: number;
    joy_score: number;
    sadness_score: number;
    valence: number;
    emotion_score: number;
    sentiment_score: number;
    emotion_category: string;
    sentiment_category: string;
    tweet_id: string;
}



interface ITwitterFetchSuccessAction {
    type: typeof FETCH_TWITTER_SUCCESS;
    payload: ITwitter[];
}
interface ITwitterFetchFailedAction {
    type: typeof FETCH_TWITTER_FAILED;
    payload: ApiResponseType<any> | Error;
}

interface ICommentFetchSuccessAction {
    type: typeof FETCH_COMMENTS_SUCCESS;
    payload: ICommentResponse;
}
interface ICommentFetchFailedAction {
    type: typeof FETCH_COMMENTS_FAILED;
    payload: ApiResponseType<any> | Error;
}

interface ITrendsFetchSuccessAction {
    type: typeof FETCH_TRENDS_SUCCESS;
    payload: ITrendsResponse;
}
interface ITrendsFetchFailedAction {
    type: typeof FETCH_TRENDS_FAILED;
    payload: ApiResponseType<any> | Error;
}

interface IRssByTimeFetchSuccessAction {
    type: typeof FETCH_RSS_BY_DATETIME_SUCCESS;
    payload: IRssTable[];
}
interface IRssByTimeFetchFailedAction {
    type: typeof FETCH_RSS_BY_DATETIME_FAILED;
    payload: ApiResponseType<any> | Error;
}

interface ICurrentPeriodSettingAction {
    type: typeof SET_CURRENT_PERIOD_TIME;
    payload: IPeriod;
}

interface IAutomovingRadioSettingAction {
    type: typeof SET_RADIO_AUTO_MOVING;
    payload: IRadioAutoMovingValue;
}

interface IDisableMovingRadioSettingAction {
    type: typeof SET_ALL_DISABLE_RADIO_AUTO_MOVING;
}

export type MainActionTypes = ITwitterFetchSuccessAction
    | ITwitterFetchFailedAction
    | IRssByTimeFetchSuccessAction
    | IRssByTimeFetchFailedAction
    | ICommentFetchSuccessAction
    | ICommentFetchFailedAction
    | ITrendsFetchSuccessAction
    | ITrendsFetchFailedAction
    | ICurrentPeriodSettingAction
    | IAutomovingRadioSettingAction
    | IDisableMovingRadioSettingAction;
