import { StatusCode } from "../../constants/enum";

export interface ApiResponseType<T> {
    data?: T;
    code: StatusCode;
    message: String;
    stackTraces?: String;
}
