import { ApiResponseType } from "../Types";
import { IS_LOADING, IS_SHOW, SET_ERROR, SET_ERROR_MESSAGES, SET_MESSAGES, SET_SUCCESS_MESSAGES } from "./messages.types";


export function setLoading(result: boolean) {
    return {
        type: IS_LOADING,
        payload: result
    };
}
export function setMessage(result?: string) {
    return {
        type: SET_MESSAGES,
        payload: result
    };
}
export function setSuccessMessage(result?: string) {
    return {
        type: SET_SUCCESS_MESSAGES,
        payload: result
    };
}
export function setErrorMessage(result?: ApiResponseType<any> | Error) {
    return {
        type: SET_ERROR_MESSAGES,
        payload: result
    };
}
export function setError(result: boolean) {
    return {
        type: SET_ERROR,
        payload: result
    };
}
export function setShowMessage(result: boolean) {
    return {
        type: IS_SHOW,
        payload: result
    };
}