import { MessageType } from "../../../constants/enum";
import { IMessagesState, IS_LOADING, IS_SHOW, MessagesActionTypes, SET_ERROR, SET_ERROR_MESSAGES, SET_MESSAGES, SET_SUCCESS_MESSAGES } from "./messages.types";

const initialState: IMessagesState = {
    nLoadingProcess: 0,
    messageLoading: undefined,
    messageError: undefined,
    isError: false,
    isShow: false,
    type: MessageType.INFO
}
export function messageReducer(
    state: IMessagesState = initialState,
    action: MessagesActionTypes
): IMessagesState {
    switch (action.type) {
        case IS_LOADING:
            if (action.payload) {
                state.nLoadingProcess++;

            } else {
                state.nLoadingProcess--;

            }
            return {
                ...state,
            }
        case IS_SHOW:
            state.isShow = action.payload;
            return { ...state };
        case SET_MESSAGES:
            state.type = MessageType.INFO;
            state.messageLoading = action.payload;
            return { ...state };
        case SET_SUCCESS_MESSAGES:
            state.type = MessageType.SUCCESS;
            state.messageSuccess = action.payload;
            return { ...state };
        case SET_ERROR_MESSAGES:
            state.type = MessageType.ERROR;
            state.messageError = action.payload;
            return { ...state };
        case SET_ERROR:
            state.isError = action.payload;
            return { ...state };
        default:
            return state;
    }
}
