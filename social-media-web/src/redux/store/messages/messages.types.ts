import { MessageType } from "../../../constants/enum";
import { ApiResponseType } from "../Types";

export const IS_LOADING = 'IS_LOADING';
export const SET_MESSAGES = 'SET_MESSAGES';
export const SET_ERROR_MESSAGES = 'SET_ERROR_MESSAGES';
export const SET_ERROR = 'SET_ERROR';
export const IS_SHOW = 'IS_SHOW';
export const SET_SUCCESS_MESSAGES = 'SET_SUCCESS_MESSAGES';

export interface IMessagesState {
    nLoadingProcess: number,
    messageLoading?: string,
    messageSuccess?: string,
    messageError?: ApiResponseType<any> | Error,
    isError: boolean,
    isShow: boolean,
    type: MessageType
}
interface IShow {
    type: typeof IS_SHOW;
    payload: boolean;
}
interface INLoadingProcess {
    type: typeof IS_LOADING;
    payload: number;
}
interface ILoadedMessaging {
    type: typeof SET_MESSAGES;
    payload: string;
}
interface IErrorMessaging {
    type: typeof SET_ERROR_MESSAGES;
    payload: ApiResponseType<any> | Error;
}
interface ISuccessMessaging {
    type: typeof SET_SUCCESS_MESSAGES;
    payload: string;
}
interface IError {
    type: typeof SET_ERROR;
    payload: boolean;
}

export type MessagesActionTypes =
    | INLoadingProcess
    | ILoadedMessaging
    | IError
    | ISuccessMessaging
    | IErrorMessaging
    | IShow
    ;
