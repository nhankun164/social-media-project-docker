import { Action } from "redux";
import { ThunkAction } from "redux-thunk";
import { AppState } from "..";
import { ApiResponseType } from "../Types";
import { setError, setErrorMessage, setLoading, setMessage, setShowMessage, setSuccessMessage } from "./messages.actions";

export const thunkSetShowMessage = (
    result: boolean
): ThunkAction<void, AppState, null, Action<string>> => (dispatch) => {
    return dispatch(setShowMessage(result))
};
export const thunkSetLoading = (
    result: boolean
): ThunkAction<void, AppState, null, Action<string>> => (dispatch) => {
    return dispatch(setLoading(result))
};
export const thunkSetError = (
    result: boolean
): ThunkAction<void, AppState, null, Action<string>> => (dispatch) => {
    return dispatch(setError(result))
};
export const thunkSetMessage = (
    result?: string
): ThunkAction<void, AppState, null, Action<string>> => (dispatch) => {
    return dispatch(setMessage(result))
};
export const thunkSetSuccessMessage = (
    result: string
): ThunkAction<void, AppState, null, Action<string>> => (dispatch) => {
    return dispatch(setSuccessMessage(result))
};
export const thunkSetErrorMessage = (
    result: ApiResponseType<any> | Error
): ThunkAction<void, AppState, null, Action<string>> => (dispatch) => {
    return dispatch(setErrorMessage(result))
};