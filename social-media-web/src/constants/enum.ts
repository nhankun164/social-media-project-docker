export enum StatusCode {
    INTERNAL_SERVER_ERROR = '500',
    BAD_REQUEST = '400',
    UNAUTHORIZED = '401',
    FORBIDDEN = '403',
    NOT_FOUND = '404',
    CONFLICT = '409',
    OK = '200'
}
export enum MessageType {
    SUCCESS = "success",
    WARNING = "warning",
    INFO = "info",
    ERROR = "error"
}

export enum PeriodTabs {
    History = 'History',
    Current = 'Current',
    Prediction = 'Prediction'
}

export enum ChartNameEnum {
    Emotion = 'Emotion' , 
    Sentiment = 'Sentiment', 
    Comment = 'Comment', 
    Twitter = 'Twitter'
}