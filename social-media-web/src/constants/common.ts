export const SECRET_KEY_CRYPT = 'social_media_key';
export const NO_DATA_AVAILABLE = 'No Data Available';
export const CommaSeparator = ', ';
export const AutoMovingDuration = 9000;