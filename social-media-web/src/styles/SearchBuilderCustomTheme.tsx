import { Button } from 'antd';
import { ActionProps, NotToggleProps, ValueEditorProps, ValueSelectorProps } from 'react-querybuilder';
import { Checkbox, Input, Radio, Select } from 'antd';


export const AntDActionElement = ({ className, handleOnClick, label, title }: ActionProps) => (
  <Button type="primary" className={className} title={title} onClick={(e) => handleOnClick(e)}>
    {label}
  </Button>
);

AntDActionElement.displayName = 'AntDActionElement';




export const AntDNotToggle = ({ className, handleOnChange, checked }: NotToggleProps) => {
  return (
    <Checkbox
      className={className}
      onChange={(e) => handleOnChange(e.target.checked)}
      checked={!!checked}>
      Not
    </Checkbox>
  );
};

AntDNotToggle.displayName = 'AntDNotToggle';



const { Option } = Select;

export const AntDValueEditor = ({
  operator,
  value,
  handleOnChange,
  title,
  className,
  type,
  inputType,
  values
}: ValueEditorProps) => {
  if (operator === 'null' || operator === 'notNull') {
    return null;
  }

  switch (type) {
    case 'select':
      return (
        <Select className={className} onChange={(v) => handleOnChange(v)} value={value}>
          {values!.map((v) => (
            <Option key={v.name} value={v.name}>
              {v.label}
            </Option>
          ))}
        </Select>
      );

    case 'checkbox':
      return (
        <Checkbox
          type="checkbox"
          className={className}
          onChange={(e) => handleOnChange(e.target.checked)}
          checked={!!value}
        />
      );

    case 'radio':
      return (
        <span className={className} title={title}>
          {values!.map((v) => (
            <Radio
              key={v.name}
              value={v.name}
              checked={value === v.name}
              onChange={(e) => handleOnChange(e.target.value)}>
              {v.label}
            </Radio>
          ))}
        </span>
      );

    default:
      return (
        <Input
          type={inputType || 'text'}
          value={value}
          title={title}
          className={className}
          onChange={(e) => handleOnChange(e.target.value)}
        />
      );
  }
};

AntDValueEditor.displayName = 'AntDValueEditor';



export const AntDValueSelector = ({ className, handleOnChange, options, value }: ValueSelectorProps) => (
  <Select className={className} value={value} onChange={(v) => handleOnChange(v)}>
    {options.map((option) => {
      const key = option.id ? `key-${option.id}` : `key-${option.name}`;
      return (
        <Option key={key} value={option.name}>
          {option.label}
        </Option>
      );
    })}
  </Select>
);

AntDValueSelector.displayName = 'AntDValueSelector';

