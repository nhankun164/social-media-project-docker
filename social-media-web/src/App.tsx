import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import configureStore from './redux/store';
import Routes from './components/Routes';
import './App.css';



const store = configureStore();
const App: React.FC = () => {
  return (
    <Provider store={store}>
          <BrowserRouter>
            <div>
              <header>
                {/* <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500&display=swap' />
              <link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons' /> */}
                <link href='https://fonts.googleapis.com/css?family=Roboto&display=swap' rel='stylesheet'></link>
                <meta name='viewport' content='minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no' />
              </header>
              <Routes />
            </div>
          </BrowserRouter>
    </Provider>
  );
}

export default App;
