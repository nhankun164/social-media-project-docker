import json
import random
from datetime import timedelta, date, datetime
import calendar

def daterange_day(start_date, end_date):
    for n in range(int((end_date - start_date).days) + 1):
        yield start_date + timedelta(n)

def daterange_hour(start_date, end_date):
    delta = timedelta(hours=1)
    while start_date < end_date:
        yield start_date
        start_date += delta

def filter_data_by_date(orig_data, from_date, to_date):
    '''

    :param orig_data: list(datetime, count)
    :param from_date: date
    :param to_date: date
    :return:
    '''
    message = 'success'
    new_data = []
    for dt, count in orig_data:
        if from_date <= dt.date() <= to_date:
            new_data.append([dt, count])
    return new_data, message

def aggregate_data_by_unit(orig_data, unit):
    '''

    :param orig_data: tuple(timestamp, count) for 1 series
    :param unit: 'day' or 'month' or 'total'
    :return:
    '''
    message = 'success'
    if unit == 'day':
        i = 0
        day_count = 0
        data = []
        prev_ts, prev_count = orig_data[0]
        prev_dt = prev_ts.date()
        day_count += prev_count
        while True:
            # print('i', i)
            if i > len(orig_data)-2:
                data.append((prev_dt, day_count))
                break
            # if i == 0:
            #     prev_ts, prev_count = orig_data[0]
            #     prev_dt = prev_ts.date()
            #     day_count += prev_count
            i += 1
            cur_ts, cur_count = orig_data[i]
            cur_dt = cur_ts.date()
            if prev_dt == cur_dt:
                day_count += cur_count
            else:
                data.append((prev_dt, day_count))
                prev_dt = cur_dt
                day_count = cur_count
            # print('cur_dt', cur_dt, 'cur_count', cur_count, 'day_count', day_count)
        # print(data)

    elif unit == 'month':
        # print('here at month', len(orig_data))
        i = 0
        month_count = 0
        data = []
        prev_ts, prev_count = orig_data[0]
        prev_dt = prev_ts.date()
        month_count += prev_count
        while True:
            # print('i', i)
            if i > len(orig_data) - 2:
                yr_dt = datetime(prev_dt.year, prev_dt.month, 1, 0, 00)
                data.append((yr_dt, month_count))
                break
            # if i == 0:
            #     prev_ts, prev_count = orig_data[0]
            #     prev_dt = prev_ts.date()
            #     month_count += prev_count
            i += 1
            cur_ts, cur_count = orig_data[i]
            cur_dt = cur_ts.date()
            if (prev_dt.year == cur_dt.year) and ((prev_dt.month == cur_dt.month)):
                month_count += cur_count
            else:
                yr_dt = datetime(prev_dt.year, prev_dt.month, 1, 0, 00)
                data.append((yr_dt, month_count))
                prev_dt = cur_dt
                month_count = cur_count
        #     print('cur_dt', cur_dt, 'cur_count', cur_count, 'day_count', month_count)
        # print(data)

    elif unit == 'total':
        i = 0
        total_count = 0
        data = []
        # print(len(data102[0]['data']) - 1)
        while True:
            # print('i', i)
            if i >= len(orig_data):
                data.append(('total', total_count))
                break
            total_count += orig_data[i][1]
            i += 1
    elif unit == 'hour':
        message = 'success'
        return orig_data, message
    else:
        message = 'Warning: unit not recognized! Enter: "day", "month", or "total"!'
        return orig_data, message

    return data, message


def parse_datestring(datestring):
    d = [int(i) for i in datestring.split('-')]
    return date(d[0], d[1], d[2])

def timestamp_to_datetime(orig_data):
    new_data = orig_data.copy()
    # print('orig_data', orig_data[1])
    for i, series in enumerate(orig_data):
        # print('series', series['data'])
        temp_data = []
        for key, val in enumerate(series['data']):
            timestamp, count = val
            dt = datetime.fromtimestamp(timestamp)
            temp_data.append((dt, count))
        new_data[i]['data'] = temp_data
    return new_data


def get_overall_chart(data, request, sources_cat, sources_config):
    cat_names = {
        'positive': 'Positive Mentions',
        'very positive': 'Very Positive Mentions',
        'negative': 'Negative Mentions',
        'very negative': 'Very Negative Mentions',
        'neutral': 'Neutral Mentions',
        'joy': 'Joy',
        'fear': 'Fear',
        'sadness': 'Sadness',
        'anger': 'Anger'
    }
    if not request.args.get('chartId'):
        message = 'Error: chartId not provided'
        return None, message
    if not request.args.get('topicId'):
        message = 'Error: topicId not provided'
        return None, message
    if not request.args.get('fromDate'):
        message = 'Error: fromDate not provided'
        return None, message
    if not request.args.get('toDate'):
        message = 'Error: toDate not provided'
        return None, message
    # if not request.args.get('sources'):
    #     print('here no sources')
    #     message = 'Error: sources not provided'
    #     return None, message

    message = 'success'
    if int(request.args.get('chartId')) in [100, 101, 102, 103, 104, 105, 107, 108, 106]: # and request.args.get('fromDate') and request.args.get('toDate') and request.args.get('topicId'):  # and request.args.get('sources')
        if int(request.args.get('chartId')) in [100, 102, 104]: #sentiments count
            label = 'sentiment'
        elif int(request.args.get('chartId')) in [101, 103, 105, 108, 106]: #emotions count
            label = 'emotion'
        else:
            label = 'finegrained'

        if int(request.args.get('chartId')) in [100, 101]:
            data_label = 'count'
        else:
            data_label = 'data'

        topicId = int(request.args.get('topicId'))
        if request.args.get('sources'):
            sources = [s.lower().strip() for s in request.args.get('sources').split(',')]
        else:
            sources = ['all']

        if request.args.get('unit'):
            unit = request.args.get('unit')
        else:
            unit = 'hour'

        final_data = []
        for cat in data[0][label]:
            data_per_cat = {}
            data_per_cat['category'] = cat
            data_per_cat['categoryName'] = cat_names[cat]
            if int(request.args.get('chartId')) in [100, 101]:
                data_per_cat['change'] = round(random.uniform(-1, 1), 2)
            data_per_cat[data_label] = []
            data, message = filter_data_by_source(data, sources, sources_cat, sources_config)
            if int(request.args.get('chartId')) in [106]:
                for d in data:
                    if d['topicId'] == topicId:  # filtering by topicId
                        dt = datetime.strptime(d['datetime'], '%Y-%m-%d %H:%M:%S')
                        data_per_cat[data_label].append([dt, d[label][cat]])
            else:
                for d in data:
                    if d['topicId'] == topicId and cat == d[label+'Label']: # filtering by topicId
                        # print(d)
                        dt = datetime.strptime(d['datetime'], '%Y-%m-%d %H:%M:%S')
                        data_per_cat[data_label].append([dt, 1])
                        # print('label', label)
                        # print('cat', cat)
            final_data.append(data_per_cat)
        # print('final_data', final_data)
        # for d in final_data:
        #     print(d['category'], len(d['data']))

        fromDate = parse_datestring(request.args.get('fromDate'))
        toDate = parse_datestring(request.args.get('toDate'))
        data_found = True
        for series in final_data:
            series[data_label], message = filter_data_by_date(series[data_label], fromDate, toDate)
            n = len(series[data_label])
            if series[data_label]:
                series[data_label], message= aggregate_data_by_unit(series[data_label], unit)
                if series[data_label]:
                    if int(request.args.get('chartId')) in [106] and unit == 'total':
                        temp_data = []
                        avg = series[data_label][0][1] / n
                        temp_data.append((series[data_label][0][0], avg))
                        series[data_label] = temp_data
                    else:
                        series[data_label] = series[data_label]
                else:
                    if int(request.args.get('chartId')) in [100, 101]:
                        data_found = False
                    # else:
                    #     return final_data, message
            else:  # return original data if date range is not within the data
                if int(request.args.get('chartId')) in [100, 101]:
                    data_found = False
                # else:
                #     return final_data, message
        # print('final_data', final_data)
        # special case of no data found for chartId = 100, 101
        if int(request.args.get('chartId')) in [100, 101] and unit == 'total' and not data_found:
            for cat in final_data:
                cat['change'] = 0
                cat['count'] = 0

        # add 'total' in charts 102 (can be done for 103 also, but frontend does not expect it)
        if int(request.args.get('chartId')) in [102] and final_data[0]['data']:
            final_data = create_total_data_in_trend(final_data, fromDate, toDate, unit)
        elif int(request.args.get('chartId')) in [102]:
                final_data.append({
                    'category': 'total',
                    'categoryName': 'Total Mentions',
                    'data': []
                })

        # print(final_data)
        # reformat 100 and 101
        if int(request.args.get('chartId')) in [100, 101] and unit == 'total':
            # add 'total' in chart 100 (can be done for 101 also, but frontend does not expect it)
            # print(final_data)
            if int(request.args.get('chartId')) == 100:
                final_data = create_total_data_in_total(final_data, data_label)
            for series in final_data:
                if series[data_label]:
                    series[data_label] = series[data_label][0][1]

        # reformat 104, 105, 107
        if int(request.args.get('chartId')) in [104, 105, 107] and unit == 'total':
            temp_data = []
            final_data_with_count = {}
            final_data = create_total_data_in_total(final_data, data_label)
            for series in final_data:
                if series['category'] != 'total': #exclude total from processing:
                    # print('series', series)
                    if series['data']:
                        temp_data.append([series['categoryName'], series['data'][0][1]])
                    else:
                        temp_data.append([series['categoryName'], 0])
                else:
                    final_data_with_count['count'] = series['data'][0][1]
            final_data_with_count['data'] = temp_data
            final_data = final_data_with_count

        # reformat 108, 106
        if int(request.args.get('chartId')) in [108, 106] and unit == 'total':
            temp_data = {}
            temp_data['categories'] = []
            temp_data['values'] = []
            for series in final_data:
                # print('series', series)
                temp_data['categories'].append(series['category'])
                if series['data']:
                    temp_data['values'].append(round(series['data'][0][1], 2))
                else:
                    temp_data['values'].append(0)
            final_data = temp_data

        return final_data, message


def create_total_data_in_trend(data, fromDate, toDate, unit):
    # print(data)
    total_data = {
        'category': 'total',
        'categoryName': 'Total Mentions',
        'data': []
    }
    if unit == 'total':
        count = 0
        for d in data:
            count += d['data'][0][1]

        total_data['data'] = [('total', count)]

    elif unit == 'month':
        i = 0
        # print('fromDate', fromDate)
        cur_date = fromDate
        month_data = []
        while True:
            cur_date = add_months(cur_date, i)
            # print('cur_date', cur_date)
            if cur_date > toDate:
                break
            count = 0
            for d in data:
                for cat_data in d['data']:
                    if cat_data[0].month == cur_date.month:
                        # print('here', cat_data[1])
                        count += cat_data[1]
            if count != 0:
                month_data.append((datetime(cur_date.year, cur_date.month, cur_date.day), count))
            i += 1
        # print(month_data)
        total_data['data'] = month_data
    elif unit == 'day':
        cur_date = fromDate
        day_data = []
        while True:
            if cur_date > toDate:
                break
            count = 0
            for d in data:
                for cat_data in d['data']:
                    if (cat_data[0].month == cur_date.month) and (cat_data[0].year == cur_date.year) and (cat_data[0].day == cur_date.day):
                        # print('here', cat_data[1])
                        count += cat_data[1]
            if count != 0:
                day_data.append((datetime(cur_date.year, cur_date.month, cur_date.day), count))
            cur_date = add_days(cur_date, 1)
        # print(day_data)
        total_data['data'] = day_data

    else:
        cur_datetime = datetime(fromDate.year, fromDate.month, fromDate.day)
        toDate_dt = datetime(toDate.year, toDate.month, toDate.day)
        hour_data = []
        # print(add_hours(cur_datetime, 1))
        while True:
            if cur_datetime > toDate_dt:
                break
            count = 0
            for d in data:
                for cat_data in d['data']:
                    if cat_data[0] == cur_datetime:
                        # print('here', cur_datetime, cat_data[0], cat_data[1])
                        count += cat_data[1]
            if count != 0:
                hour_data.append((cur_datetime, count))
            cur_datetime = add_hours(cur_datetime, 1)
        # print(hour_data)
        total_data['data'] = hour_data
    data.append(total_data)
    return data

def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    # day = min(sourcedate.day, calendar.monthrange(year,month)[1])
    day = 1
    return date(year, month, day)

def add_days(sourcedate, days):
    return sourcedate + timedelta(days=days)

def add_hours(sourcedatetime, hours):
    return sourcedatetime + timedelta(hours=hours)


def create_total_data_in_total(data, data_label):
    total_data = {
        'category': 'total',
        'categoryName': 'Total Mentions',
        'change': 0,
    }
    count = 0
    for d in data:
        if d[data_label] == 0:
            count += 0
        else:
            for cat_count in d[data_label]:
                count += cat_count[1]

    total_data[data_label] = [('total', count)]
    if count != 0:
        total_data['change'] = round(random.uniform(-1, 1), 2)
    data.append(total_data)
    return data

def filter_data_by_source(orig_data, sources, sources_cat, sources_config):
    message = 'Error: No data for filtered sources found'
    new_data = []
    # print('sources_cat', sources_cat)

    # create a list of all sources required
    sources_list = []
    sources = [source.lower() for source in sources]
    # if '' in sources:
    #     for cat in sources_cat:
    #         sources_list += cat['sources']
    if 'all sources' in sources or 'all' in sources:
        for cat in sources_cat:
            sources_list += cat['sources']
    elif 'all news' in sources:
        s_list = next(item for item in sources_cat if item['sourceType'].lower() == 'all news')
        sources_list = s_list['sources']
    elif 'all social media' in sources:
        s_list = next(item for item in sources_cat if item['sourceType'].lower() == 'all social media')
        sources_list = s_list['sources']
    else:
        sources_list = sources
    sources_list = [source.lower() for source in sources_list]  #make sure all small letters
    # print('sources_list', sources_list)

    # remove from list those that are not configured
    updated_src_list = []
    for src in sources_config:
        if src['sourceName'].lower() in sources_list and src["selected"] == True:
            updated_src_list.append(src["sourceName"].lower())
        else:
            continue
    # print('updated_src_list', updated_src_list)

    for d in orig_data:
        # print(d['source'])
        # print('cat[sources]', cat['sources'])
        if d['source'].lower() in updated_src_list:
            new_data.append(d)
            message = 'success'

    return new_data, message
