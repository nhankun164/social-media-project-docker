import os, sys
# sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
from .base_chart import BaseChart
from .backend_logger import BackendLogger
SCRIPT_NAME = os.path.basename(__file__)
LOG_NAME = SCRIPT_NAME.replace(".py","")

class SentimentCountChart(BaseChart):

    CHANGE_INDEX_TOTAL_MENTIONS = 0
    CHANGE_INDEX_POSITIVE_MENTIONS = 1
    CHANGE_INDEX_NEGATIVE_MENTIONS = 2
    CHANGE_INDEX_NEUTRAL_MENTIONS = 3
    INDEX_TOTAL_COUNT = 0
    SUCCESS_CODE = 200

    def __init__(self, **kwargs):
        super().__init__(kwargs['fromDate'], kwargs['toDate'], kwargs['topicId'], kwargs['sources'], kwargs['unit'], kwargs['db_session'], BackendLogger(LOG_NAME))

    def get_chart(self):
        sentiments_count = self._get_sentiments_count_from_db()
        time_interval = self.to_date - self.from_date
        from_date_before_time_interval = self.from_date - time_interval - 1
        to_date_before_time_interval = self.from_date - 1
        sentiments_count_before_the_same_time_interval = self._get_sentiments_count_from_db(from_date_before_time_interval, to_date_before_time_interval)
        changes_in_sentiments_count = self._calculate_sentiment_or_emotion_counts_changes(sentiments_count, sentiments_count_before_the_same_time_interval)
        chart_resp = self._convert_chart_data_to_json(sentiments_count, changes_in_sentiments_count)
        return chart_resp

    def _convert_chart_data_to_json(self, sentiments_count, changes_in_sentiments_count):
        def truncate_val_to_two_decimals(val):
            return "{:.2f}".format(val)

        total_mentions_change, positive_mentions_change, negative_mentions_change, neutral_mentions_change = changes_in_sentiments_count
        total_sentiment_count, positive_sentiment_count, negative_sentiment_count, neutral_sentiment_count = sentiments_count
        data = [
            {
                "category": "total",
                "categoryName": "Total Mentions",
                "change": truncate_val_to_two_decimals(total_mentions_change),
                "count": str(total_sentiment_count)
            },
            {
                "category": "positive",
                "categoryName": "Positive Mentions",
                "change": truncate_val_to_two_decimals(positive_mentions_change),
                "count": str(positive_sentiment_count),
            },
            {
                "category": "negative",
                "categoryName": "Negative Mentions",
                "change": truncate_val_to_two_decimals(negative_mentions_change),
                "count": str(negative_sentiment_count),
            },
            {
                "category": "neutral",
                "categoryName": "Neutral Mentions",
                "change": truncate_val_to_two_decimals(neutral_mentions_change),
                "count": str(neutral_sentiment_count),
            }
        ]
        resp = {
            "message": "success",
            "data": data,
            "status": self.SUCCESS_CODE
        }
        return resp
        