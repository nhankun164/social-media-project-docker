import os, sys
import time
# sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
from .base_chart import BaseChart
from .backend_logger import BackendLogger
SCRIPT_NAME = os.path.basename(__file__)
LOG_NAME = SCRIPT_NAME.replace(".py","")

class SentimentTrendChart(BaseChart):
    HOUR = 'hour'
    DAY = 'day'
    MONTH = 'month'
    SENTIMENT_CATEGORY_NAME = 'sentiment_category'
    DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
    SECONDS_PER_DAY = 86400
    SECONDS_PER_HOUR = 3600
    SUCCESS_CODE = 200
    
    def __init__(self, **kwargs):
        super().__init__(kwargs['fromDate'], kwargs['toDate'], kwargs['topicId'], kwargs['sources'], kwargs['unit'], kwargs['db_session'], BackendLogger(LOG_NAME))
        self.container_for_sentiments = [
                                         {"category": super().TOTAL, "categoryName": "Total Mentions", "data": []},
                                         {"category": super().POSITIVE, "categoryName": "Positive Mentions", "data": []},
                                         {"category": super().NEGATIVE, "categoryName": "Negative Mentions", "data": []},
                                         {"category": super().NEUTRAL, "categoryName": "Neutral Mentions", "data": []}
                                        ]
    
    def get_chart(self):
        resp = None
        if self.type == self.HOUR:
            self.__get_sentiment_hourly_trend()
            resp = self._convert_chart_data_to_json()
        elif self.type == self.MONTH:
            self.__get_sentiment_monthly_trend()
            resp = self._convert_chart_data_to_json()
        elif self.type == self.DAY:
            self.__get_sentiment_daily_trend()
            resp = self._convert_chart_data_to_json()
        return resp

    def _convert_chart_data_to_json(self):
        resp = {
            "message": "success",
            "data": self.container_for_sentiments,
            "status": self.SUCCESS_CODE
        }
        return self.container_for_sentiments

    def __break_the_loop_when_date_of_sentiment_exceeds_limit(self, sentiment_date, date_limit):
        return sentiment_date > date_limit

    def __get_next_start_index_fdr_iteration(self, next_start_index):
        return next_start_index

    def __is_sentiment_within_time_range(self, sentiment_date, to_hour, from_hour):
        return sentiment_date >= from_hour and sentiment_date <= to_hour
    
    def __increase_sentiment_counts_based_on_category_when_sentiments_within_time_range(self, sentiment_type, unix_timestamp_of_sentiment, last_second_of_to_date, first_second_of_from_date, sentiments_counter):
        
        def increase_sentiment_count_by_one():
            nonlocal sentiments_counter, sentiment_type
            sentiments_counter[sentiment_type] = sentiments_counter[sentiment_type] + 1
            
        def convert_very_sentiment_type_to_ordinary_type():
            nonlocal sentiment_type
            if sentiment_type == self.VERY_POSITIVE:
                return self.POSITIVE
            elif sentiment_type == self.VERY_NEGATIVE:
                return self.NEGATIVE
            else:
                return sentiment_type
        
        if self.__is_sentiment_within_time_range(unix_timestamp_of_sentiment, last_second_of_to_date, first_second_of_from_date):
            sentiment_type = convert_very_sentiment_type_to_ordinary_type()
            sentiments_counter['total'] += 1
            increase_sentiment_count_by_one()
    
    def __fill_up_sentiments_counter_based_on_type(self, counter, date):
        sentiment_index_mapper = {
            0: super().TOTAL,
            1: super().POSITIVE,
            2: super().NEGATIVE,
            3: super().NEUTRAL
        }
        for index, sentiment_type in sentiment_index_mapper.items():
            self.container_for_sentiments[index]['data'].append([date, counter[sentiment_type]])
    
    def __get_sentiments_in_month(self, sentiments):
        
        def no_records():
            nonlocal num_of_records
            return num_of_records == 0
        
        def get_first_date_of_next_month():
            nonlocal to_time_of_month
            return to_time_of_month + 1
        
        def current_month_is_last_month():
            nonlocal next_month, month_of_to_day
            return next_month == month_of_to_day and is_last_month
        
        def all_records_traversed():
            nonlocal index_recorder, num_of_records
            return index_recorder >= num_of_records - 1
        
        num_of_records = len(sentiments)
        from_time_in_seconds = self.from_date
        to_time_in_seconds = self.to_date
        start_index = 0
        index_recorder = start_index
        start_time_of_month, to_time_of_month = self._get_first_day_and_last_day_of_month(from_time_in_seconds)
        to_time_readable = self._convert_unix_timestamp_to_humanreadable_format(to_time_in_seconds)
        month_of_to_day = self._extract_month_from_date(to_time_readable)
        is_last_month = True
        
        while to_time_of_month <= to_time_in_seconds:
            from_time_readable = self._convert_unix_timestamp_to_humanreadable_format(from_time_in_seconds)
            sentiment_counter_per_month = {
                super().TOTAL:0,
                super().POSITIVE:0,
                super().NEGATIVE:0,
                super().NEUTRAL:0,
                super().HYPEN: 0
            }
            for i in range(start_index, num_of_records):
                if no_records():
                    break
                index_recorder = i
                sentiment_date = sentiments[i][0]
                sentiment_type = sentiments[i][1]
                if self.__break_the_loop_when_date_of_sentiment_exceeds_limit(sentiment_date, to_time_of_month):
                    start_index = self.__get_next_start_index_fdr_iteration(i)
                    break
                self.__increase_sentiment_counts_based_on_category_when_sentiments_within_time_range(sentiment_type, sentiment_date, to_time_of_month, from_time_in_seconds, sentiment_counter_per_month)
            self.__fill_up_sentiments_counter_based_on_type(sentiment_counter_per_month, from_time_in_seconds)  
            first_day_of_next_month_readable = self._convert_unix_timestamp_to_humanreadable_format(get_first_date_of_next_month())
            from_time_in_seconds, to_time_of_month =  self._get_first_day_and_last_day_of_month(get_first_date_of_next_month())
            next_month = self._extract_month_from_date(first_day_of_next_month_readable)
            if current_month_is_last_month():
                to_time_of_month = to_time_in_seconds
                is_last_month = False
            if all_records_traversed():
                start_index = num_of_records - 1

    def __get_sentiments_in_hour(self, sentiments):
        
        def get_last_second_of_to_date():
            return date - 1

        def get_first_second_of_from_date():
            return date - self.SECONDS_PER_HOUR
        
        num_of_records = len(sentiments)
        from_time_in_seconds = self.from_date + self.SECONDS_PER_HOUR
        to_time_in_seconds = self.to_date + self.SECONDS_PER_HOUR
        start_index = 0
        for date in range(from_time_in_seconds, to_time_in_seconds, self.SECONDS_PER_HOUR):
            readable_time = self._convert_unix_timestamp_to_humanreadable_format(date)
            sentiment_counter_per_hour = {
                super().TOTAL:0,
                super().POSITIVE:0,
                super().NEGATIVE:0,
                super().NEUTRAL:0,
                super().HYPEN: 0
            }
            for i in range(start_index, num_of_records):
                unix_timestamp_of_sentiment = sentiments[i][0]
                sentiment_type = sentiments[i][1]
                if self.__break_the_loop_when_date_of_sentiment_exceeds_limit(unix_timestamp_of_sentiment, date):
                    start_index = self.__get_next_start_index_fdr_iteration(i)
                    break
                self.__increase_sentiment_counts_based_on_category_when_sentiments_within_time_range(sentiment_type, unix_timestamp_of_sentiment, get_last_second_of_to_date(), get_first_second_of_from_date(), sentiment_counter_per_hour)
            self.__fill_up_sentiments_counter_based_on_type(sentiment_counter_per_hour, date)  
    
    def __get_sentiments_in_day(self, sentiments):
        
        def get_first_second_of_from_time():
            nonlocal date
            return date - self.SECONDS_PER_DAY
        
        def get_last_second_of_to_time():
            nonlocal date
            return date - 1
        
        num_of_records = len(sentiments)
        from_time_in_seconds = self.from_date + self.SECONDS_PER_DAY
        to_time_in_seconds = self.to_date + self.SECONDS_PER_DAY
        start_index = 0
        for date in range(from_time_in_seconds, to_time_in_seconds, self.SECONDS_PER_DAY):
            date_readable = self._convert_unix_timestamp_to_humanreadable_format(get_first_second_of_from_time())
            sentiment_counter_per_day = {
                super().TOTAL:0,
                super().POSITIVE:0,
                super().NEGATIVE:0,
                super().NEUTRAL:0,
                super().HYPEN: 0
            }
            for i in range(start_index, num_of_records):
                sentiment_date = sentiments[i][0]
                sentiment_type = sentiments[i][1]
                if self.__break_the_loop_when_date_of_sentiment_exceeds_limit(sentiment_date, date):
                    start_index = self.__get_next_start_index_fdr_iteration(i)
                    break
                self.__increase_sentiment_counts_based_on_category_when_sentiments_within_time_range(sentiment_type, sentiment_date, get_last_second_of_to_time(), get_first_second_of_from_time(), sentiment_counter_per_day)
            self.__fill_up_sentiments_counter_based_on_type(sentiment_counter_per_day, get_first_second_of_from_time())
                
    def __get_sentiment_hourly_trend(self):
        sentiments = self._get_emotion_or_sentiment_of_comments(self.SENTIMENT_CATEGORY_NAME)
        self.__get_sentiments_in_hour(sentiments)
    
    def __get_sentiment_monthly_trend(self):
        sentiments = self._get_emotion_or_sentiment_of_comments(self.SENTIMENT_CATEGORY_NAME)
        self.__get_sentiments_in_month(sentiments)
    
    def __get_sentiment_daily_trend(self):
        sentiments = self._get_emotion_or_sentiment_of_comments(self.SENTIMENT_CATEGORY_NAME)
        self.__get_sentiments_in_day(sentiments)


            
            