
class SelectSourcesAndTypesError(Exception):
    pass


class GetCommentsFromDatabaseError(Exception):
    pass


class InvalidSourcesError(Exception):
    pass


class GetPostsFromDatabaseError(Exception):
    pass


class DatabaseConnectionIsLost(Exception):
    pass


class DatesAreInvalid(Exception):
    pass


class PageInfoInvalid(Exception):
    pass


class SourcesListInvalid(Exception):
    pass


class TopicIdInvalid(Exception):
    pass


class SentimentInvalid(Exception):
    pass