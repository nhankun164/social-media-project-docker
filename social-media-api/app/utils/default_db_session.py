
import sqlalchemy
from .MysqlConnector import MysqlConnector
from sqlalchemy import create_engine
from contextlib import contextmanager
from sqlalchemy.orm import sessionmaker
SCHEMA_NAME='social_media_analysis'

default_connector = MysqlConnector()
# connection_str_template = 'mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}'
# default_server_connection_str = connection_str_template.format(
#     user=default_connector.user,
#     passwd=default_connector.passwd,
#     host=default_connector.host,
#     port=default_connector.port,
#     db=SCHEMA_NAME
# )

connection_str_template = 'mysql+pymysql://{user}:{passwd}@{host}/{db}'
default_server_connection_str = connection_str_template.format(
    user=default_connector.user,
    passwd=default_connector.passwd,
    host=default_connector.host,
    db=SCHEMA_NAME
)


default_engine = create_engine(default_server_connection_str, echo=False)

DefaultDBSession  = sessionmaker(bind=default_engine)

@contextmanager
def default_db_session_transaction():
    """Provide a transactional scope around a series of operations."""
    session = DefaultDBSession()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()