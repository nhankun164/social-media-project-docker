
class NotATweetError(Exception):
    pass


class CredentialCheckingError(Exception):
    pass


class KeywordsInvalidError(Exception):
    pass


class CollectTweetsError(Exception):
    pass


class GetDuplicatedTweetsError(Exception):
    pass


class InsertTweetsError(Exception):
    pass


class ParseDatesError(Exception):
    pass


class CheckTweetExistenceError(Exception):
    pass


class GetTweetsAmtsError(Exception):
    pass


class ExtractTweetsFromDBError(Exception):
    pass


class ParseTweetsInPipelineError(Exception):
    pass


class UpdateTweetsError(Exception):
    pass


class TwitterStandardAuthticationError(Exception):
    pass


class TwitterStandardGetApiError(Exception):
    pass


class GetLatestTweetIdError(Exception):
    pass