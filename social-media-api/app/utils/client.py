import socket
import re
import json
import random
import time
import threading
import os

DEFAULT_CRYSTALFEEL_HOST = 'MySecondHost'
CUSTOM_CRYSTALFEEL_HOST  = os.getenv('CRYSTALFEEL_HOST')

# Input ::
#data = "Another cheap attempt to in news or he was really drunk when he was giving that statement";
#data = "HK youths have shown the world what perseverance and struggle means."

# Output :: 
#{"intensity_scores": {"tAnger": 0.464, "tFear": 0.324, "tJoy": 0.289, "tSadness": 0.418, "tValence": 0.422}, "labels": {"tEmotion": "anger", "tEmotionScore": 5, "tSentiment": "negative", "tSentimentScore": -1}, "status": "success"}

class EmotionalAndSentimentalProcesser:
    
    MESSAGE_TIMEOUT_RETRY = "Process time out..retry one more time..."
    MESSAGE_TIMEOUT_STOP = "Process time out, return default value."
    MESSAGE_REMOTE_SERVER_ERROR = "Something is wrong with remote crystalFeel server connection.. Return default value."
    MESSAGE_NO_TEXT_FOUND = "no text found"
    
    CRYSTALFEEL_SERVER_TIMEOUT_IN_SECONDS = 5 * 60
    
    def __init__(self, logger):
        
        self.__PORT_NUMBER = 8247
        self.__IP_ADDRESS  = CUSTOM_CRYSTALFEEL_HOST if CUSTOM_CRYSTALFEEL_HOST else DEFAULT_CRYSTALFEEL_HOST
        self.__s = None
        self.__logger = logger
        self.__logger.log_info("Using " + str(self.__IP_ADDRESS) + "as CrystalFeel host.")
        
    def method(self, data):
        
        ret = None
        if len(data) <=0:
            self.__logger.log_info(self.MESSAGE_NO_TEXT_FOUND)
            return ret
        else:
            count_to_retry = 3
            while count_to_retry >= 0:
                try:
                    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    s.connect((self.__IP_ADDRESS, self.__PORT_NUMBER))
                    s.settimeout(self.CRYSTALFEEL_SERVER_TIMEOUT_IN_SECONDS)
                    data = bytes(data, 'utf-8')
                    s.send(data)
                    result = ''
                    out = s.recv(1)
                    while (out != b'\n'):
                        result = result + str(out, 'utf-8')
                        out = s.recv(1)
                    s.close()
                    emotion = result.strip()
                    ret = json.loads(emotion)
                    return ret
                except socket.timeout as e:
                    error_message = self.MESSAGE_TIMEOUT_RETRY if count_to_retry > 0 else self.MESSAGE_TIMEOUT_STOP
                    self.__logger.log_error(error_message)
                    count_to_retry -= 1
                    time.sleep(3)
                    continue
        return ret 





