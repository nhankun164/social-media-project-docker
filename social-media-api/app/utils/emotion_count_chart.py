import os, sys
from .base_chart import BaseChart
from .backend_logger import BackendLogger
SCRIPT_NAME = os.path.basename(__file__)
LOG_NAME = SCRIPT_NAME.replace(".py","")

class EmotionCountChart(BaseChart):
    CHANGE_INDEX_TOTAL_MENTIONS = 0
    CHANGE_INDEX_POSITIVE_MENTIONS = 1
    CHANGE_INDEX_NEGATIVE_MENTIONS = 2
    CHANGE_INDEX_NEUTRAL_MENTIONS = 3
    INDEX_TOTAL_COUNT = 0
    SUCCESS_CODE = 200
    
    def __init__(self, **kwargs):
        super().__init__(kwargs['fromDate'], kwargs['toDate'], kwargs['topicId'], kwargs['sources'], kwargs['unit'], kwargs['db_session'], BackendLogger(LOG_NAME))

    def get_chart(self):
        emotions_count = self._get_emotions_count_from_db()
        time_interval = self.to_date - self.from_date
        from_date_before_time_interval = self.from_date - time_interval - 1
        to_date_before_time_interval = self.from_date - 1
        emotions_count_before_the_same_time_interval = self._get_emotions_count_from_db(from_date_before_time_interval, to_date_before_time_interval)
        changes_in_emotions_count = self._calculate_sentiment_or_emotion_counts_changes(emotions_count, emotions_count_before_the_same_time_interval)
        chart_resp = self._convert_chart_data_to_json(emotions_count, changes_in_emotions_count)
        return chart_resp

    def _convert_chart_data_to_json(self, emotions_count, changes_in_emotions_count):
            def truncate_val_to_two_decimals(val):
                return "{:.2f}".format(val)

            total_mentions_change, joy_emotion_change, fear_emotion_change, sadness_emotion_change, anger_emotion_change = changes_in_emotions_count
            total_sentiment_count, joy_emotion_count, fear_emotion_count, sadness_emotion_count, anger_emotion_count = emotions_count
            emotion_change_and_count_dict = {
                "joy":[joy_emotion_change, joy_emotion_count],
                "fear": [fear_emotion_change, fear_emotion_count],
                "sadness": [sadness_emotion_change, sadness_emotion_count],
                "anger": [anger_emotion_change, anger_emotion_count]
            }
            data = [{
                "category": emotion_type,
                "categoryName": emotion_type.capitalize(),
                "change": truncate_val_to_two_decimals(change_count[0]),
                "count": str(change_count[1])
            } for emotion_type, change_count in emotion_change_and_count_dict.items()]
            resp = {
                "message": "success",
                "data": data,
                "status": self.SUCCESS_CODE
            }
            return resp
