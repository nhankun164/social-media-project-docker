import logging
import os
from sqlalchemy.dialects import mysql
import json
from sqlalchemy import event
from sqlalchemy.engine import Engine
import time
import logging
import datetime

class BackendLogger:
    SQL_ALCHEMY_LOG_LEVEL = 'DEBUG'
    FILE_LOGGING_FORMAT   = '%(asctime)s - %(levelname)s - %(message)s'
    STREAM_LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

    def __init__(self, log_name, log_dir=os.getenv('HOME') + "/log", log_sql_alchemy_queries=False):
        self._log_name = log_name
        self._logger   = logging.getLogger(log_name)
        log_file_name  = log_name + ".log"
        log_file_path  = log_dir + "/" + log_file_name
        self._logger.setLevel(logging.DEBUG)
        os.makedirs(log_dir, exist_ok=True)
        if not self._logger.hasHandlers():
            file_handler   = self.__get_file_logging_handler(log_file_path)
            stream_handler = self.__get_stream_logging_handler()
            self._logger.addHandler(file_handler)
            self._logger.addHandler(stream_handler)
            if log_sql_alchemy_queries:
                self._enable_sql_alchemy_query_logging_event_listeners()


    def __get_file_logging_handler(self, log_file_path):
        fh = logging.FileHandler(log_file_path)
        fh.setLevel(logging.DEBUG)
        formatter = logging.Formatter(self.FILE_LOGGING_FORMAT)
        fh.setFormatter(formatter)
        return fh

    def __get_stream_logging_handler(self):
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        formatter = logging.Formatter(self.STREAM_LOGGING_FORMAT)
        ch.setFormatter(formatter)
        return ch

    def _enable_sql_alchemy_query_logging_event_listeners(self):
        log_name             = self._log_name
        query_start_time_key = 'query_start_time'

        @event.listens_for(Engine, "before_cursor_execute")
        def before_cursor_execute(conn, cursor, statement,
                                parameters, context, executemany):
            conn.info.setdefault(query_start_time_key, []).append(time.time())

        @event.listens_for(Engine, "after_cursor_execute")
        def after_cursor_execute(conn, cursor, statement, parameters, context, executemany):
            log_query_time_as_json(conn, cursor, statement, parameters, context, executemany)
        
        def log_query_time_as_json(conn, cursor, statement, parameters, context, executemany):
            def my_custom_json_converter(o):
                if isinstance(o, datetime.datetime):
                    return o.__str__()

            query_time_in_seconds = time.time() - conn.info[query_start_time_key].pop(-1)
            logger                = logging.getLogger(log_name)
            log_json              = {
                "query_time_in_seconds": format(query_time_in_seconds,'.3f'),
                "statement":             statement.replace("\n"," "),
                "params":                parameters
            }
            log_jsonn_str         = json.dumps(log_json, default=my_custom_json_converter)
            logger.debug(log_jsonn_str)


    def log_error(self, msg):
        self._logger.error(msg)

    def log_warning(self, msg):
        self._logger.warning(msg)

    def log_info(self, msg):
        self._logger.info(msg)

    def log_debug(self, msg):
        if (len(msg.split("\n")) >2):
            msg_lines = msg.split("\n")
            for msg_line in msg_lines:
                self._logger.debug(msg_line)
        else:
            self._logger.debug(msg)


    def log_debug_with_msg_and_nested_list(self, msg, nested_list):
        formatted_msg = msg + "\n"
        for list_item in nested_list:
            formatted_msg += str(list_item) + "\n"
        self.log_debug(formatted_msg)
