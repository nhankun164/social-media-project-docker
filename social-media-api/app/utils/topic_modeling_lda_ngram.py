import nltk
nltk.download('stopwords')
# nltk.download('averaged_perceptron_tagger')
import re
import string
import pandas as pd
import numpy as np
from nltk.corpus import stopwords
import gensim
from gensim import corpora
import pyLDAvis.gensim_models
import matplotlib.pyplot as plt
import json
from pprint import pprint
from tqdm import tqdm
# Gensim
from gensim.utils import simple_preprocess
from gensim.models import CoherenceModel
# spacy for lemmatization
import spacy
# Plotting tools
import pyLDAvis
import time
import tomotopy as tp
from nltk.stem.porter import PorterStemmer


def generateTopicNGram(data, num_of_topics, path):
    stop_word_list = set(stopwords.words('english'))
    print('data------------------- ', len(data))

    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = nltk.collocations.BigramCollocationFinder.from_documents([content.split() for content in data])
    # Filter only those that occur at least 50 times
    finder.apply_freq_filter(50)
    bigram_scores = finder.score_ngrams(bigram_measures.pmi)



    # Tri-gram
    trigram_measures = nltk.collocations.TrigramAssocMeasures()
    finder = nltk.collocations.TrigramCollocationFinder.from_documents([content.split() for content in data])
    # Filter only those that occur at least 50 times
    finder.apply_freq_filter(50)
    trigram_scores = finder.score_ngrams(trigram_measures.pmi)



    # Bi-gram
    bigram_pmi = pd.DataFrame(bigram_scores)
    bigram_pmi.columns = ['bigram', 'pmi']
    bigram_pmi.sort_values(by='pmi', axis = 0, ascending = False, inplace = True)

    print('bigram_pmi------------- ', bigram_pmi)
    # Tri-gram
    trigram_pmi = pd.DataFrame(trigram_scores)
    trigram_pmi.columns = ['trigram', 'pmi']
    trigram_pmi.sort_values(by='pmi', axis = 0, ascending = False, inplace = True)

    print('trigram_pmi------------- ', trigram_pmi)


    # Filter for bigrams with only noun-type structures
    def bigram_filter(bigram):
        tag = nltk.pos_tag(bigram)
        if tag[0][1] not in ['JJ', 'NN'] and tag[1][1] not in ['NN']:
            return False
        if bigram[0] in stop_word_list or bigram[1] in stop_word_list:
            return False
        if 'n' in bigram or 't' in bigram:
            return False
        if 'PRON' in bigram:
            return False
        return True


    # Filter for trigrams with only noun-type structures
    def trigram_filter(trigram):
        tag = nltk.pos_tag(trigram)
        if tag[0][1] not in ['JJ', 'NN'] and tag[1][1] not in ['JJ','NN']:
            return False
        if trigram[0] in stop_word_list or trigram[-1] in stop_word_list or trigram[1] in stop_word_list:
            return False
        if 'n' in trigram or 't' in trigram:
            return False
        if 'PRON' in trigram:
            return False
        return True 






    # Can set pmi threshold to whatever makes sense - eyeball through and select threshold where n-grams stop making sense
    # choose top 500 ngrams in this case ranked by PMI that have noun like structures
    filtered_bigram = bigram_pmi[bigram_pmi.apply(lambda bigram:\
                                                bigram_filter(bigram['bigram'])\
                                                and bigram.pmi > 5, axis = 1)][:500]

    filtered_trigram = trigram_pmi[trigram_pmi.apply(lambda trigram: \
                                                    trigram_filter(trigram['trigram'])\
                                                    and trigram.pmi > 5, axis = 1)][:500]

    print('filtered_bigram----------------- ', filtered_bigram)
    bigrams = [' '.join(x) for x in filtered_bigram.bigram.values if len(x[0]) > 2 or len(x[1]) > 2]
    trigrams = [' '.join(x) for x in filtered_trigram.trigram.values if len(x[0]) > 2 or len(x[1]) > 2 and len(x[2]) > 2]




   



    # Concatenate n-grams
    def replace_ngram(x):
        for gram in trigrams:
            x = x.replace(gram, '_'.join(gram.split()))
        for gram in bigrams:
            x = x.replace(gram, '_'.join(gram.split()))
        return x


    
    reviews_w_ngrams = [a for a in map(lambda x: replace_ngram(x), data)]




    # tokenize reviews + remove stop words + remove names + remove words with less than 2 characters
    reviews_w_ngrams = [a for a in map(lambda x: [word for word in x.split()\
                                                 if word not in stop_word_list\
                                                            and len(word) > 2], reviews_w_ngrams)]


    # print('reviews_w_ngrams------------ ', reviews_w_ngrams)

    # Filter for only nouns
    def noun_only(x):
        pos_comment = nltk.pos_tag(x)
        filtered = [word[0] for word in pos_comment if word[1] in ['NN']]
        # to filter both noun and verbs
        #filtered = [word[0] for word in pos_comment if word[1] in ['NN','VB', 'VBD', 'VBG', 'VBN', 'VBZ']]
        return filtered





    final_reviews = [a for a in map(lambda x: noun_only(x), reviews_w_ngrams)]

    # print('final_reviews------------ ', final_reviews)


    #LDA model
    dictionary = corpora.Dictionary(final_reviews)
    doc_term_matrix = [dictionary.doc2bow(doc) for doc in final_reviews]

    Lda = gensim.models.ldamodel.LdaModel
    ldamodel = Lda(doc_term_matrix, num_topics=num_of_topics, id2word = dictionary, passes=40,\
                iterations=200,  chunksize = 10000, eval_every = None, random_state=0)




    #Relevancy
    topic_data =  pyLDAvis.gensim_models.prepare(ldamodel, doc_term_matrix, dictionary, mds = 'pcoa')
    all_topics = {}
    num_terms = 10 # Adjust number of words to represent each topic
    lambd = 0.6 # Adjust this accordingly based on tuning above
    for i in range(1,num_of_topics + 1): #Adjust this to reflect number of topics chosen for final LDA model
        topic = topic_data.topic_info[topic_data.topic_info.Category == 'Topic'+str(i)].copy()
        topic['relevance'] = topic['loglift']*(1-lambd)+topic['logprob']*lambd
        all_topics['Topic '+str(i)] = topic.sort_values(by='relevance', ascending=False).Term[:num_terms].values
    print('all_topics----------------- ', all_topics)
    csv_topic_df = pd.DataFrame(all_topics)
    csv_topic_df.to_csv(path[:-5] + '_csv_topics.csv', index=False)
    return path[:-5] + '_csv_topics.csv'




def generateTopicNGram2(data, num_of_topics, path):
    # corpus.process(open('logs/enwiki-stemmed-1000.txt', encoding='utf-8'))

    # mdl = tp.LDAModel(k=20, min_cf=10, min_df=5, corpus=corpus)
    # mdl.train(0)
    # print('Num docs:', len(mdl.docs), ', Vocab size:', len(mdl.used_vocabs), ', Num words:', mdl.num_words)
    # print('Removed top words:', mdl.removed_top_words)

    # # for docs in data:
    # #     mdl.add_doc(docs.strip().split())
    
    # for i in range(0, 100, 10):
    #     mdl.train(10)
    #     print('Iteration: {}\tLog-likelihood: {}'.format(i, mdl.ll_per_word))


    # mdl.summary()
    # mdl.save('logs/sample_hdp_model.bin')

        

    stemmer = PorterStemmer()
    stops = set(stopwords.words('english'))
    stops.update(set(['However', 'however', 'Really', 'really']))
    print('stopword------------------ ', stops)
    corpus = tp.utils.Corpus(tokenizer=tp.utils.SimpleTokenizer(stemmer=stemmer.stem), 
        stopwords=lambda x: len(x) <= 2 or x in stops)
    # data_feeder yields a tuple of (raw string, user data) or a str (raw string)
    corpus.process(data)

    # corpus = tp.utils.Corpus(tokenizer=tp.utils.SimpleTokenizer(), stopwords=['.'])
    # # data_feeder yields a tuple of (raw string, user data) or a str (raw string)
    # corpus.process(open('logs/enwiki-stemmed-1000.txt', encoding='utf-8'))

    # make LDA model and train
    mdl = tp.LDAModel(k=num_of_topics, min_cf=10, min_df=5, corpus=corpus)
    # The word 'church' is assigned to Topic 0 with a weight of 1.0 and to the remaining topics with a weight of 0.1.
    # Therefore, a topic related to 'church' can be fixed at Topic 0 .
    mdl.set_word_prior('church', [1.0 if k == 0 else 0.1 for k in range(num_of_topics)])
    # Topic 1 for a topic related to 'softwar'
    mdl.set_word_prior('softwar', [1.0 if k == 1 else 0.1 for k in range(num_of_topics)])
    # Topic 2 for a topic related to 'citi'
    mdl.set_word_prior('citi', [1.0 if k == 2 else 0.1 for k in range(num_of_topics)])
    mdl.train(0)
    print('Num docs:', len(mdl.docs), ', Vocab size:', len(mdl.used_vocabs), ', Num words:', mdl.num_words)
    print('Removed top words:', mdl.removed_top_words)
    for i in range(0, 2000, 10):
        mdl.train(10)
        print('Iteration: {}\tLog-likelihood: {}'.format(i, mdl.ll_per_word))
    
    mdl.summary()

    # extract candidates for auto topic labeling
    extractor = tp.label.PMIExtractor(min_cf=10, min_df=5, max_len=5, max_cand=10000, normalized=True)
    cands = extractor.extract(mdl)
    labeler = tp.label.FoRelevance(mdl, cands, min_df=5, smoothing=1e-2, mu=0.25)
    results = []
    for k in range(mdl.k):
        print("== Topic #{} ==".format(k))
        # labels = []

        # for label, score in labeler.get_topic_labels(k, top_n=5):
        #     # labels.append({
        #     #     "label": label,
        #     #     "score": score
        #     # })
        #     print("Labels:", ', '.join(label + " (" + str(score) + ")" ))
        labels = labeler.get_topic_labels(k, top_n=5)
        print('labels------------------ ', labels)
        element = {
            "topic": k,
            "label": labels[0][0] 
        }
        results.append(element)
        # for word, prob in mdl.get_topic_words(k, top_n=20):
        #     print(word, prob, sep='\t')
    return results


