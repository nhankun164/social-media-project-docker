# -*- coding: utf-8 -*-
"""
This code is used to generate refined_text of the content of a comment from Chapm..
Some objects have been serialized into files and stored locally to save processing time.
"""
import re
import aspell
import pickle
from sklearn import feature_extraction
import os

maxword = 58
DIRNAME = os.path.dirname(__file__)

# extract data object from local files.
with open(DIRNAME + "/../Files/words_cost_obj.txt", "rb") as obj:
    wordcost = pickle.load(obj)

with open(DIRNAME + "/../Files/subList.txt", "rb") as obj:
    subList = pickle.load(obj)

with open(DIRNAME + "/../Files/misspelledList.txt", "rb") as obj:
    misspelledList = pickle.load(obj)

with open(DIRNAME + "/../Files/correctList.txt", "rb") as obj:
    correctList = pickle.load(obj)

with open(DIRNAME + "/../Files/doubleList.txt", "rb") as obj:
    doubleList = pickle.load(obj)

with open(DIRNAME + "/../Files/cdoubleList.txt", "rb") as obj:
    cdoubleList = pickle.load(obj)

with open(DIRNAME + "/../Files/stopList.txt", "rb") as obj:
    stopList = pickle.load(obj)

with open(DIRNAME +  "/../Files/contList.txt", "rb") as obj:
    contList = pickle.load(obj)

with open(DIRNAME +  "/../Files/subsList.txt", "rb") as obj:
    subsList = pickle.load(obj)


def get_ngram_from_text(tweet):
    lst = []
    freq = []
    try:
        v = feature_extraction.text.CountVectorizer(ngram_range=(1, 2))
        dct = v.fit([tweet]).vocabulary_
        for key, value in dct.items():
            #word = key.encode('ascii', errors='ignore');
            word = tweet.encode('ascii', errors='ignore').decode('utf-8').lower()
            word = key
            if word not in lst:
                lst.append(word)
                freq.append(1)
            else:
                freq[lst.index(word)] = freq[lst.index(word)] + 1
    except ValueError:
        print("ERROR")

    counter = 0
    for w in lst:
        if (freq[counter] > 1):
            if ' ' in w:
                word = w.replace(' ', '')
                if word in lst:
                    freq[lst.index(word)] = freq[lst.index(word)] + freq[lst.index(w)]
                    freq[lst.index(w)] = 0
        counter = counter + 1

    counter = 0
    for w in lst:
        if (freq[counter] > 1):
            if w[-1] == 's':
                word = w[:-1]
                if word in lst:
                    freq[lst.index(word)] = freq[lst.index(word)] + freq[lst.index(w)]
                    freq[lst.index(w)] = 0
        counter = counter + 1

    counter = 0
    for w in lst:
        if (freq[counter] > 1):
            if w[-2:] == 'es':
                word = w[:-2]
                if word in lst:
                    freq[lst.index(word)] = freq[lst.index(word)] + freq[lst.index(w)]
                    freq[lst.index(w)] = 0
        counter = counter + 1

    wordList = []
    wordFreq = []
    counter = 0
    for word in lst:
        if (freq[counter] > 0):
            wordList.append(lst[counter])
            wordFreq.append(freq[counter])
        counter = counter + 1
    return wordList, wordFreq


def infer_spaces(s):
    def best_match(i):
        candidates = enumerate(reversed(cost[max(0, i - maxword):i]))
        return min((c + wordcost.get(s[i - k - 1:i], 9e999), k + 1) for k, c in candidates)

    # Build the cost array.
    cost = [0]
    for i in range(1, len(s) + 1):
        c, k = best_match(i)
        cost.append(c)

    # Backtrack to recover the minimal-cost string.
    out = []
    i = len(s)
    while i > 0:
        c, k = best_match(i)
        assert c == cost[i]
        out.append(s[i - k:i])
        i -= k

    return " ".join(reversed(out))


def RemoveHTMLTags(tweet):
    tweet = tweet.replace("&amp;", '')
    tweet = tweet.replace("&gt;", '')
    tweet = tweet.replace("&lt;", '')
    tweet = tweet.strip()
    tweet = tweet.replace("’", "'")
    tweet = tweet.encode('ascii', errors='ignore').decode('utf-8').lower()
    tweet = tweet.lower()
    return tweet


def repetitions(s):
    r = re.compile(r"(.+?)\1+")
    for match in r.finditer(s):
        yield (match.group(1), len(match.group(0)) / len(match.group(1)))


def checkRepetition(word):
    lexicons = ['ha', 'haa', 'he', 'hee']
    lst = list(repetitions(word))
    # print word, lst;
    count = 0
    for l in lst:
        if l[0] not in lexicons:
            if (len(lst) == 1):
                if (l[1] > 2):
                    return l[0] + l[0]
            return word
        count = count + l[1]

    if (count < 2):
        return word

    return 'haha'


def remove_tags_from_tweet(tweet):
    tokens = tweet.split()
    out = ''
    for token in tokens:
        word = token.strip()
        if word.__len__() > 1:
            if ((word[0] != '#') and (word[0] != '@') and (word[0] != '&')):
                if (word.isdigit() == False):
                    if (bool(re.search(r'\d', word)) == False):
                        out = out + ' ' + word
    tweet = re.sub('[^A-Za-z\s]+', '', out.strip())
    return tweet


def combine_split_words(tweet):
    global aspl
    out = ''
    tokens = tweet.split()
    word1 = tokens[0].strip()
    word2 = ''
    flag = 0
    for i in range(1, len(tokens)):
        if flag:
            flag = 0
            continue
        word2 = tokens[i].strip()
        mixword = word1 + word2
        if aspl.check(mixword):
            out = out + ' ' + mixword
            if (i + 1) == len(tokens):
                return out.strip()
            else:
                word1 = tokens[i + 1]
                word2 = ''
                flag = 1
        else:
            out = out + ' ' + word1
            word1 = word2
    if (word2 == ''):
        out = out + ' ' + word1
    else:
        out = out + ' ' + word2
    return out.strip()


def remove_stopwords_from_tweet(tweet, stopList):
    out = ''
    tokens = re.findall(r'[^,.;\s]+', tweet)  # tweet.split();
    for token in tokens:
        word = token.strip()
        if word not in stopList:
            out = out + ' ' + word

    return out.strip()


aspl = aspell.Speller('lang', 'en')


def getRefinedText(text):
    tweet = RemoveHTMLTags(text)
    if tweet != '':
        tweet = re.sub(r'\<.*\>', '', tweet)
        tweet = re.sub(r'[@]\S*', '@', tweet)
        tweet = re.sub(r'(.)\1{2,}', r'\1\1', tweet)
        tweet = tweet.replace("\n", ' ')
        tokens = tweet.split()

        rt = ''
        for t in tokens:
            tk = (re.sub(r'^https?:\/\/.*[\r\n]*', '', t.strip(), flags=re.MULTILINE)).strip()
            tk = (re.sub(r'^www?.*[\r\n]*', '', tk.strip(), flags=re.MULTILINE)).strip()
            if tk != '':
                tk = checkRepetition(tk)
                rt = rt + ' ' + tk
        tweet = rt

        counter = 0
        for w in contList:
            tweet = re.sub(r'\b%s\b' % re.escape(w), subsList[counter], tweet)
            counter = counter + 1

        tokens = re.findall(r'[#]\S*', tweet)
        for t in tokens:
            if (len(tweet) > 1):
                word = t[1:].lower()
                if word in misspelledList:
                    word = correctList[misspelledList.index(word)]
                    tweet = tweet.replace(t, word)
                else:
                    word = ''.join([i for i in word if not i.isdigit()])
                    word = infer_spaces(word)
                    tweet = tweet.replace(t, word)

        counter = 0
        for w in misspelledList:
            if w in tweet:
                tweet = re.sub(r'\b%s\b' % re.escape(w), correctList[counter], tweet)
            counter = counter + 1

        counter = 0
        for w in doubleList:
            if w in tweet:
                tweet = re.sub(r'\b%s\b' % re.escape(w), cdoubleList[counter], tweet)
            counter = counter + 1

        tweet = remove_tags_from_tweet(tweet)

        if ((tweet.strip()).__len__() > 4):
            tweet = combine_split_words(tweet)
            tweet = remove_stopwords_from_tweet(tweet, stopList)

        tweet = tweet.replace("'", "")
        tweet = tweet.replace('"', '')
        tweet = tweet.replace(',', ' ')
        tweet = tweet.replace(':', ' ')

        counter = 0
        ngrams = '-'

        if tweet.__len__() > 10:
            token_lst, freq = get_ngram_from_text(tweet)
            sort_idx = sorted(range(len(freq)), key=lambda k: freq[k], reverse=True)
            wd_counter = 0
            for wd in token_lst:
                if (wd_counter):
                    ngrams = ngrams + ':' + token_lst[sort_idx[wd_counter]] + ',' + str(freq[sort_idx[wd_counter]])
                else:
                    ngrams = token_lst[sort_idx[wd_counter]] + ',' + str(freq[sort_idx[wd_counter]])

                wd_counter = wd_counter + 1
                if (ngrams.__len__() > 50000):
                    break
        return ngrams
