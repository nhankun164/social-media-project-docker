import os, sys
import time
from .base_chart import BaseChart
from .backend_logger import BackendLogger
SCRIPT_NAME = os.path.basename(__file__)
LOG_NAME = SCRIPT_NAME.replace(".py","")

class EmotionTrendChart(BaseChart):
    HOUR = 'hour'
    DAY = 'day'
    MONTH = 'month'
    EMOTION_CATEGORY_NAME = 'emotion_category'
    SECONDS_PER_DAY = 86400
    SECONDS_PER_HOUR = 3600
    
    def __init__(self, **kwargs):
        super().__init__(kwargs['fromDate'], kwargs['toDate'], kwargs['topicId'], kwargs['sources'], kwargs['unit'], kwargs['db_session'], BackendLogger(LOG_NAME))
        self.container_for_emotions = [
                                         {"category": super().JOY, "categoryName": "Joy", "data": []},
                                         {"category": super().FEAR, "categoryName": "Fear", "data": []},
                                         {"category": super().SADNESS, "categoryName": "Sadness", "data": []},
                                         {"category": super().ANGER, "categoryName": "Anger", "data": []}
                                        ]
    
    def get_chart(self):
        resp = None
        if self.type == self.HOUR:
            self.__get_emotions_hourly_trend()
            resp = self._convert_chart_data_to_json()
        elif self.type == self.MONTH:
            self.__get_emotions_monthly_trend()
            resp = self._convert_chart_data_to_json()
        elif self.type == self.DAY:
            self.__get_emotions_daily_trend()
            resp = self._convert_chart_data_to_json()
        return resp

    def _convert_chart_data_to_json(self):
        resp = {
            "message": super().SUCCESS_MESSAGE,
            "data": self.container_for_emotions,
            "status": super().SUCCESS_CODE
        }
        return self.container_for_emotions

    def __break_the_loop_when_date_of_emotion_exceeds_limit(self, emotion_date, date_limit):
        return emotion_date > date_limit

    def __get_next_start_index_fdr_iteration(self, next_start_index):
        return next_start_index

    def __is_emotion_within_time_range(self, emotion_date, to_hour, from_hour):
        return emotion_date >= from_hour and emotion_date <= to_hour

    def __fill_up_emotions_counter_based_on_type(self, counter, date):
        emotion_index_mapper = {
            0: super().JOY,
            1: super().FEAR,
            2: super().SADNESS,
            3: super().ANGER
        }
        for index, emotion_type in emotion_index_mapper.items():
            self.container_for_emotions[index]['data'].append([date, counter[emotion_type]])

    def __increase_emotion_counts_based_on_category_when_emotions_within_time_range(self, emotion_type, unix_timestamp_of_emotion, last_second_of_to_date, first_second_of_from_date, emotions_counter):
        def increase_emotion_count_by_one():
            nonlocal emotions_counter, emotion_type
            emotions_counter[emotion_type] = emotions_counter[emotion_type] + 1
        
        if self.__is_emotion_within_time_range(unix_timestamp_of_emotion, last_second_of_to_date, first_second_of_from_date):
            increase_emotion_count_by_one()
    
    def __get_emotions_hourly_trend(self):
        emotions = self._get_emotion_or_sentiment_of_comments(self.EMOTION_CATEGORY_NAME)
        self.__get_emotions_in_hour(emotions)

    def __get_emotions_monthly_trend(self):
        emotions = self._get_emotion_or_sentiment_of_comments(self.EMOTION_CATEGORY_NAME)
        self.__get_emotions_in_month(emotions)

    def __get_emotions_daily_trend(self):
        emotions = self._get_emotion_or_sentiment_of_comments(self.EMOTION_CATEGORY_NAME)
        self.__get_emotions_in_day(emotions)

    def __get_emotions_in_hour(self, emotions):
        
        def get_last_second_of_to_date():
            return date - 1

        def get_first_second_of_from_date():
            return date - self.SECONDS_PER_HOUR
        
        num_of_records = len(emotions)
        print('num_of_records-------------- ', num_of_records)
        from_time_in_seconds = self.from_date + self.SECONDS_PER_HOUR
        to_time_in_seconds = self.to_date + self.SECONDS_PER_HOUR
        start_index = 0
        for date in range(from_time_in_seconds, to_time_in_seconds, self.SECONDS_PER_HOUR):
            readable_time = self._convert_unix_timestamp_to_humanreadable_format(date)
            emotion_counter_per_hour = {
                super().JOY:0,
                super().FEAR:0,
                super().SADNESS:0,
                super().ANGER:0,
                super().NO_SPECIFIC_EMOTION: 0,
                super().HYPEN:0
            }
            for i in range(start_index, num_of_records):
                unix_timestamp_of_emotion = emotions[i][0]
                emotion_type = emotions[i][1]
                if self.__break_the_loop_when_date_of_emotion_exceeds_limit(unix_timestamp_of_emotion, date):
                    start_index = self.__get_next_start_index_fdr_iteration(i)
                    break
                self.__increase_emotion_counts_based_on_category_when_emotions_within_time_range(emotion_type, unix_timestamp_of_emotion, get_last_second_of_to_date(), get_first_second_of_from_date(), emotion_counter_per_hour)
            self.__fill_up_emotions_counter_based_on_type(emotion_counter_per_hour, date)

    def __get_emotions_in_day(self, emotions):
        
        def get_first_second_of_from_time():
            nonlocal date
            return date - self.SECONDS_PER_DAY
        
        def get_last_second_of_to_time():
            nonlocal date
            return date - 1
        
        num_of_records = len(emotions)
        from_time_in_seconds = self.from_date + self.SECONDS_PER_DAY
        to_time_in_seconds = self.to_date + self.SECONDS_PER_DAY
        start_index = 0
        for date in range(from_time_in_seconds, to_time_in_seconds, self.SECONDS_PER_DAY):
            date_readable = self._convert_unix_timestamp_to_humanreadable_format(get_first_second_of_from_time())
            emotion_counter_per_day = {
                super().JOY:0,
                super().FEAR:0,
                super().SADNESS:0,
                super().ANGER:0,
                super().NO_SPECIFIC_EMOTION: 0,
                super().HYPEN:0
            }
            for i in range(start_index, num_of_records):
                emotion_date = emotions[i][0]
                emotion_type = emotions[i][1]
                if self.__break_the_loop_when_date_of_emotion_exceeds_limit(emotion_date, date):
                    start_index = self.__get_next_start_index_fdr_iteration(i)
                    break
                self.__increase_emotion_counts_based_on_category_when_emotions_within_time_range(emotion_type, emotion_date, get_last_second_of_to_time(), get_first_second_of_from_time(), emotion_counter_per_day)
            self.__fill_up_emotions_counter_based_on_type(emotion_counter_per_day, get_first_second_of_from_time())

    def __get_emotions_in_month(self, emotions):
        
        def no_records():
            nonlocal num_of_records
            return num_of_records == 0
        
        def get_first_date_of_next_month():
            nonlocal to_time_of_month
            return to_time_of_month + 1
        
        def current_month_is_last_month():
            nonlocal next_month, month_of_to_day
            return next_month == month_of_to_day and is_last_month
        
        def all_records_traversed():
            nonlocal index_recorder, num_of_records
            return index_recorder >= num_of_records - 1
        
        num_of_records = len(emotions)
        from_time_in_seconds = self.from_date
        to_time_in_seconds = self.to_date
        start_index = 0
        index_recorder = start_index
        start_time_of_month, to_time_of_month = self._get_first_day_and_last_day_of_month(from_time_in_seconds)
        to_time_readable = self._convert_unix_timestamp_to_humanreadable_format(to_time_in_seconds)
        month_of_to_day = self._extract_month_from_date(to_time_readable)
        is_last_month = True
        
        while to_time_of_month <= to_time_in_seconds:
            from_time_readable = self._convert_unix_timestamp_to_humanreadable_format(from_time_in_seconds)
            emotion_counter_per_month = {
                super().JOY:0,
                super().FEAR:0,
                super().SADNESS:0,
                super().ANGER:0,
                super().NO_SPECIFIC_EMOTION: 0,
                super().HYPEN:0
            }
            for i in range(start_index, num_of_records):
                if no_records():
                    break
                index_recorder = i
                emotion_date = emotions[i][0]
                emotion_type = emotions[i][1]
                if self.__break_the_loop_when_date_of_emotion_exceeds_limit(emotion_date, to_time_of_month):
                    start_index = self.__get_next_start_index_fdr_iteration(i)
                    break
                self.__increase_emotion_counts_based_on_category_when_emotions_within_time_range(emotion_type, emotion_date, to_time_of_month, from_time_in_seconds, emotion_counter_per_month)
            self.__fill_up_emotions_counter_based_on_type(emotion_counter_per_month, from_time_in_seconds)  
            first_day_of_next_month_readable = self._convert_unix_timestamp_to_humanreadable_format(get_first_date_of_next_month())
            from_time_in_seconds, to_time_of_month =  self._get_first_day_and_last_day_of_month(get_first_date_of_next_month())
            next_month = self._extract_month_from_date(first_day_of_next_month_readable)
            if current_month_is_last_month():
                to_time_of_month = to_time_in_seconds
                is_last_month = False
            if all_records_traversed():
                start_index = num_of_records - 1