import pymysql
from pymysql.err import OperationalError
import sys
import time
import os
from dotenv import load_dotenv
load_dotenv()

class MysqlConnector:

    def __init__(self, host='', port=3306, user='', database='', passwd=''):
        # self.host = os.getenv('BACKEND_DB_HOST') if os.getenv('BACKEND_DB_HOST') else host
        # self.port = int(os.getenv('BACKEND_DB_PORT')) if os.getenv('BACKEND_DB_PORT') else port
        # self.user = os.getenv('BACKEND_DB_USER') if os.getenv('BACKEND_DB_USER') else user
        # self.database = os.getenv('BACKEND_DB_DATABASE') if os.getenv('BACKEND_DB_DATABASE') else database
        # self.passwd = os.getenv('BACKEND_DB_PASSWORD') if os.getenv('BACKEND_DB_PASSWORD') else passwd
        self.host = os.getenv('DB_HOST') if os.getenv('DB_HOST') else host
        self.user = os.getenv('DB_USER') if os.getenv('DB_USER') else user
        self.database = os.getenv('DB_DATABASE') if os.getenv('DB_DATABASE') else database
        self.passwd = os.getenv('DB_PASSWORD') if os.getenv('DB_PASSWORD') else passwd
        self.__dbConnector = None
        self.__connectToDB()

    def getConnectorInstance(self):
        if self.__dbConnector is not None:
            return self.__dbConnector
        else:
            print("The db connector has not been initialized, will try to initialize it soon...")
            self.__connectToDB()

    def __connectToDB(self):
        try:
            self.__dbConnector = pymysql.connect(host=self.host, port=self.port, user=self.user, passwd=self.passwd, \
                                                 database=self.database)
            return True
        except Exception as e:
            return False

    def __reConnect(self, maxConnectionCount = 60, sleepDuration = 5):
        _count = 0
        _status = True
        while _status and _count <= maxConnectionCount:
            try:
                self.__dbConnector.ping()
                _status = False
            except Exception as e:
                if self.__connectToDB():
                    _status = False
                    break
                _count += 1
                time.sleep(sleepDuration)
        if not _status:
            print("The database connection is running successfully.")
        else:
            print("Cannot connect to database, system exits...")
            sys.exit()

    def closeConnection(self):
        self.__dbConnector.close()

    def checkConnectionStatus(self):
        try:
            self.__dbConnector.ping()
        except Exception as e:
            print("Lose connection to database....Will try to reconnect database...")
            self.__reConnect()

    def isClosed(self):
        try:
            self.__dbConnector.ping(reconnect=False)
        except:
            return True
        return False

    def queryOneFromDB(self, querySQL, values = None):

        self.checkConnectionStatus()
        self.cur = self.__dbConnector.cursor()

        if values is None:
            self.cur.execute(querySQL)
        elif values is not None:
            self.cur.execute(querySQL, values)
        result = self.cur.fetchone()
        return result

    def queryManyFromDB(self, querySQL, values = None):

        self.checkConnectionStatus()
        self.cur = self.__dbConnector.cursor()

        if values is None:
            self.cur.execute(querySQL)
        elif values is not None:
            self.cur.execute(querySQL, values)
        result = self.cur.fetchall()
        return result

    def queryManyFromDBUsingDictCursor(self, querySQL, values=None):

        self.checkConnectionStatus()
        self.cur = self.__dbConnector.cursor(pymysql.cursors.DictCursor)
        if values is None:
            self.cur.execute(querySQL)
        elif values is not None:
            self.cur.execute(querySQL, values)
        results = self.cur.fetchall()
        return results

    def queryOneFromDBUsingDictCursor(self, querySQL, values=None):

        self.checkConnectionStatus()
        self.cur = self.__dbConnector.cursor(pymysql.cursors.DictCursor)
        if values is None:
            self.cur.execute(querySQL)
        elif values is not None:
            self.cur.execute(querySQL, values)
        result = self.cur.fetchone()
        return result

    def insertOneIntoDB(self, insertSQL, values = None):

        self.checkConnectionStatus()
        self.cur = self.__dbConnector.cursor()

        if values is None:
            self.cur.execute(insertSQL)
        elif values is not None:
            self.cur.execute(insertSQL, values)

    def insertManyIntoDB(self, insertSQL, values = None):

        self.checkConnectionStatus()
        self.cur = self.__dbConnector.cursor()

        if values is None:
            self.cur.executemany(insertSQL)
        elif values is not None:
            self.cur.executemany(insertSQL, values)

    def updateManyIntoDB(self, updateSQL, values):

        self.checkConnectionStatus()
        self.cur = self.__dbConnector.cursor()

        self.cur.executemany(updateSQL, values)

    def commitQuery(self):

        self.__dbConnector.commit()

    def closeCursor(self):

        if hasattr(self, 'cur'):
            self.cur.close()

    def getLastRowCount(self):

        if hasattr(self, 'cur'):
            return self.cur.rowcount
        else:
            return None

    def beginTransaction(self):

        self.__dbConnector.begin()

    def rollbackInsertion(self):

        self.__dbConnector.rollback()



















