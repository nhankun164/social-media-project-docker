
import math
import traceback
from .BuildRefinedText import *
from .MysqlConnector import MysqlConnector
from .TwitterErrors import ExtractTweetsFromDBError, ParseTweetsInPipelineError, CheckTweetExistenceError, InsertTweetsError, UpdateTweetsError
from .client import EmotionalAndSentimentalProcesser

SCHEMA_NAME = "MyDatabase"
NAME_TABLE_TWITTER = SCHEMA_NAME + ".twitter_table"
NAME_COMMENTS = SCHEMA_NAME + ".comments"

TWITTER_NAME = "twitter"

MESSAGE_PARSE_TWEET_ERROR = "Error occurs at parsing tweets."
MESSAGE_GET_TWEETS_ERROR = "Error occurs at getting tweets from twitter_table"
MESSAGE_INSERT_TWEETS_ERROR = "Errors occur at inserting tweets into comments table."
MESSAGE_UPDATE_TWEETS_ERROR = "Errors occur at updating tweets."
MESSAGE_CHECK_TWEETS_EXISTENCE_ERROR = "Error occurs at checking tweets existence at comments table."
MESSAGE_CLOSE_DB_IN_EXTRACT_TWEETS = "Close database connection in extractTweetsFromDB method due to errors."
MESSAGE_CLOSE_DB_IN_GET_TWEETS = "Close database conncection in getTweetsAndProcess method."
MESSAGE_CLOSE_DB_IN_GET_DUPLICATED_TWEETS = "Close database connection at getTweetsAlreadyInComments method due to errors"
MESSAGE_CLOSE_DB_IN_INSERT_TWEETS = "Close database connection in loadIntoCommentsTable due to errors."
MESSAGE_EMPTY_TWEETS = "Empty tweets returned from database."
MESSAGE_START = "Start....it may take a while..."
MESSAGE_END = "End..."

INVALID_CRYSTALFEEL_ERROR = "Invalid CrystalFeel output."


class CrystalFeelInfo:

    def __init__(self, crystalfeel_output_json):

        self.angerScore = crystalfeel_output_json["intensity_scores"]["tAnger"]
        self.fearScore = crystalfeel_output_json["intensity_scores"]["tFear"]
        self.joyScore = crystalfeel_output_json["intensity_scores"]["tJoy"]
        self.sadnessScore = crystalfeel_output_json["intensity_scores"]["tSadness"]
        self.valence = crystalfeel_output_json["intensity_scores"]["tValence"]
        self.emotionScore = crystalfeel_output_json["labels"]["tEmotionScore"]
        self.sentimentScore = crystalfeel_output_json["labels"]["tSentimentScore"]
        self.emoitonType = crystalfeel_output_json["labels"]["tEmotion"]
        self.sentimentType = crystalfeel_output_json["labels"]["tSentiment"]
        self.itemsCount = len(crystalfeel_output_json.keys())

class TwitterParse:

    def __init__(self, logger):
        self.__logger = logger
        self.__esProcessor = EmotionalAndSentimentalProcesser(logger)

    def __getCrystalFeelOutput(self, text):

        e_json = self.__esProcessor.method(text)  # get sentimental and emotional values of a post
        return e_json

    def __getHashTags(self, text):

        hashtags = '-'
        regex = re.compile('[^a-zA-Z]')
        post = (re.sub(r'https?:\/\/.*[\r\n]*', '', text.strip(), flags=re.MULTILINE)).strip()
        tokens = re.findall(r'[#]\S*', post)
        for token in set(tokens):
            s = regex.sub('', token)
            if len(s) > 2:
                token = re.sub('[,-;_).%&$]$', '', token)
                if token not in hashtags:
                    if hashtags == '-':
                        hashtags = token
                    else:
                        hashtags = hashtags + ',' + token
        return hashtags

    def parseTweetsToBeLoaded(self, tweets):
        
        def isInvalidCrystalFeelOutput(output):

            return True if len(output.keys()) == 1 \
                        or math.isnan(output["intensity_scores"]["tFear"]) \
                        or math.isnan(output["intensity_scores"]["tAnger"]) \
                        or math.isnan(output["intensity_scores"]["tJoy"]) \
                        or math.isnan(output["intensity_scores"]["tSadness"]) \
                        or math.isnan(output["intensity_scores"]["tValence"]) else False

        tweetsParsedPerPage = []
        count = 1
        try:
            for tweet in tweets:
                text = tweet["tweet_text"]
                crystalFeelOutput = self.__getCrystalFeelOutput(text)
                if crystalFeelOutput is None:
                    self.__logger.log_info("The processed tweet is none, skip it.")
                    continue
                if isInvalidCrystalFeelOutput(crystalFeelOutput):
                    self.__logger.log_info("The processed tweet is incomplete, skip it.")
                    self.__logger.log_info("The processed tweet: {}".format(str(crystalFeelOutput)))
                    continue
                crystalFeelInfo = CrystalFeelInfo(crystalFeelOutput)
                angerScore = crystalFeelInfo.angerScore
                fearScore = crystalFeelInfo.fearScore
                joyScore = crystalFeelInfo.joyScore
                sadnessScore = crystalFeelInfo.sadnessScore
                valence = crystalFeelInfo.valence
                emotionScore = crystalFeelInfo.emotionScore
                sentimentScore = crystalFeelInfo.sentimentScore
                emotionType = crystalFeelInfo.emoitonType
                sentimentType = crystalFeelInfo.sentimentType
                createDate = tweet["create_date_unix"]
                topicId = tweet["topic_id"]
                tweetId = tweet["tweet_id"]
                relevance = tweet["relevance"]
                source = tweet["source"]
                url = tweet["url"]
                hashTags = self.__getHashTags(text)
                refinedText = getRefinedText(text)
                tweetParsed = ['-', createDate, topicId, relevance, text, source, url, hashTags, refinedText,
                               angerScore, fearScore, joyScore, sadnessScore, valence, emotionScore, sentimentScore,
                               emotionType, sentimentType, tweetId]
                self.__logger.log_debug("No {} tweet has been processed.".format(count))
                tweetsParsedPerPage.append(tweetParsed)
                count += 1
        except Exception:
            raise ParseTweetsInPipelineError(MESSAGE_PARSE_TWEET_ERROR)
        return tweetsParsedPerPage


class TwitterPipeline:

    def __init__(self, topicId, keywords, logger, flagOfWarnOnly):
        self.__topicId = topicId
        self.__logger = logger
        self.__con = MysqlConnector()
        self.__tweetsPerPage = 500
        self.__tweetParser = TwitterParse(logger)

    def __getExractTweetsSQL(self):

        sqlExtractTweets = "SELECT `tweet_id`, `create_date_unix`, `topic_id`, `relevance`, `tweet_text`, `source`, `url` \
                            FROM " + NAME_TABLE_TWITTER + " \
                            WHERE `topic_id` = %s \
                            AND `processed` = %s \
                            AND `source` = %s"
        return sqlExtractTweets

    def __getExistingTweetsSQL(self):

        sqlGetExistingTweets = "SELECT `tweet_id`, `source`, `topic_id` \
                                FROM " + NAME_COMMENTS + " \
                                WHERE `source` = %s \
                                AND `topic_id` = %s \
                                AND `tweet_id` IN %s"
        return sqlGetExistingTweets

    def __getLoadTweetsSQL(self):

        sqlLoadTweets = "INSERT INTO " + NAME_COMMENTS + " \
                        (`post_id`, `create_date_unix`, `topic_id`, `relevance`, `content`, `source`, `source_link`, \
                        `hash_tags`, `refined_text`, `anger_score`, `fear_score`, `joy_score`, `sadness_score`, \
                        `valence`, `emotion_score`, `sentiment_score`, `emotion_category`, `sentiment_category`, `tweet_id`) \
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        return sqlLoadTweets

    def __getUpdateTweetsSQL(self):

        sqlUpdateTweets = "UPDATE " + NAME_TABLE_TWITTER + " \
                           SET `processed` = %s \
                           WHERE `topic_id` = %s \
                           AND `tweet_id` = %s"
        return sqlUpdateTweets

    def __extractTweetsFromDB(self):

        self.__sqlExtractTweets = self.__getExractTweetsSQL()
        self.__logger.log_debug(self.__sqlExtractTweets)
        try:
            tweets = self.__con.queryManyFromDBUsingDictCursor(self.__sqlExtractTweets, (self.__topicId, 0, TWITTER_NAME))
            return tweets
        except Exception:
            if not self.__con.isClosed():
                self.__con.closeCursor()
                self.__con.closeConnection()
                self.__logger.debug_error(MESSAGE_CLOSE_DB_IN_EXTRACT_TWEETS)
            raise ExtractTweetsFromDBError(MESSAGE_GET_TWEETS_ERROR)

    def __tweetsIsEmpty(self):

        return self.__tweetsTotal is None or len(self.__tweetsTotal) == 0

    def __loadTweetsByExcutingSQL(self):

        try:
            self.__con.insertManyIntoDB(self.__sqlLoadTweets, self.__parsedTweetsToBeLoaded)
        except Exception:
            raise InsertTweetsError(MESSAGE_INSERT_TWEETS_ERROR)

    def __updateTweetsToBeProcessed(self):

        def getUpdatedInfo():

            updatedInfo = [(1, self.__topicId, tweetId) for tweetId in self.__tweetsIds]
            return updatedInfo

        updatedInfo = getUpdatedInfo()
        try:
            self.__con.updateManyIntoDB(self.__sqlUpdateTweetsToBeProcessed, updatedInfo)
        except Exception:
            raise UpdateTweetsError(MESSAGE_UPDATE_TWEETS_ERROR)

    def __loadIntoCommentsTableAndMarkedProcessed(self):

        self.__sqlLoadTweets = self.__getLoadTweetsSQL()
        self.__sqlUpdateTweetsToBeProcessed = self.__getUpdateTweetsSQL()
        try:
            self.__con.beginTransaction()
            self.__loadTweetsByExcutingSQL()
            self.__updateTweetsToBeProcessed()
            self.__con.commitQuery()
        except Exception:
            if not self.__con.isClosed():
                self.__con.rollbackInsertion()
                self.__con.closeCursor()
                self.__con.closeConnection()
                self.__logger.log_error(MESSAGE_CLOSE_DB_IN_INSERT_TWEETS)
            raise

    def __processTweetsWithCrystalFeelAlgorithmAndLoad(self):

        def getPageNum():

            return int(numTweets / self.__tweetsPerPage) + 1 if numTweets % self.__tweetsPerPage != 0 else int(numTweets / self.__tweetsPerPage)
        
        def emptyTweetsOnePage():
            
            return self.__parsedTweetsToBeLoaded is None or len(self.__parsedTweetsToBeLoaded) == 0

        try:
            numTweets = len(self.__tweetsTotal)
            numPages = getPageNum()
            self.__logger.log_info("The number of pages: {}".format(numPages))
            for i in range(0, numPages):
                startIndex = i * self.__tweetsPerPage
                endIndex = (i + 1) * self.__tweetsPerPage
                self.__tweets = self.__tweetsTotal[startIndex:endIndex]
                self.__removeDuplicatedTweets()
                self.__parsedTweetsToBeLoaded = self.__tweetParser.parseTweetsToBeLoaded(self.__tweets)
                if emptyTweetsOnePage():
                    self.__logger.log_info("No processed tweets at page {}, skip to next page".format(i+1))
                    continue
                self.__logger.log_info("Finish processing tweets at page {} out of pages {}".format(i+1, numPages))
                self.__loadIntoCommentsTableAndMarkedProcessed()
        except ParseTweetsInPipelineError:
            raise

    def __removeDuplicatedTweets(self):

        def getTweetsIds():

            ids = [tweet["tweet_id"] for tweet in self.__tweets]
            return ids

        def getTweetsAlreadyInComments():

            try:
                self.__logger.log_info(TWITTER_NAME)
                self.__logger.log_info(self.__topicId)
                tweetsExists = self.__con.queryManyFromDBUsingDictCursor(self.__sqlGetExistingTweets, (TWITTER_NAME, self.__topicId, self.__tweetsIds))
                self.__logger.log_debug("The tweets already in comments table: {}".format(tweetsExists))
                return tweetsExists
            except Exception:
                if not self.__con.isClosed:
                    self.__con.closeCursor()
                    self.__con.closeConnection()
                    self.__logger.log_error(MESSAGE_CLOSE_DB_IN_GET_DUPLICATED_TWEETS)
                raise CheckTweetExistenceError(MESSAGE_CHECK_TWEETS_EXISTENCE_ERROR)

        def getTweetsWithoutDuplication():

            return [tweet for tweet in self.__tweets if {"tweet_id": tweet["tweet_id"], "source": TWITTER_NAME, "topic_id": self.__topicId} not in tweetsAlreadyInComments] if len(tweetsAlreadyInComments) != 0 else self.__tweets

        try:
            self.__sqlGetExistingTweets = self.__getExistingTweetsSQL()
            self.__tweetsIds = getTweetsIds()
            tweetsAlreadyInComments = getTweetsAlreadyInComments()
            self.__tweets = getTweetsWithoutDuplication()
            self.__logger.log_info("The number of unqiue tweets at one page to be loaded is: {}".format(len(self.__tweets)))
        except Exception:
            raise

    def __getTweetsAndProcess(self):

        try:
            self.__tweetsTotal = self.__extractTweetsFromDB()
            if self.__tweetsIsEmpty():
                self.__logger.log_info(MESSAGE_EMPTY_TWEETS)
                return
            self.__logger.log_info("The number of tweets returned: {}".format(len(self.__tweetsTotal)))
            self.__logger.log_info(MESSAGE_START)
            self.__processTweetsWithCrystalFeelAlgorithmAndLoad()
            self.__logger.log_info(MESSAGE_END)
        except ExtractTweetsFromDBError:
            raise
        except ParseTweetsInPipelineError:
            raise
        except CheckTweetExistenceError:
            raise
        except InsertTweetsError:
            raise
        except UpdateTweetsError:
            raise
        except Exception:
            raise
        finally:
            if not self.__con.isClosed():
                self.__con.closeCursor()
                self.__con.closeConnection()
                self.__logger.log_info(MESSAGE_CLOSE_DB_IN_GET_TWEETS)

    def start(self):

        try:
            self.__getTweetsAndProcess()
        except Exception as e:
            traceback.print_exc()
            self.__logger.log_error(str(e))
            raise e


