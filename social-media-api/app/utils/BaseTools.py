import time
import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from .BaseOperationErrors import SelectSourcesAndTypesError, GetCommentsFromDatabaseError, \
    InvalidSourcesError, DatabaseConnectionIsLost, DatesAreInvalid, PageInfoInvalid, SourcesListInvalid, \
    TopicIdInvalid, SentimentInvalid, GetPostsFromDatabaseError
from .default_db_session import default_db_session_transaction
from ..models.model import Source
from sqlalchemy import and_
from sqlalchemy.sql import func, label, exists

class ParamChecker:

    def inValidSources(self, sources):

        return sources is None or type(sources) != str or sources == ""

    def invalidPageInfo(self, pageId, pageLimit):

        return (pageId is None or pageLimit is None) or (type(pageId) != int or type(pageLimit) != int)

    def invalidSourcesList(self, sourcesList):

        return sourcesList is None or type(sourcesList) != list

    def invalidTopicId(self, topicId):

        return topicId is None or type(topicId) != int

    def invalidUnixDates(self, fromDate, endDate):

        return (fromDate is None or endDate is None) or \
               (type(fromDate) != int or type(endDate) != int)

    def invalidHumanreableDates(self, fromDate, endDate):

        return (fromDate is None or endDate is None) or (type(fromDate) != str or type(endDate) != str) or (fromDate == "" or endDate == "")

    def invalidConnector(self, con):

        return con is None or con.isClosed()

    def invalidSentiment(self, sentiment):

        return sentiment is None or sentiment == "" or type(sentiment) != str


class BaseTools:

    SECONDS_PER_DAY = 86400
    DEFAULT_SELECTED_VALUE = 1
    CASE_INVALID_HYPHEN = "-"
    DEFAULT_EMOTION_COUNT = 1
    DEFAULT_RELEVANCE = 1
    DEFAULT_START_INDEX_PAGINATION = 0
    DEFAULT_END_INDEX_PAGINATION = -1
    EMOTION_TYPE_INDEX = 5
    SENTIMENT_TYPE_INDEX = 7
    NAME_SOCIAL_MEDIA_SOURCES_LIST = "socialMediaSourcesList"
    NAME_SOURCES_LIST = "sourcesList"
    SCHEMA_NAME = "social_media_analysis"
    NAME_SOURCE = "sources"
    NAME_REDDIT_SOURCE = "reddit"
    NAME_FACEBOOK_SOURCE = "facebook"
    NAME_POSTS_TABLE = SCHEMA_NAME + ".posts"
    NAME_COMMENTS_TABLE = SCHEMA_NAME + ".comments"
    NAME_SOURCES_TABLE = SCHEMA_NAME + "." + NAME_SOURCE
    EMOTION_TYPE_LIST = ["anger", "fear", "joy", "sadness", "no specific emotion"]
    SENTIMENT_TYPE_LIST = ["positive", "negative", "neutral", "very positive", "very negative"]
    MESSAGE_SELECT_SOURCE_TYPE_ERROR = "Errors occur at selecting sources and types from database"
    MESSAGE_GET_POSTS_ERROR = "Error occurs at getting posts from database."
    MESSAGE_GET_COMMENTS_ERROR = "Error occurs at getting comments from database."
    MESSAGE_CLOSE_DB_MESSAGE = "Database connection is closed at parent class."
    MESSAGE_INVALID_SOURCES = "Invalid sources."
    MESSAGE_INVALID_DB_CONNECTION = "Database connection is not alive."
    MESSAGE_INVALID_DATES = "Dates are invalid"
    MESSAGE_INVALID_PAGE_INFO = "Page limit and/or page id is/are invalid."
    MESSAGE_INVALID_SOURCES_LIST = "Sources list is invalid."
    MESSAGE_INVALID_TOPIC_ID = "Topic id is invalid."
    MESSAGE_INVALID_SENTIMENT = "Sentiment is invalid"
    SOURCE_TABLE = Source

    def __init__(self, con, is_orm_mode=False):

        self.__con = con
        self.__checker = ParamChecker()
        self.__is_orm_mode = is_orm_mode

    def __getSqlToExtractSocialMediaPosts(self):

        sqlGetSocialMediaPosts = "SELECT `create_date_unix`, `description`, `id`, `post_id`, `title`, `source`, `source_link`, \
                              `highest_freq_emotion_count`, `highest_freq_emotion_type`, `highest_freq_positive_sentiment_count`,\
                              `highest_freq_negative_sentiment_count`, `relevance`, `topic_id` \
                               FROM " + self.NAME_POSTS_TABLE + " \
                               WHERE `topic_id` = %s \
                               AND `create_date_unix` BETWEEN %s AND %s \
                               AND `relevance` = %s \
                               AND `source` IN %s"
        return sqlGetSocialMediaPosts

    def __getSqlToExtractRssTwitterPosts(self):

        sqlGetRssTwitterPosts = "SELECT `create_date_unix`, `id`, `tweet_id`, `content`, `source`, `source_link`, `emotion_category`\
                                , `sentiment_category`, `relevance`, `topic_id`\
                                FROM " + self.NAME_COMMENTS_TABLE + " \
                                WHERE `topic_id` = %s \
                                AND `create_date_unix` BETWEEN %s AND %s \
                                AND `relevance` = %s \
                                AND `source` IN %s"
        return sqlGetRssTwitterPosts

    def __getSqlToExtractComments(self):

        sqlGetComments = "SELECT `content`, `relevance`, `id`, `create_date_unix`, `emotion_score`, `emotion_category`, \
                                  `sentiment_score`, `sentiment_category`, `source`, `source_link`, `topic_id`, `anger_score`, \
                                  `fear_score`, `joy_score`, `sadness_score`, `valence`, `post_id` FROM " + self.NAME_COMMENTS_TABLE + " \
                                   WHERE `relevance` = %s \
                                   AND `topic_id` = %s \
                                   AND `create_date_unix` BETWEEN %s AND %s \
                                   AND `source` IN %s"
        return sqlGetComments

    def __getSelectTypeAndSourcesSQL(self):

        sqlSelectTypeAndSources = "SELECT `source_name`, `source_type` FROM " + self.NAME_SOURCES_TABLE + " WHERE `selected` = %s"
        return sqlSelectTypeAndSources

    def __parseSources(self, sources):

        def isSourcesSeperateedByComma():

            return "," in sources

        def getSourcesFromSelectedSources(sources, target):

            selectedSources = list()
            indexSourceName = 0
            indexSourceType = 1
            for st in sources:
                if target == "all":
                    selectedSources.append(st[indexSourceName])
                elif target == st[indexSourceType]:
                    selectedSources.append(st[indexSourceName])
            return selectedSources

        def getSourcesFromMultipleSourcesWordsSeperatedByComma():

            nonlocal sourcesReturned, sources, selectedSourcesAndTypes
            sourcesAfterSplit = sources.split(",")
            if "all social media" in sourcesAfterSplit:
                sourcesReturned = getSourcesFromSelectedSources(selectedSourcesAndTypes, "social media")
            if "all news" in sourcesAfterSplit:
                sourcesReturned += getSourcesFromSelectedSources(selectedSourcesAndTypes, "news")
            for source in sourcesAfterSplit:
                if source != "all social media" and source != "all news":
                    sourcesReturned.append(source)

        def getSourcesFromSingleSourceWord():

            nonlocal sourcesReturned, sources, selectedSourcesAndTypes
            if sources == "all sources":
                sourcesReturned = getSourcesFromSelectedSources(selectedSourcesAndTypes, "all")
            if "all social media" == sources:
                sourcesReturned = getSourcesFromSelectedSources(selectedSourcesAndTypes, "social media")
            if "all news" == sources:
                sourcesReturned = getSourcesFromSelectedSources(selectedSourcesAndTypes, "news")
            if sources != "all sources" and sources != "all social media" and sources != "all news":
                sourcesReturned.append(sources)

        try:
            sources = sources.lower().strip()
            selectedSourcesAndTypes = self.__getSelectedSourcesAndTypesFromDB() if not self.__is_orm_mode else self.__getSelectedSourcesAndTypesByORM()
            sourcesReturned = list()

            if isSourcesSeperateedByComma():
                getSourcesFromMultipleSourcesWordsSeperatedByComma()
            else:
                getSourcesFromSingleSourceWord()
            return sourcesReturned
        except Exception:
            raise

    def getSourcesList(self, sources):

        if self.__checker.inValidSources(sources):
            raise InvalidSourcesError(self.MESSAGE_INVALID_SOURCES)
        return self.__parseSources(sources)
    
    def __getSelectedSourcesAndTypesByORM(self):
        return self.__con.query(self.SOURCE_TABLE.source_name, self.SOURCE_TABLE.source_type).filter(self.SOURCE_TABLE.selected==1).all()

    def __getSelectedSourcesAndTypesFromDB(self):

        if self.__checker.invalidConnector(self.__con):
            raise DatabaseConnectionIsLost(self.MESSAGE_INVALID_DB_CONNECTION)
        try:
            self.__sqlSelectTypeAndSources = self.__getSelectTypeAndSourcesSQL()
            return self.__con.queryManyFromDB(self.__sqlSelectTypeAndSources, self.DEFAULT_SELECTED_VALUE)
        except Exception:
            raise SelectSourcesAndTypesError(self.MESSAGE_SELECT_SOURCE_TYPE_ERROR)

    def getDateRange(self, fromDate, endDate):
        def parseDate():
            startDate = int(time.mktime(time.strptime(fromDate, '%Y-%m-%d')))
            toDate = int(time.mktime(time.strptime(endDate, '%Y-%m-%d')))
            theLastSecondOfToDate = toDate + self.SECONDS_PER_DAY - 1
            return startDate, toDate

        if self.__checker.invalidHumanreableDates(fromDate, endDate):
            raise DatesAreInvalid(self.MESSAGE_INVALID_DATES)

        return parseDate()

    def getPaginationIndexes(self, pageId, pageLimit):

        if self.__checker.invalidPageInfo(pageId, pageLimit):
            raise PageInfoInvalid(self.MESSAGE_INVALID_PAGE_INFO)

        startIndex = (pageId-1)*pageLimit
        endIndex = startIndex + pageLimit
        return startIndex, endIndex

    def filterRSSAndTwitterFromSources(self, sourcesList):

        """
        This method is used to filter rss and twitter data source from sources list
        Although rss and twitter are stored in our comments table. They should
        be obtained from csv get post method, not from get comments method.
        :return:
        """

        if self.__checker.invalidSourcesList(sourcesList):

            raise SourcesListInvalid(self.MESSAGE_INVALID_SOURCES_LIST)

        socialMediaSourcesList = [source for source in sourcesList if source == self.NAME_REDDIT_SOURCE or source == self.NAME_FACEBOOK_SOURCE]
        return socialMediaSourcesList

    def getCommentsByTopicIdAndDateAndSources(self, topicId, fromDate, endDate, sourcesList):

        if self.__checker.invalidTopicId(topicId):
            raise TopicIdInvalid(self.MESSAGE_INVALID_TOPIC_ID)

        if self.__checker.invalidUnixDates(fromDate, endDate):
            raise DatesAreInvalid(self.MESSAGE_INVALID_DATES)

        if self.__checker.invalidSourcesList(sourcesList):
            raise SourcesListInvalid(self.MESSAGE_INVALID_SOURCES_LIST)

        if self.__checker.invalidConnector(self.__con):
            raise DatabaseConnectionIsLost(self.MESSAGE_INVALID_DB_CONNECTION)

        try:
            sqlGetComments = self.__getSqlToExtractComments()
            comments = self.__con.queryManyFromDB(sqlGetComments, (self.DEFAULT_RELEVANCE, topicId, fromDate, endDate, sourcesList))
            return comments
        except Exception:
            raise GetCommentsFromDatabaseError(self.MESSAGE_GET_COMMENTS_ERROR)

    def __splitSourcesIntoRssTwitterAndSocialMedia(self, sourcesList):

        """
        This method is used to split social media sources ex: reddit, facebook and non-social media sources ex: rss, twitter.
        :return:
        """

        socialMediaSourcesList = [source for source in sourcesList if source == self.NAME_REDDIT_SOURCE or source == self.NAME_FACEBOOK_SOURCE]
        rssTwitterSourcesList = [source for source in sourcesList if source not in socialMediaSourcesList]
        return socialMediaSourcesList, rssTwitterSourcesList

    def getFeedsByTopicIdAndDateAndSources(self, topicId, fromDate, endDate, sourcesList):

        if self.__checker.invalidTopicId(topicId):
            raise TopicIdInvalid(self.MESSAGE_INVALID_TOPIC_ID)

        if self.__checker.invalidUnixDates(fromDate, endDate):
            raise DatesAreInvalid(self.MESSAGE_INVALID_DATES)

        if self.__checker.invalidSourcesList(sourcesList):
            raise SourcesListInvalid(self.MESSAGE_INVALID_SOURCES_LIST)

        if self.__checker.invalidConnector(self.__con):
            raise DatabaseConnectionIsLost(self.MESSAGE_INVALID_DB_CONNECTION)
        sqlGetSocialMediaPosts = self.__getSqlToExtractSocialMediaPosts()
        try:
            socialMediaSources = self.filterRSSAndTwitterFromSources(sourcesList)
            posts = []
            if len(socialMediaSources) != 0:
                socialMediaPosts = self.__con.queryManyFromDB(sqlGetSocialMediaPosts, (topicId, fromDate, endDate, self.DEFAULT_RELEVANCE, socialMediaSources))
                posts += socialMediaPosts
            return posts
        except Exception:
            raise GetPostsFromDatabaseError(self.MESSAGE_GET_POSTS_ERROR)

    def getSentimenalAndEmotionalLists(self, sentiment):

        def parseSentimentAndEmotion():

            sentiments = sentiment if sentiment is None else sentiment.lower().strip()
            sentimentEmotionList = []

            if not self.isSentimentNotProvided(sentiment):
                if "," in sentiments:
                    sentimentEmotionList = [senEmo.strip() for senEmo in sentiments.split(",")]
                else:
                    sentimentEmotionList.append(sentiments)
            print("'list", sentimentEmotionList)
            return sentimentEmotionList

        sentimentEmotionList = parseSentimentAndEmotion()
        sentimentList = []
        emotionList = []
        if len(sentimentEmotionList) != 0:
            for se in sentimentEmotionList:
                se = se.lower().strip()
                if se in self.EMOTION_TYPE_LIST:
                    emotionList.append(se)
                elif se in self.SENTIMENT_TYPE_LIST:
                    sentimentList.append(se)
        return sentimentList, emotionList, sentimentEmotionList

    def selectCommentsBySentimentalAndEmotionalTypes(self, comments, sentiment):

        if self.__checker.invalidSentiment(sentiment):

            raise SentimentInvalid(self.MESSAGE_INVALID_SENTIMENT)

        sentimentList, emotionList, sentimentEmotionList = self.getSentimenalAndEmotionalLists(sentiment)
        results = []

        for comment in comments:
            if len(sentimentEmotionList) != 0:
                if len(emotionList) != 0 and len(sentimentList) != 0 and comment[self.EMOTION_TYPE_INDEX] in emotionList and comment[self.SENTIMENT_TYPE_INDEX] in sentimentList:
                    results.append(comment)
                if len(emotionList) != 0 and len(sentimentList) == 0 and comment[self.EMOTION_TYPE_INDEX] in emotionList:
                    results.append(comment)
                if len(sentimentList) != 0 and len(emotionList) == 0 and comment[self.SENTIMENT_TYPE_INDEX] in sentimentList:
                    results.append(comment)
            else:
                results.append(comment)
        return results
    
    def isSentimentNotProvided(self, sentiment):
        if sentiment == "fear, sadness":
            return False
        return sentiment is None or sentiment == "" or sentiment == "all" or sentiment not in ["positive" , "negative"]
            
            
            



