import time
import os, sys
import calendar
import datetime
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from .BaseTools import BaseTools
from ..models.model import Comment
from sqlalchemy import and_, select, or_
from sqlalchemy.sql import func, case
from sqlalchemy.dialects import mysql
import abc

class BaseChart:
    
    ORM_MODE_OPEN = True
    COMMENT_TABLE = Comment
    RELEVANCE_VAL = 1
    SUCCESS_CODE = 200
    SUCCESS_MESSAGE = 'success'
    DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
    SECONDS_PER_DAY = 86400
    NEGATIVE = 'negative'
    VERY_NEGATIVE = 'very negative'
    POSITIVE = 'positive'
    VERY_POSITIVE = 'very positive'
    NEUTRAL = 'neutral'
    TOTAL = 'total'
    HYPEN = '-'
    JOY = 'joy'
    FEAR = 'fear'
    SADNESS = 'sadness'
    ANGER = 'anger'
    NO_SPECIFIC_EMOTION = 'no specific emotion'
    
    def __init__(self, from_date, to_date, topic_id, sources, type, db_session, logger):
        self.tool = BaseTools(db_session, self.ORM_MODE_OPEN)
        self.from_date, self.to_date = self.tool.getDateRange(from_date, to_date)
        self.topic_id = topic_id
        self.sources_list = self.tool.getSourcesList(sources)
        self.type = type
        self.db_session = db_session
        self.logger = logger
    
    @abc.abstractclassmethod
    def get_chart(self):
        return
    
    @abc.abstractclassmethod
    def _convert_chart_data_to_json(self):
        return
    
    def _get_emotion_or_sentiment_of_comments(self, sentiment_or_emotion_tag):
        sentiment_or_emotion = getattr(self.COMMENT_TABLE, sentiment_or_emotion_tag)
        print('sentiment_or_emotion------------------ ', sentiment_or_emotion)
        print('self.sources_list----------------- ', self.sources_list)
        print('RELEVANCE_VAL-------------- ', self.RELEVANCE_VAL)
        print('from_date---------------- ', self.from_date)
        print('to_date---------------- ', self.to_date)
        query_to_get_sentiment_or_emotion = self.db_session.query(self.COMMENT_TABLE.create_date_unix,
                                                                  sentiment_or_emotion,
                                                                  self.COMMENT_TABLE.topic_id).filter(
                                                                      and_(self.COMMENT_TABLE.topic_id == self.topic_id,
                                                                           self.COMMENT_TABLE.source.in_(self.sources_list),
                                                                           self.COMMENT_TABLE.relevance == self.RELEVANCE_VAL,
                                                                           self.COMMENT_TABLE.create_date_unix.between(self.from_date, self.to_date)))\
                                                                  .order_by(self.COMMENT_TABLE.create_date_unix.asc())
        self._print_sql_statement(query_to_get_sentiment_or_emotion)
        sentiments_or_emotions_data = query_to_get_sentiment_or_emotion.all()
        # print('sentiments_or_emotions_data-------------- ', sentiments_or_emotions_data)
        return sentiments_or_emotions_data

    def _calculate_sentiment_or_emotion_counts_changes(self, count, count_before_time_interval):
        num_of_counts = len(count)
        changes_in_ratios = self.__calculate_count_ratio(num_of_counts, count, count_before_time_interval)
        return changes_in_ratios
    
    def _get_emotions_count_from_db(self, from_date=None, to_date=None):
        
        def get_specific_dates_if_provided(from_date, to_date):
            if from_date is None or to_date is None:
                from_date, to_date = self.from_date, self.to_date
            else:
                from_date, to_date = from_date, to_date
            return from_date, to_date

        def convert_result_to_list():
            # convert for example [(2, Decimal('0'), Decimal('2'), Decimal('0'))] to [2, Decimal('0'), Decimal('2'), Decimal('0')]
            nonlocal emotions_count
            return list(emotions_count[0])

        def provide_default_emotion_counts_if_no_data_returned():
            _total_count_index = 0
            nonlocal emotions_count
            if emotions_count[_total_count_index] == 0:
                return (0,0,0,0,0)
            else:
                return emotions_count
        
        from_date, to_date = get_specific_dates_if_provided(from_date, to_date)
        case_joy_emotion, case_fear_emotion, case_sadness_emotion, case_anger_emotion = self.__get_case_query_for_emotions_chart()
        query_to_get_emotions_count = self.db_session.query(
                                        func.count(self.COMMENT_TABLE.id).label('total_metions'), 
                                        func.sum(case_joy_emotion.label('joy_emotion')), 
                                        func.sum(case_fear_emotion.label('fear_emotion')),
                                        func.sum(case_sadness_emotion.label('sadness_emotion')),
                                        func.sum(case_anger_emotion.label('anger_emotion'))
                                        ).filter(
                                                and_(self.COMMENT_TABLE.topic_id == self.topic_id, 
                                                    self.COMMENT_TABLE.source.in_(self.sources_list),
                                                    self.COMMENT_TABLE.relevance == self.RELEVANCE_VAL,
                                                    self.COMMENT_TABLE.create_date_unix.between(from_date, to_date)
                                                    )
                                                )
        self._print_sql_statement(query_to_get_emotions_count)
        emotions_count = query_to_get_emotions_count.all()
        emotions_count = convert_result_to_list()
        emotions_count = provide_default_emotion_counts_if_no_data_returned()
        return emotions_count
    
    def _get_sentiments_count_from_db(self, from_date=None, to_date=None):
    
        def get_specific_dates_if_provided(from_date, to_date):
            if from_date is None or to_date is None:
                from_date, to_date = self.from_date, self.to_date
            else:
                from_date, to_date = from_date, to_date
            return from_date, to_date
            
        def provide_default_sentiment_counts_if_no_data_returned():
            _total_count_index = 0
            nonlocal sentiments_count
            if sentiments_count[_total_count_index] == 0:
                return (0,0,0,0)
            else:
                return sentiments_count
            
        def convert_result_to_list():
            # convert for example [(2, Decimal('0'), Decimal('2'), Decimal('0'))] to [2, Decimal('0'), Decimal('2'), Decimal('0')]
            nonlocal sentiments_count
            return list(sentiments_count[0])
        
        from_date, to_date = get_specific_dates_if_provided(from_date, to_date)
        case_positive_mentions, case_negative_mentions, case_neutral_mentions = self.__get_case_query_for_sentiments_chart()
        query_to_get_sentiments_count = self.db_session.query(
                                        func.count(self.COMMENT_TABLE.id).label('total_metions'), 
                                        func.sum(case_positive_mentions.label('positive_mentions')),
                                        func.sum(case_negative_mentions.label('negative_mentions')),
                                        func.sum(case_neutral_mentions.label('neutral_mentions'))
                                        ).filter(
                                                and_(self.COMMENT_TABLE.topic_id == self.topic_id, 
                                                    self.COMMENT_TABLE.source.in_(self.sources_list),
                                                    self.COMMENT_TABLE.relevance == self.RELEVANCE_VAL,
                                                    self.COMMENT_TABLE.create_date_unix.between(from_date, to_date)
                                                    )
                                                )
        self._print_sql_statement(query_to_get_sentiments_count)
        sentiments_count = query_to_get_sentiments_count.all()
        sentiments_count = convert_result_to_list()
        sentiments_count = provide_default_sentiment_counts_if_no_data_returned()
        return sentiments_count
    
    def _print_sql_statement(self, query):
        self.logger.log_info(str(query.statement.compile(dialect=mysql.dialect())))
    
    def __get_case_query_for_emotions_chart(self):
        EMOTION_CATEGORIES = ['joy', 'fear', 'sadness', 'anger']
        case_emotion_list = [case([(self.COMMENT_TABLE.emotion_category == emo, 1),], else_=0) for emo in EMOTION_CATEGORIES]
        return case_emotion_list

    def __get_case_query_for_sentiments_chart(self):
        case_positive_mentions = case(
            [
                (or_(self.COMMENT_TABLE.sentiment_category == 'positive', self.COMMENT_TABLE.sentiment_category == 'very positive'), 1),
            ], 
            else_ = 0)
        
        case_negative_mentions = case(
            [
                (or_(self.COMMENT_TABLE.sentiment_category == 'negative', self.COMMENT_TABLE.sentiment_category == 'very negative'), 1),
            ], 
            else_ = 0)
        
        case_neutral_mentions = case(
            [
                (self.COMMENT_TABLE.sentiment_category == 'neutral', 1),
            ], 
            else_ = 0)
        return case_positive_mentions, case_negative_mentions, case_neutral_mentions
    
    def __calculate_count_ratio(self, num_of_counts, count, count_before_time_interval):
        _default_ratio_when_denominator_is_zero = 1.00
        _default_ratio_when_no_data_found = 0.00
        change_ratios = list()
        
        def is_no_data_found():
            # deal with default val when data not found (0,0,0,0)
            return count_before_time_interval[count_index] == 0 and count[count_index] == 0
        
        def provide_ratio(ratio):
            change_ratios.append(ratio)
        
        def is_denominator_zero():
            return count_before_time_interval[count_index] == 0

        for count_index in range(0, num_of_counts):
            if is_no_data_found():
                provide_ratio(_default_ratio_when_no_data_found)
            elif is_denominator_zero():
                provide_ratio(_default_ratio_when_denominator_is_zero)
            else:
                ratio = float((count[count_index] - count_before_time_interval[count_index])/count_before_time_interval[count_index])
                provide_ratio(ratio)
        return change_ratios

    def _get_first_day_and_last_day_of_month(self, date):
        readable_date = self._convert_unix_timestamp_to_humanreadable_format(date)
        year = int(readable_date[0:4])
        month = int(self._extract_month_from_date(readable_date))
        _, num_of_days = calendar.monthrange(year, month)
        first_date = datetime.date(year, month, 1)
        first_date = first_date.strftime(self.DATE_FORMAT)
        last_date = datetime.date(year, month, num_of_days)
        last_date = last_date.strftime(self.DATE_FORMAT)
        first_date_unix = self._convert_humanreadable_date_to_unix_timestamp(first_date)
        last_date_unix = self._convert_humanreadable_date_to_unix_timestamp(last_date)
        return first_date_unix, last_date_unix + self.SECONDS_PER_DAY - 1

    def _extract_month_from_date(self, readable_date):  
        datetime_obj = datetime.datetime.strptime(readable_date, self.DATE_FORMAT)
        return datetime_obj.month

    def _convert_humanreadable_date_to_unix_timestamp(self, date_readable):
        return int(time.mktime(time.strptime(date_readable, self.DATE_FORMAT)))

    def _convert_unix_timestamp_to_humanreadable_format(self, unix_timestamp):
        return str(time.strftime(self.DATE_FORMAT, time.localtime(unix_timestamp)))

    
        
        