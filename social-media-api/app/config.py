import os

class Config(object):
    # _SQLALCHEMY_DATABASE_USERNAME = os.getenv('BACKEND_DB_USER')
    # _SQLALCHEMY_DATABASE_PASSWORD = os.getenv('BACKEND_DB_PASSWORD')
    # _SQLALCHEMY_DATABASE_HOSTNAME= os.getenv('BACKEND_DB_HOST')
    # _SQLALCHEMY_DATABASE_PORT= os.getenv('BACKEND_DB_PORT')
    # _SQLALCHEMY_DATABASE_DATABASE_NAME= os.getenv('BACKEND_DB_DATABASE')
    # SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{user_name}:{password}@{host}:{port}/{db_name}'.format(
    #     user_name=_SQLALCHEMY_DATABASE_USERNAME,
    #     host=_SQLALCHEMY_DATABASE_HOSTNAME,
    #     port=_SQLALCHEMY_DATABASE_PORT,
    #     password=_SQLALCHEMY_DATABASE_PASSWORD,
    #     db_name=_SQLALCHEMY_DATABASE_DATABASE_NAME
    # )
    _SQLALCHEMY_DATABASE_USERNAME = os.getenv('DB_USER', 'root')
    _SQLALCHEMY_DATABASE_PASSWORD = os.getenv('DB_PASSWORD', 'Nhoc12345')
    _SQLALCHEMY_DATABASE_HOSTNAME= os.getenv('DB_HOST', 'mysql')
    _SQLALCHEMY_DATABASE_DATABASE_NAME= os.getenv('DB_DATABASE', 'social_media_analysis')
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{user_name}:{password}@{host}/{db_name}'.format(
        user_name=_SQLALCHEMY_DATABASE_USERNAME,
        host=_SQLALCHEMY_DATABASE_HOSTNAME,
        password=_SQLALCHEMY_DATABASE_PASSWORD,
        db_name=_SQLALCHEMY_DATABASE_DATABASE_NAME
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False