from app.utils.default_db_session import DefaultDBSession, default_engine
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from .config import Config
from flask_restplus import Resource, Api
from flask_restplus_sqlalchemy import ApiModelFactory
from flask_cors import CORS


print("Before ----------------------")
app = Flask(__name__)
print("After ----------------------")
app.config.from_object(Config)
app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(resources={r'/api/*': {'origins': ['*', '*:3000', 'http://*:3000']}})
cors.init_app(app)
db = SQLAlchemy(app)

migrate = Migrate(app, db)

from .models import model

print('Ready Flask App !!!!')
api = Api(app, prefix='/api')

# Get all model to use in Controller
api_model_factory = ApiModelFactory(api=api, db=db)


from app.controllers.twitterController import Twitter, TwitterFilterDate
from app.controllers.rssController import Rss
from app.controllers.commentController import Comment, CommentFilterDate
from app.controllers.trendsController import Trends, TrendsFilterDate
from app.controllers.searchController import Search, SearchKeyword




# from app import routes

