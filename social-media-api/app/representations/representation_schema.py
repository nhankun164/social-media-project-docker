from app.models.model import Comment, Post
from marshmallow import Schema, fields
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, auto_field, SQLAlchemySchema


class CommentSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Comment
        include_relationships = True
        load_instance = True



class PostSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Post
        include_relationships = True
        load_instance = True



class CommentRepresentation(Schema):
    id = fields.Int()
    post_id = fields.Str()
    create_date_unix = fields.Int()
    topic_id = fields.Int()
    relevance = fields.Int()
    content = fields.Str()
    source = fields.Str()
    source_link = fields.Str()
    ne_data = fields.Str()
    senti_type = fields.Int()
    senti_flag = fields.Int()
    senti_val = fields.Float()
    pos_words = fields.Str()
    neg_words = fields.Str()
    senti_word_flag = fields.Int()
    hash_tags = fields.Str()
    refined_text = fields.Str()
    is_refined = fields.Int()
    anger = fields.Int()
    anxiety = fields.Int()
    sadness = fields.Int()
    satisfaction = fields.Int()
    happiness = fields.Int()
    excitement = fields.Int()
    sarcasm_score = fields.Float()
    sarcasm_flag = fields.Int()
    crystalfeel_out = fields.Str()
    crystalfeel_flag = fields.Int()
    anger_score = fields.Float()
    fear_score = fields.Float()
    joy_score = fields.Float()
    sadness_score = fields.Float()
    valence = fields.Float()
    emotion_score = fields.Float()
    sentiment_score = fields.Float()
    emotion_category = fields.Str()
    sentiment_category = fields.Str()
    tweet_id = fields.Str()


class PostRepresentation(Schema):
    id = fields.Int()
    description = fields.Str()
    topic_id = fields.Int()
    relevance = fields.Int()
    create_date_unix = fields.Int()
    title = fields.Str()
    source = fields.Str()
    source_link = fields.Str()
    post_id = fields.Str()
    highest_freq_emotion_count = fields.Int()
    highest_freq_emotion_type = fields.Str()
    highest_freq_positive_sentiment_count = fields.Int()
    highest_freq_negative_sentiment_count = fields.Int()
    processed = fields.Int()
    processed_state = fields.Str()