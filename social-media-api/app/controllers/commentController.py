from app.services.commonServices import generateTopicsByField
from app.services.commentsServices import getCommentDataByPeriodTime, getCommentDataByPeriodType
from app.services.rssServices import getRssDataByTime, updateImageSrcForRssLink
from flask.json import jsonify
from app.models.model import Category, Comment, RssTable, TwitterTable
from flask_restplus import Resource, fields, reqparse
from flask import request
from app import api, api_model_factory
from app.models.response import Response
from flask_cors import cross_origin

commentNS = api.namespace('comments', description='API Comment for social media')
commentModel = api_model_factory.get_entity(Comment.__tablename__)


commentPostSearchModel = commentNS.model('post_search', {
    'table': fields.String(required=True),
    'field': fields.String(required=True),
    'startDate': fields.Integer(required=True),
    'endDate': fields.Integer(required=True)
})

commentResponse = commentNS.model('comment', {
        'emotion_count': fields.Raw,
        'sentiment_count': fields.Raw,
        'data': fields.List(fields.Nested(commentModel))
})



commentListResponse = commentNS.model('commentList', {
        'code': fields.String(required=True),
        'message': fields.String(required=True),
        'data': fields.Nested(commentResponse)
})

listResponse = commentNS.model('list', {
        'code': fields.String(required=True),
        'message': fields.String(required=True),
        'data': fields.List(fields.String)
})


@commentNS.route('')
class Comment(Resource):
    @commentNS.marshal_with(commentListResponse)
    @commentNS.doc(
        params={
            'periodType': 'History, Current or Prediction',
        }
    )
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('periodType', type=str, required=True, help='period type is required.')
        args = parser.parse_args()

        data = getCommentDataByPeriodType(args.get('periodType'))
        return Response('200', 'Success', data)


@commentNS.route('/filterDate')
class CommentFilterDate(Resource):
    @commentNS.marshal_with(commentListResponse)
    @commentNS.doc(
        params={
            'startDate': 'from Date',
            'endDate': 'to Date'
        }
    )
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('startDate', type=int, required=True, help='start is required.')
        parser.add_argument('endDate', type=int, required=True, help='end is required.')
        args = parser.parse_args()

        data = getCommentDataByPeriodTime(args.get('startDate'), args.get('endDate'))
        return Response('200', 'Success', data)

    @commentNS.expect(commentPostSearchModel)
    @commentNS.marshal_with(listResponse)
    def post(self): 
        body = request.json
        data = generateTopicsByField(body['table'], body['field'], body['startDate'], body['endDate'])
        return Response('200', 'Success', data)


