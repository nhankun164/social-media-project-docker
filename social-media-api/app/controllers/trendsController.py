from app.services.trendsServices import getEmotionAndSentimentDataByFilterDate, getEmotionAndSentimentDataByPeriodType
from app.services.commentsServices import getCommentDataByPeriodTime
from app.services.rssServices import getRssDataByTime, updateImageSrcForRssLink
from flask.json import jsonify
from app.models.model import Category, Comment, RssTable, TwitterTable
from flask_restplus import Resource, fields, reqparse
from flask import request
from app import api, api_model_factory
from app.models.response import Response
from flask_cors import cross_origin
from pprint import pprint
trendNS = api.namespace('trends', description='API Comment for social media')

trendModel = trendNS.model('trend', {
    'emotion': fields.List(fields.Raw),
    'sentiment': fields.List(fields.Raw),
    'emotion_count': fields.Raw,
    'sentiment_count': fields.Raw
})

trendListResponse = trendNS.model('trendList', {
    'code': fields.String(required=True),
    'message': fields.String(required=True),
    'data': fields.Nested(trendModel)
})


@trendNS.route('')
class Trends(Resource):
    # @cross_origin()
    @trendNS.marshal_with(trendListResponse)
    @trendNS.doc(
        params={
            'periodType': 'History, Current or Prediction',
        }
    )
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('periodType', type=str, required=True, help='period type is required.')
        args = parser.parse_args()

        data = getEmotionAndSentimentDataByPeriodType(args.get('periodType'))
        return Response('200', 'Success', data)




@trendNS.route('/filterDate')
class TrendsFilterDate(Resource):
    # @cross_origin()
    @trendNS.marshal_with(trendListResponse)
    @trendNS.doc(
        params={
            'startDate': 'from Date',
            'endDate': 'to Date'
        }
    )
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('startDate', type=int, required=True, help='start is required.')
        parser.add_argument('endDate', type=int, required=True, help='end is required.')
        args = parser.parse_args()

        data = getEmotionAndSentimentDataByFilterDate(args.get('startDate'), args.get('endDate'))
        print('data.emotion_count---------------- ', data.emotion_count)
        return Response('200', 'Success', data)

