from marshmallow.utils import pprint
from app.representations.representation_schema import CommentRepresentation, CommentSchema, PostRepresentation, PostSchema
from app.services.commonServices import generateTopicsByField, getAllCommentContainKeywords
from app.services.commentsServices import getCommentDataByPeriodTime, getCommentDataByPeriodType
from app.services.rssServices import getRssDataByTime, updateImageSrcForRssLink
from flask.json import jsonify
from app.models.model import Category, Comment, Post, RssTable, TwitterTable
from flask_restplus import Resource, fields, reqparse
from flask import request
from app import api, api_model_factory
from app.models.response import Response
from flask_cors import cross_origin

searchNS = api.namespace('search', description='API Comment for social media')
commentModel = api_model_factory.get_entity(Comment.__tablename__)
postModel = api_model_factory.get_entity(Post.__tablename__)

postSearchModel = searchNS.model('post_search_keyword', {
    'table': fields.String(required=True),
    'field': fields.String(required=True),
    'statement': fields.String(required=True),
    'keywords': fields.String(required=True),
    'startDate': fields.Integer(required=True),
    'endDate': fields.Integer(required=True)
})

searchModel = searchNS.model('dataSearch', {
    'series': fields.List(fields.Raw),
    'topic_labeling': fields.List(fields.Raw),
})

searchResponse = searchNS.model('search', {
    'code': fields.String(required=True),
    'message': fields.String(required=True),
    'data': fields.Nested(searchModel)
})

searchBytopicModel = searchNS.model('searchBytopicModel', {
    'list_data': fields.List(fields.Raw),
})


searchByKeywordCommentResponse = searchNS.model('searchCommentByKeyword', {
    'code': fields.String(required=True),
    'message': fields.String(required=True),
    'data': fields.List(fields.Raw)
})

# searchByKeywordPostResponse = searchNS.model('searchPostByKeyword', {
#     'code': fields.String(required=True),
#     'message': fields.String(required=True),
#     'data': fields.List(fields.Nested(postModel))
# })




@searchNS.route('')
class Search(Resource):
    @searchNS.marshal_with(searchResponse)
    @searchNS.doc(
        params={
            'table': 'Table Name',
            'field': 'Field Name',
            'statement': 'SQL statement condition',
            'startDate': 'from Date',
            'endDate': 'to Date'
        }
    )
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('table', type=str, required=True, help='table name is required.')
        parser.add_argument('field', type=str, required=True, help='field name is required.')
        parser.add_argument('statement', type=str, required=True, help='statement is required.')
        parser.add_argument('startDate', type=str, required=True, help='startDate is required.')
        parser.add_argument('endDate', type=str, required=True, help='endDate is required.')
        args = parser.parse_args()

        data = generateTopicsByField(args.get('table'), args.get('field'), args.get('statement'), args.get('startDate'), args.get('endDate'))
        return Response('200', 'Success', data)


    # @searchNS.expect(postSearchModel)
    # @searchNS.marshal_with(searchByKeywordResponse)
    # def post(self): 
    #     body = request.json
    #     data = getAllCommentContainKeywords(body['table'], body['field'], body['statement'], body['keywords'], body['startDate'], body['endDate'])
    #     return Response('200', 'Success', data)


@searchNS.route('/keyword')
class SearchKeyword(Resource):
    # @searchNS.marshal_with(searchByKeywordPostResponse, code=201, description="Post")
    @searchNS.marshal_with(searchByKeywordCommentResponse)
    @searchNS.doc(
        params={
            'table': 'Table Name',
            'field': 'Field Name',
            'statement': 'SQL statement condition',
            'keyword': 'keyword',
            'startDate': 'from Date',
            'endDate': 'to Date'
        }
    )
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('table', type=str, required=True, help='table name is required.')
        parser.add_argument('field', type=str, required=True, help='field name is required.')
        parser.add_argument('statement', type=str, required=True, help='statement is required.')
        parser.add_argument('keyword', type=str, required=True, help='keyword is required.')
        parser.add_argument('startDate', type=str, required=True, help='startDate is required.')
        parser.add_argument('endDate', type=str, required=True, help='endDate is required.')
        args = parser.parse_args()

        data = getAllCommentContainKeywords(args.get('table'), args.get('field'), args.get('statement'), args.get('keyword'), args.get('startDate'), args.get('endDate'))
        if args.get('table') == 'Comment':
            schema = CommentRepresentation(many=True)
            # schema = CommentSchema()
        else:
            schema = PostRepresentation(many=True)
            # schema = PostSchema()
            # data = schema.dump(data)
        # data = schema.dump(data, many=True)
        data = schema.dump(data)
        return Response('201', 'Success', data)