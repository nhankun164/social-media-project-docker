from app.services.rssServices import getRssDataByTime, updateImageSrcForRssLink
from flask.json import jsonify
from app.models.model import Category, RssTable, TwitterTable
from flask_restplus import Resource, fields
from flask import request
from app import api, api_model_factory
from app.models.response import Response
from flask_cors import cross_origin

rssNS = api.namespace('rss', description='API RSS for social media')
rssModel = api_model_factory.get_entity(RssTable.__tablename__)


rssResponse = rssNS.model('rss', {
        'code': fields.String(required=True),
        'message': fields.String(required=True),
        'data': fields.Nested(rssModel)
})

rssListResponse = rssNS.model('twitterList', {
        'code': fields.String(required=True),
        'message': fields.String(required=True),
        'data': fields.List(fields.Nested(rssModel))
})


@rssNS.route('/<timestamp>', endpoint='/')
class Rss(Resource):
    @cross_origin()
    @rssNS.marshal_with(rssListResponse)
    def get(self, timestamp):
        data = getRssDataByTime(timestamp)
        return Response('200', 'Success', data)

    # @rssNS.expect(twitterModel)
    # @rssNS.marshal_with(twitterResponse)
    # def post(self):
    #     body = request.json
    #     newTwitter = createNewTwitter(body)
    #     return Response('200', 'Success', newTwitter)

@rssNS.route('/')
class RssList(Resource):
    @cross_origin()
    @rssNS.marshal_with(rssListResponse)
    # find metadata from RSS source link and update thumbnail into every record of Rss table
    def put(self):
        updateImageSrcForRssLink()
        return Response('200', 'Success', [])


