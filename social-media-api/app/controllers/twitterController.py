from flask.json import jsonify
from app.services.twitterServices import createNewTwitter, getTwiiterDataByPeriodType, getTwitterDataByPeriodTime
from app.models.model import TwitterTable
from flask_restplus import Resource, fields, reqparse
from flask import request
from app import api, api_model_factory
from app.models.response import Response
from flask_cors import cross_origin


# create Namespace
twitterNS = api.namespace('twitter', description='API Twitter for social media')
twitterModel = api_model_factory.get_entity(TwitterTable.__tablename__)


twitterResponse = twitterNS.model('twitter', {
        'code': fields.String(required=True),
        'message': fields.String(required=True),
        'data': fields.Nested(twitterModel)
})

twitterListResponse = twitterNS.model('twitterList', {
        'code': fields.String(required=True),
        'message': fields.String(required=True),
        'data': fields.List(fields.Nested(twitterModel))
})


# @twitterNS.route('/')
# class Twitter(Resource):
#     @twitterNS.marshal_with(twitterListResponse)
#     def get(self):
#         data = getAllTwitterData()
#         return Response('200', 'Success', data)

#     @twitterNS.expect(twitterModel)
#     @twitterNS.marshal_with(twitterResponse)
    
#     def post(self):
#         body = request.json
#         newTwitter = createNewTwitter(body)
#         return Response('200', 'Success', newTwitter)


@twitterNS.route('')
class Twitter(Resource):
    @twitterNS.marshal_with(twitterListResponse)
    @twitterNS.doc(
        params={
            'periodType': 'History, Current or Prediction',
        }
    )
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('periodType', type=str, required=True, help='period type is required.')
        args = parser.parse_args()

        data = getTwiiterDataByPeriodType(args.get('periodType'))
        return Response('200', 'Success', data)


@twitterNS.route('/filterDate')
class TwitterFilterDate(Resource):
    @twitterNS.marshal_with(twitterListResponse)
    @twitterNS.doc(
        params={
            'startDate': 'from Date',
            'endDate': 'to Date'
        }
    )
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('startDate', type=int, required=True, help='start is required.')
        parser.add_argument('endDate', type=int, required=True, help='end is required.')
        args = parser.parse_args()
        print('start----------- ', args.get('startDate'))
        print('end----------- ', args.get('endDate'))

        data = getTwitterDataByPeriodTime(args.get('startDate'), args.get('endDate'))
        return Response('200', 'Success', data)



