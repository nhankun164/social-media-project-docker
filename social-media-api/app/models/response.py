from flask_restplus import Namespace, fields
from typing import TypeVar, Generic

T = TypeVar('T')

class Response(Generic[T]):
    def __init__(self, code: str, message: str, data: T) -> None:
        self.code = code
        self.message = message
        self.data = data