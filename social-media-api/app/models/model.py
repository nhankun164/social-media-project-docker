# coding: utf-8
from sqlalchemy import Column, DECIMAL, DateTime, Enum, ForeignKey, Index, JSON, String, Table, Text, text
from sqlalchemy.dialects.mysql import BIGINT, DECIMAL, INTEGER, MEDIUMINT, SMALLINT, TINYINT
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from app import db
Base = declarative_base()
metadata = Base.metadata


class Category(db.Model):
    __tablename__ = 'categories'

    id = db.Column(INTEGER(11), primary_key=True, unique=True)
    category_name = db.Column(String(45), nullable=False, unique=True)


class Chart(db.Model):
    __tablename__ = 'chart'

    id = db.Column(INTEGER(10), primary_key=True)
    type = db.Column(String(45), nullable=False, server_default=text("'-'"))
    name = db.Column(String(100), nullable=False, server_default=text("'-'"))
    status = db.Column(TINYINT(2), nullable=False)
    chart_id = db.Column(INTEGER(100), nullable=False)
    display_order = db.Column(INTEGER(10), nullable=False)


class ChartsStatistic(db.Model):
    __tablename__ = 'charts_statistics'
    __table_args__ = (
        Index('topic_id_year_month_day_hour_source_index', 'topic_id', 'year', 'month', 'day', 'hour', 'source'),
    )

    id = db.Column(INTEGER(10), primary_key=True)
    source = db.Column(String(45), nullable=False)
    topic_id = db.Column(INTEGER(10), nullable=False)
    relevance = db.Column(TINYINT(3), nullable=False)
    year = db.Column(SMALLINT(5), nullable=False)
    month = db.Column(TINYINT(3), nullable=False)
    day = db.Column(TINYINT(3), nullable=False)
    hour = db.Column(TINYINT(3), nullable=False)
    total_positive_mentions_count = db.Column(INTEGER(10), nullable=False)
    total_very_positive_mentions_count = db.Column(INTEGER(10), nullable=False)
    total_negative_mentions_count = db.Column(INTEGER(10), nullable=False)
    total_very_negative_mentions_count = db.Column(INTEGER(10), nullable=False)
    total_positive_very_positive_mentions_count = db.Column(INTEGER(10), nullable=False)
    total_negative_very_negative_mentions_count = db.Column(INTEGER(10), nullable=False)
    total_neutral_mentions_count = db.Column(INTEGER(10), nullable=False)
    total_joy_emotion_count = db.Column(INTEGER(10), nullable=False)
    total_fear_emotion_count = db.Column(INTEGER(10), nullable=False)
    total_sadness_emotion_count = db.Column(INTEGER(10), nullable=False)
    total_anger_emotion_count = db.Column(INTEGER(10), nullable=False)
    total_joy_score = db.Column(DECIMAL(10, 3), nullable=False)
    total_fear_score = db.Column(DECIMAL(10, 3), nullable=False)
    total_sadness_score = db.Column(DECIMAL(10, 3), nullable=False)
    total_anger_score = db.Column(DECIMAL(10, 3), nullable=False)
    total_valence_count = db.Column(INTEGER(10), nullable=False)
    total_valence_score = db.Column(DECIMAL(10, 3), nullable=False)
    total_mentions_count = db.Column(INTEGER(10), nullable=False)


class Comment(db.Model):
    __tablename__ = 'comments'
    __table_args__ = (
        Index('comments_conditions_index', 'topic_id', 'source', 'relevance', 'create_date_unix', 'emotion_category', 'sentiment_category', 'valence'),
    )

    id = db.Column(INTEGER(10), primary_key=True)
    post_id = db.Column(String(50), nullable=False, index=True)
    create_date_unix = db.Column(INTEGER(10), nullable=False)
    topic_id = db.Column(INTEGER(10), nullable=False)
    relevance = db.Column(TINYINT(2), nullable=False)
    content = db.Column(Text)
    source = db.Column(String(45), nullable=False)
    source_link = db.Column(String(500), server_default=text("'-'"))
    ne_data = db.Column(String(600))
    senti_type = db.Column(TINYINT(1), server_default=text("'0'"))
    senti_flag = db.Column(TINYINT(1), server_default=text("'0'"))
    senti_val = db.Column(DECIMAL(6, 4), server_default=text("'0.0000'"))
    pos_words = db.Column(String(150))
    neg_words = db.Column(String(150))
    senti_word_flag = db.Column(TINYINT(4))
    hash_tags = db.Column(String(2000))
    refined_text = db.Column(Text)
    is_refined = db.Column(TINYINT(1), server_default=text("'0'"))
    anger = db.Column(TINYINT(4), server_default=text("'0'"))
    anxiety = db.Column(TINYINT(4), server_default=text("'0'"))
    sadness = db.Column(TINYINT(4), server_default=text("'0'"))
    satisfaction = db.Column(TINYINT(4), server_default=text("'0'"))
    happiness = db.Column(TINYINT(4), server_default=text("'0'"))
    excitement = db.Column(TINYINT(4), server_default=text("'0'"))
    sarcasm_score = db.Column(DECIMAL(6, 4), server_default=text("'-1.0000'"))
    sarcasm_flag = db.Column(TINYINT(1), server_default=text("'0'"))
    crystalfeel_out = db.Column(String(128))
    crystalfeel_flag = db.Column(TINYINT(1), server_default=text("'0'"))
    anger_score = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    fear_score = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    joy_score = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    sadness_score = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    valence = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    emotion_score = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    sentiment_score = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    emotion_category = db.Column(String(45), server_default=text("'-'"))
    sentiment_category = db.Column(String(45), server_default=text("'-'"))
    tweet_id = db.Column(String(125), server_default=text("'-'"))


class FbPostTable(db.Model):
    __tablename__ = 'fb_post_table'

    id = db.Column(INTEGER(10), primary_key=True)
    article_id = db.Column(INTEGER(11), server_default=text("'0'"))
    page_id = db.Column(BIGINT(20), nullable=False, index=True)
    post_id = db.Column(String(50), nullable=False, index=True)
    post_message = db.Column(Text, nullable=False)
    post_caption = db.Column(Text, nullable=False)
    post_description = db.Column(Text, nullable=False)
    post_img = db.Column(String(1000))
    post_object_id = db.Column(BIGINT(20), server_default=text("'0'"))
    post_timestamp = db.Column(INTEGER(10), server_default=text("'0'"))
    post_creation_time = db.Column(INTEGER(10), server_default=text("'0'"))
    post_link = db.Column(String(500), server_default=text("'-'"))
    status_type = db.Column(String(24), server_default=text("'-'"))
    post_type = db.Column(String(24), server_default=text("'-'"))
    like_count = db.Column(INTEGER(10), server_default=text("'0'"))
    comment_count = db.Column(INTEGER(10), server_default=text("'0'"))
    share_count = db.Column(INTEGER(10), server_default=text("'0'"))
    pflag = db.Column(TINYINT(3), server_default=text("'0'"))
    when_last_crawled = db.Column(INTEGER(11))
    unshimmed_url = db.Column(String(1000))
    processed_state = db.Column(Enum('no', 'partial', 'yes'), nullable=False, server_default=text("'no'"))


class FbPostTableWithSentiment(db.Model):
    __tablename__ = 'fb_post_table_with_sentiments'
    __table_args__ = (
        Index('unique_index', 'post_id', 'page_id', unique=True),
    )

    id = db.Column(BIGINT(20), primary_key=True)
    page_id = db.Column(BIGINT(20), nullable=False, index=True)
    post_id = db.Column(String(50), nullable=False, index=True)
    highest_freq_emotion_count = db.Column(INTEGER(11), server_default=text("'0'"))
    highest_freq_emotion_type = db.Column(String(45), server_default=text("'-'"))
    highest_freq_positive_sentiment_count = db.Column(INTEGER(11), server_default=text("'0'"))
    highest_freq_negative_sentiment_count = db.Column(INTEGER(11), server_default=text("'0'"))


class FbUserDataTable(db.Model):
    __tablename__ = 'fb_user_data_table'

    id = db.Column(INTEGER(10), primary_key=True)
    page_id = db.Column(BIGINT(20), nullable=False, index=True)
    post_id = db.Column(String(50), nullable=False, index=True)
    user_id = db.Column(BIGINT(20), nullable=False, index=True)
    user_name = db.Column(String(48), server_default=text("'-'"))
    entry_id = db.Column(BIGINT(20), index=True, server_default=text("'0'"))
    entry_text = db.Column(Text)
    entry_timestamp = db.Column(INTEGER(10), index=True, server_default=text("'0'"))
    attachment = db.Column(String(500), server_default=text("'-'"))
    creation_time = db.Column(INTEGER(10), server_default=text("'0'"))
    ne_data = db.Column(String(600), server_default=text("'-'"))
    senti_type = db.Column(TINYINT(1), server_default=text("'0'"))
    senti_flag = db.Column(TINYINT(1), server_default=text("'0'"))
    senti_val = db.Column(DECIMAL(6, 4), server_default=text("'0.0000'"))
    pos_words = db.Column(String(150), server_default=text("'-'"))
    neg_words = db.Column(String(150), server_default=text("'-'"))
    senti_word_flag = db.Column(TINYINT(4), server_default=text("'0'"))
    hash_tags = db.Column(String(300), server_default=text("'-'"))
    refined_text = db.Column(Text)
    is_refined = db.Column(TINYINT(1), server_default=text("'0'"))
    anger = db.Column(TINYINT(4), server_default=text("'0'"))
    anxiety = db.Column(TINYINT(4), server_default=text("'0'"))
    sadness = db.Column(TINYINT(4), server_default=text("'0'"))
    satisfaction = db.Column(TINYINT(4), server_default=text("'0'"))
    happiness = db.Column(TINYINT(4), server_default=text("'0'"))
    excitement = db.Column(TINYINT(4), server_default=text("'0'"))
    sarcasm_score = db.Column(DECIMAL(6, 4), server_default=text("'-1.0000'"))
    sarcasm_flag = db.Column(TINYINT(1), server_default=text("'0'"))
    crystalfeel_out = db.Column(String(128), server_default=text("'-'"))
    crystalfeel_flag = db.Column(TINYINT(1), server_default=text("'0'"))
    comment_link = db.Column(String(500))
    processed = db.Column(TINYINT(1), nullable=False, server_default=text("'0'"))


class Feedarticle(db.Model):
    __tablename__ = 'feedarticles'

    id = db.Column(INTEGER(11), primary_key=True)
    title = db.Column(String(300), server_default=text("'-'"))
    pubdate = db.Column(INTEGER(10), server_default=text("'0'"))
    url = db.Column(String(500), unique=True, server_default=text("'-'"))
    description = db.Column(String(1000), server_default=text("'-'"))
    keywords = db.Column(String(300), server_default=text("'-'"))
    category = db.Column(String(64), server_default=text("'-'"))
    source = db.Column(String(64), server_default=text("'-'"))
    rsssource = db.Column(String(200), server_default=text("'-'"))
    shareimg = db.Column(String(500), server_default=text("'-'"))
    crime_flag = db.Column(TINYINT(4), server_default=text("'-1'"))

class News(db.Model):
    __tablename__ = 'news'

    id = db.Column(INTEGER(255), primary_key=True)
    Title = db.Column(Text)
    Source_Link = db.Column(Text)
    Time_of_the_post = db.Column(Text)
    Source_Name = db.Column(Text)
# t_news = Table(
#     'news', metadata,
#     db.Column('Title', Text),
#     db.Column('Source_Link', Text),
#     db.Column('Time_of_the_post', Text),
#     db.Column('Source_Name', Text)
# )


class Post(db.Model):
    __tablename__ = 'posts'
    __table_args__ = (
        Index('posts_conditions_index', 'topic_id', 'source', 'relevance', 'create_date_unix', 'highest_freq_emotion_type', 'highest_freq_positive_sentiment_count', 'highest_freq_negative_sentiment_count'),
    )

    id = db.Column(INTEGER(10), primary_key=True)
    description = db.Column(Text, nullable=False)
    topic_id = db.Column(INTEGER(11), nullable=False)
    relevance = db.Column(TINYINT(2), nullable=False, server_default=text("'1'"))
    create_date_unix = db.Column(INTEGER(10), nullable=False)
    title = db.Column(Text, nullable=False)
    source = db.Column(String(45), nullable=False)
    source_link = db.Column(String(500), server_default=text("'-'"))
    post_id = db.Column(String(50), nullable=False, index=True)
    highest_freq_emotion_count = db.Column(INTEGER(11), server_default=text("'0'"))
    highest_freq_emotion_type = db.Column(String(45), server_default=text("'-'"))
    highest_freq_positive_sentiment_count = db.Column(INTEGER(11), server_default=text("'0'"))
    highest_freq_negative_sentiment_count = db.Column(INTEGER(11), server_default=text("'0'"))
    processed = db.Column(TINYINT(2), nullable=False, server_default=text("'1'"))
    processed_state = db.Column(Enum('partial', 'yes'), nullable=False, server_default=text("'partial'"))


class RedditCommentTable(db.Model):
    __tablename__ = 'reddit_comment_table'
    __table_args__ = (
        Index('unique_record', 'topic_id', 'post_id', 'comment_id', unique=True),
    )

    id = db.Column(INTEGER(10), primary_key=True, unique=True)
    date = db.Column(DateTime, nullable=False)
    comment = db.Column(Text)
    score = db.Column(INTEGER(11), nullable=False)
    is_submitter = db.Column(TINYINT(2), nullable=False)
    author = db.Column(String(50))
    comment_id = db.Column(String(50), nullable=False)
    link_id = db.Column(String(100))
    parent_link = db.Column(String(100))
    date_unix = db.Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    post_id = db.Column(String(50), nullable=False)
    permalink = db.Column(String(500), server_default=text("'-'"))
    url = db.Column(String(1000), server_default=text("'-'"))
    topic_id = db.Column(INTEGER(11), nullable=False)
    anger_score = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    fear_score = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    joy_score = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    sadness_score = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    valence = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    emotion_score = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    sentiment_score = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    emotion_category = db.Column(String(45), server_default=text("'-'"))
    sentiment_category = db.Column(String(45), server_default=text("'-'"))
    relevance = db.Column(TINYINT(2), nullable=False, server_default=text("'1'"))
    source = db.Column(String(45), nullable=False, server_default=text("'reddit'"))


class RedditPostTable(db.Model):
    __tablename__ = 'reddit_post_table'
    __table_args__ = (
        Index('unique_record', 'submission_id', 'keyword_id', unique=True),
    )

    id = db.Column(INTEGER(10), primary_key=True, unique=True)
    author = db.Column(String(50), nullable=False)
    date = db.Column(DateTime, nullable=False)
    content = db.Column(Text)
    score = db.Column(INTEGER(11), nullable=False)
    title = db.Column(Text, nullable=False)
    over_18 = db.Column(TINYINT(2), nullable=False)
    submission_id = db.Column(String(50), nullable=False)
    permalink = db.Column(Text)
    upvote_ratio = db.Column(DECIMAL(6, 3), server_default=text("'0.000'"))
    url = db.Column(String(1000), server_default=text("'-'"))
    date_unix = db.Column(INTEGER(10), nullable=False, server_default=text("'0'"))
    caption = db.Column(Text)
    keyword_id = db.Column(INTEGER(10), nullable=False)
    relevance = db.Column(TINYINT(2), nullable=False, server_default=text("'1'"))
    source = db.Column(String(45), nullable=False, server_default=text("'reddit'"))
    highest_freq_emotion_count = db.Column(INTEGER(11), server_default=text("'0'"))
    highest_freq_emotion_type = db.Column(String(45), server_default=text("'-'"))
    highest_freq_positive_sentiment_count = db.Column(INTEGER(11), server_default=text("'0'"))
    highest_freq_negative_sentiment_count = db.Column(INTEGER(11), server_default=text("'0'"))
    processed = db.Column(Enum('no', 'partial', 'yes'), nullable=False, server_default=text("'no'"))
    when_last_crawled = db.Column(INTEGER(10), nullable=False)


class RssTable(db.Model):
    __tablename__ = 'rss_table'

    id = db.Column(INTEGER(10), primary_key=True, unique=True)
    title = db.Column(Text)
    source_link = db.Column(String(500), server_default=text("'-'"))
    date_created = db.Column(DateTime, nullable=False)
    processed = db.Column(TINYINT(4), nullable=False, server_default=text("'0'"))
    topic_id = db.Column(INTEGER(10), nullable=False, server_default=text("'405'"))
    date_create_unix = db.Column(INTEGER(10), nullable=False)
    image_src = db.Column(String(500), nullable=True)


class Source(db.Model):
    __tablename__ = 'sources'

    id = db.Column(INTEGER(10), primary_key=True, unique=True)
    source_name = db.Column(String(100), nullable=False)
    source_type = db.Column(String(100))
    selected = db.Column(TINYINT(2), nullable=False, server_default=text("'0'"))


class StoreAllRssTable(db.Model):
    __tablename__ = 'store_all_rss_table'

    id = db.Column(INTEGER(10), primary_key=True, unique=True)
    title = db.Column(Text)
    source_link = db.Column(String(500), server_default=text("'-'"))
    date_created = db.Column(DateTime, nullable=False)
    processed = db.Column(TINYINT(4), nullable=False, server_default=text("'0'"))
    topic_id = db.Column(INTEGER(10), nullable=False, server_default=text("'405'"))
    date_create_unix = db.Column(INTEGER(10), nullable=False)


class Topic(db.Model):
    __tablename__ = 'topics'

    id = db.Column(INTEGER(11), primary_key=True, unique=True)
    description = db.Column(String(300), server_default=text("''"))
    exclude_keywords = db.Column(String(300))
    last_modified = db.Column(DateTime)
    status = db.Column(TINYINT(2), nullable=False, server_default=text("'0'"), comment='1 represents true, 0 otherwise')
    keywords = db.Column(String(300), nullable=False, server_default=text("''"))
    page_id = db.Column(INTEGER(11), nullable=False, server_default=text("'1'"))
    topic_name = db.Column(String(100), nullable=False, unique=True, server_default=text("''"))
    processed_before = db.Column(TINYINT(2), nullable=False, server_default=text("'0'"))
    parent_topic_id = db.Column(INTEGER(11))
    keywords_for_crawling_parent_topic = db.Column(String(300))
    earliest_crawl_date_in_utc = db.Column(DateTime)
    topic_processed_state = db.Column(Enum('new_topic_needs_processing', 'updated_topic_needs_processing', 'ready'), nullable=False, server_default=text("'new_topic_needs_processing'"))
    when_topic_processed_state_is_ready = db.Column(DateTime)
    is_editable = db.Column(TINYINT(4), nullable=False, server_default=text("'1'"))
    keywords_json = db.Column(JSON)
    topic_ready_notification_email_address = db.Column(String(500))


class TwitterPostTable(db.Model):
    __tablename__ = 'twitter_post_table'

    id = db.Column(INTEGER(10), primary_key=True)
    user_id = db.Column(String(24), nullable=False)
    user_timestamp = db.Column(INTEGER(10), server_default=text("'0'"))
    user_follower = db.Column(MEDIUMINT(8), server_default=text("'0'"))
    user_friend = db.Column(MEDIUMINT(8), server_default=text("'0'"))
    user_status_count = db.Column(MEDIUMINT(8), server_default=text("'0'"))
    tweet_id = db.Column(String(24), nullable=False)
    tweet_text = db.Column(String(150))
    tweet_img = db.Column(String(125))
    object_id = db.Column(String(24), server_default=text("'-'"))
    tweet_timestamp = db.Column(INTEGER(10), server_default=text("'0'"))
    retweet_count = db.Column(MEDIUMINT(8), server_default=text("'0'"))
    favourite_count = db.Column(MEDIUMINT(8), server_default=text("'0'"))
    hash_tags = db.Column(String(120), server_default=text("'-'"))
    is_retweet = db.Column(TINYINT(1), server_default=text("'0'"))
    location = db.Column(String(48), server_default=text("'-'"))
    search_word = db.Column(String(32), server_default=text("'-'"))
    anger_score = db.Column(DECIMAL(6, 4), server_default=text("'0.0000'"))
    fear_score = db.Column(DECIMAL(6, 4), server_default=text("'0.0000'"))
    sadness_score = db.Column(DECIMAL(6, 4), server_default=text("'0.0000'"))
    joy_score = db.Column(DECIMAL(6, 4), server_default=text("'0.0000'"))
    valence_score = db.Column(DECIMAL(6, 4), server_default=text("'0.0000'"))
    is_processed = db.Column(TINYINT(1), server_default=text("'0'"))
    country = db.Column(String(32))
    topic_id = db.Column(INTEGER(11), nullable=False, server_default=text("'405'"))


class TwitterTable(db.Model):
    __tablename__ = 'twitter_table'

    id = db.Column(INTEGER(10), primary_key=True)
    tweet_id = db.Column(String(125), nullable=False, index=True)
    create_date = db.Column(DateTime, nullable=False)
    create_date_unix = db.Column(INTEGER(20), nullable=False)
    url = db.Column(Text, nullable=False)
    tweet_type = db.Column(String(24), nullable=False)
    tweet_text = db.Column(Text)
    tweet_favorite_count = db.Column(INTEGER(20), server_default=text("'0'"))
    user_follower_count = db.Column(INTEGER(20), server_default=text("'0'"))
    quoted_count = db.Column(INTEGER(20), server_default=text("'0'"))
    topic_id = db.Column(INTEGER(10), nullable=False)
    processed = db.Column(TINYINT(4), nullable=False, server_default=text("'0'"))
    country = db.Column(String(125), server_default=text("'-'"))
    locality = db.Column(String(125), server_default=text("'-'"))
    tweet_img = db.Column(Text)
    hash_tags = db.Column(String(1000))
    source = db.Column(String(24), nullable=False, server_default=text("'twitter'"))
    relevance = db.Column(TINYINT(4), nullable=False, server_default=text("'1'"))
    retweet_count = db.Column(INTEGER(20), server_default=text("'0'"))
    user_id = db.Column(String(125), nullable=False)
    user_following_count = db.Column(INTEGER(20), server_default=text("'0'"))
    region = db.Column(String(1000), server_default=text("'-'"))
    sub_region = db.Column(String(1000), server_default=text("'-'"))
    geo_full_name = db.Column(String(1000), server_default=text("'-'"))
    latitude = db.Column(String(125), server_default=text("'-'"))
    longitude = db.Column(String(125), server_default=text("'-'"))


class CategoryTopicLink(db.Model):
    __tablename__ = 'category_topic_link'

    id = db.Column(INTEGER(11), primary_key=True, unique=True)
    category_id = db.Column(ForeignKey('categories.id'), index=True)
    topic_id = db.Column(ForeignKey('topics.id'), index=True)

    category = relationship('Category')
    topic = relationship('Topic')


class RssSourceLink(db.Model):
    __tablename__ = 'rss_source_link'

    id = db.Column(INTEGER(10), primary_key=True, unique=True)
    source_id = db.Column(ForeignKey('sources.id'), index=True)
    rss_id = db.Column(ForeignKey('rss_table.id'), index=True)
    topic_id = db.Column(INTEGER(10))

    rss = relationship('RssTable')
    source = relationship('Source')
