from flask.globals import session
from sqlalchemy.dialects.mysql.types import NUMERIC
from app.services.commonServices import add_and_save_changes, commit_changes
from app.models.model import RssTable, TwitterTable
from datetime import datetime
from sqlalchemy import cast, Date
import metadata_parser

def getRssDataByTime(timestamp):
    print(timestamp)
    date = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d')
    # return RssTable.query.filter(datetime.fromtimestamp(RssTable.date_create_unix).strftime('%Y-%m-%d') == date).all()
    return RssTable.query.filter(cast(RssTable.date_created, Date) == date).all()


def updateImageSrcForRssLink():
    data = RssTable.query.all()
    print('data------ ', data[0].source_link)
    headers = { 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    for i in range(0, len(data)):
        page = metadata_parser.MetadataParser(
            url=str(data[i].source_link), 
            url_headers=headers,
            search_head_only=False,
            force_doctype=True,
            only_parse_http_ok=False,
            support_malformed=True,
        )
        img_src = page.get_metadata_link('image', allow_encoded_uri=True)
        print('img_src', img_src)
        data[i].image_src = img_src
    commit_changes()

