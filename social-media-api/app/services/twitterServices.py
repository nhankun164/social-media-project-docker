from app.utils.default_db_session import DefaultDBSession
from app.services.commonServices import add_and_save_changes
from app.models.model import TwitterTable
from sqlalchemy import cast, Date, func
from datetime import datetime
from dateutil.relativedelta import relativedelta

def getTwiiterDataByPeriodType(periodType):
    db_session = DefaultDBSession()
    maxDate = db_session.query(func.max(TwitterTable.create_date_unix)).scalar()
    maxDateString = datetime.fromtimestamp(int(maxDate)).strftime('%Y-%m-%d')
    maxDateString = datetime.strptime(maxDateString, '%Y-%m-%d')
    deltaMonth = 1
    if periodType == 'History':
        deltaMonth = 3
    elif periodType == 'Current':
        deltaMonth = 1
    historyDateString = maxDateString - relativedelta(months=deltaMonth) 
    historyDate = datetime.timestamp(historyDateString)
    return TwitterTable.query.filter(TwitterTable.create_date_unix >= historyDate, TwitterTable.create_date_unix <= int(maxDate)). \
        order_by(TwitterTable.create_date_unix). \
        all()


def getTwitterDataByPeriodTime(startDate, endDate):
    return TwitterTable.query.filter(TwitterTable.create_date_unix >= int(startDate), TwitterTable.create_date_unix <= int(endDate)). \
        order_by(TwitterTable.create_date_unix). \
        all()
    

def createNewTwitter(data):
    newTwitter = TwitterTable(data)
    add_and_save_changes(newTwitter)
    return newTwitter
