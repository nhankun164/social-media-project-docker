from sqlalchemy.sql.sqltypes import JSON
from app.utils.sentiment_trend_chart import SentimentTrendChart
from app.utils.default_db_session import DefaultDBSession, default_db_session_transaction
from app.utils.emotion_trend_chart import EmotionTrendChart
from app.services.commonServices import add_and_save_changes
from app.models.model import Comment
from sqlalchemy import cast, Date, func
from datetime import datetime
from dateutil.relativedelta import relativedelta

def getEmotionAndSentimentDataByFilterDate(startDate, endDate):
    db_session = DefaultDBSession()
    startDateString = datetime.fromtimestamp(int(startDate)).strftime('%Y-%m-%d')
    endDateString = datetime.fromtimestamp(int(endDate)).strftime('%Y-%m-%d')
    emotionTrendData = EmotionTrendChart(fromDate=startDateString, toDate=endDateString, topicId=1, sources='all sources', unit='hour', db_session=db_session)
    sentimentTrendData = SentimentTrendChart(fromDate=startDateString, toDate=endDateString, topicId=1, sources='all sources', unit='hour', db_session=db_session)
    emotionCommentCountByCategory = db_session.query(Comment.emotion_category, func.count(Comment.emotion_category)). \
                                        filter(Comment.create_date_unix >= int(startDate), Comment.create_date_unix <= int(endDate)). \
                                        filter(Comment.emotion_category != '-', Comment.emotion_category != 'no specific emotion'). \
                                        group_by(Comment.emotion_category). \
                                        order_by(Comment.create_date_unix). \
                                        all()
    emotionCommentCountByCategory = dict(list(emotionCommentCountByCategory))

    sentimentCommentCountByCategory = db_session.query(Comment.sentiment_category, func.count(Comment.sentiment_category)). \
                                        filter(Comment.create_date_unix >= int(startDate), Comment.create_date_unix <= int(endDate)). \
                                        filter(Comment.sentiment_category != '-'). \
                                        group_by(Comment.sentiment_category). \
                                        order_by(Comment.create_date_unix). \
                                        all()
    sentimentCommentCountByCategory = dict(sentimentCommentCountByCategory)
    data = {
        'emotion': emotionTrendData.get_chart(),
        'sentiment': sentimentTrendData.get_chart(),
        'emotion_count': emotionCommentCountByCategory,
        'sentiment_count': sentimentCommentCountByCategory
    }
    return data


def getEmotionAndSentimentDataByPeriodType(periodType):
    db_session = DefaultDBSession()
    maxDate = db_session.query(func.max(Comment.create_date_unix)).scalar()
    maxDateString = datetime.fromtimestamp(int(maxDate)).strftime('%Y-%m-%d')
    maxDateString = datetime.strptime(maxDateString, '%Y-%m-%d')
    deltaMonth = 1
    if periodType == 'History':
        deltaMonth = 3
    elif periodType == 'Current':
        deltaMonth = 1
    historyDateString = maxDateString - relativedelta(months=deltaMonth)  
    emotionCommentCountByCategory = db_session.query(Comment.emotion_category, func.count(Comment.emotion_category)). \
                                        filter(Comment.create_date_unix >= int(historyDateString.timestamp()), Comment.create_date_unix <= int(maxDateString.timestamp())). \
                                        filter(Comment.emotion_category != '-', Comment.emotion_category != 'no specific emotion'). \
                                        group_by(Comment.emotion_category). \
                                        order_by(Comment.create_date_unix). \
                                        all()
    emotionCommentCountByCategory = dict(emotionCommentCountByCategory)
    
    sentimentCommentCountByCategory = db_session.query(Comment.sentiment_category, func.count(Comment.sentiment_category)). \
                                        filter(Comment.create_date_unix >= int(historyDateString.timestamp()), Comment.create_date_unix <= int(maxDateString.timestamp())). \
                                        filter(Comment.sentiment_category != '-'). \
                                        group_by(Comment.sentiment_category). \
                                        order_by(Comment.create_date_unix). \
                                        all()
    sentimentCommentCountByCategory = dict(sentimentCommentCountByCategory)
    maxDateString = maxDateString.strftime('%Y-%m-%d')
    historyDateString = historyDateString.strftime('%Y-%m-%d')
    emotionTrendData = EmotionTrendChart(fromDate=historyDateString, toDate=maxDateString, topicId=1, sources='all sources', unit='hour', db_session=db_session)
    sentimentTrendData = SentimentTrendChart(fromDate=historyDateString, toDate=maxDateString, topicId=1, sources='all sources', unit='hour', db_session=db_session)
    

    data = {
        'emotion': emotionTrendData.get_chart(),
        'sentiment': sentimentTrendData.get_chart(),
        'emotion_count': emotionCommentCountByCategory,
        'sentiment_count': sentimentCommentCountByCategory
    }
    return data
    
    
