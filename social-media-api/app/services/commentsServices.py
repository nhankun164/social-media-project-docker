from dateutil.relativedelta import relativedelta
from app.utils.default_db_session import DefaultDBSession, default_db_session_transaction
from app.utils.emotion_trend_chart import EmotionTrendChart
from app.services.commonServices import add_and_save_changes
from app.models.model import Comment
from sqlalchemy import cast, Date, func
from datetime import datetime
import pandas as pd
import os 
def getCommentDataByPeriodType(periodType):
    db_session = DefaultDBSession()
    maxDate = db_session.query(func.max(Comment.create_date_unix)).scalar()
    maxDateString = datetime.fromtimestamp(int(maxDate)).strftime('%Y-%m-%d')
    maxDateString = datetime.strptime(maxDateString, '%Y-%m-%d')
    deltaMonth = 1
    if periodType == 'History':
        deltaMonth = 3
    elif periodType == 'Current':
        deltaMonth = 1
    historyDateString = maxDateString - relativedelta(months=deltaMonth) 
    print('historyDateString--------------- ', historyDateString)  
    historyDate = datetime.timestamp(historyDateString)
    print('maxDate------------------ ', int(maxDate))
    print('historyDate------------------ ', int(historyDate))
    print('111111')
    emotionCommentCountByCategory = db_session.query(Comment.emotion_category, func.count(Comment.emotion_category)). \
                                        filter(Comment.create_date_unix >= int(historyDate), Comment.create_date_unix <= int(maxDate)). \
                                        filter(Comment.emotion_category != '-', Comment.emotion_category != 'no specific emotion'). \
                                        group_by(Comment.emotion_category). \
                                        order_by(Comment.create_date_unix). \
                                        all()
    emotionCommentCountByCategory = dict(list(emotionCommentCountByCategory))

    sentimentCommentCountByCategory = db_session.query(Comment.sentiment_category, func.count(Comment.sentiment_category)). \
                                        filter(Comment.create_date_unix >= int(historyDate), Comment.create_date_unix <= int(maxDate)). \
                                        filter(Comment.sentiment_category != '-'). \
                                        group_by(Comment.sentiment_category). \
                                        order_by(Comment.create_date_unix). \
                                        all()
    sentimentCommentCountByCategory = dict(sentimentCommentCountByCategory)
    comments = Comment.query.filter(Comment.create_date_unix >= historyDate, Comment.create_date_unix <= int(maxDate)). \
                    order_by(Comment.create_date_unix). \
                    all()
    data = {
        'emotion_count': emotionCommentCountByCategory,
        'sentiment_count': sentimentCommentCountByCategory,
        'data': comments
    }
    return data


def getCommentDataByPeriodTime(startDate, endDate):
    db_session = DefaultDBSession()
    emotionCommentCountByCategory = db_session.query(Comment.emotion_category, func.count(Comment.emotion_category)). \
                                        filter(Comment.create_date_unix >= int(startDate), Comment.create_date_unix <= int(endDate)). \
                                        filter(Comment.emotion_category != '-', Comment.emotion_category != 'no specific emotion'). \
                                        group_by(Comment.emotion_category). \
                                        order_by(Comment.create_date_unix). \
                                        all()
    emotionCommentCountByCategory = dict(list(emotionCommentCountByCategory))

    sentimentCommentCountByCategory = db_session.query(Comment.sentiment_category, func.count(Comment.sentiment_category)). \
                                        filter(Comment.create_date_unix >= int(startDate), Comment.create_date_unix <= int(endDate)). \
                                        filter(Comment.sentiment_category != '-'). \
                                        group_by(Comment.sentiment_category). \
                                        order_by(Comment.create_date_unix). \
                                        all()
    sentimentCommentCountByCategory = dict(sentimentCommentCountByCategory)
    comments = Comment.query.filter(Comment.create_date_unix >= int(startDate), Comment.create_date_unix <= int(endDate)). \
                    order_by(Comment.create_date_unix). \
                    all()
    data = {
        'emotion_count': emotionCommentCountByCategory,
        'sentiment_count': sentimentCommentCountByCategory,
        'data': comments
    }
    return data


    
