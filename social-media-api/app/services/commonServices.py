from flask.json import jsonify
from app.utils.topic_modeling_lda_ngram import generateTopicNGram, generateTopicNGram2
import multiprocessing
from app.utils.topic_labeling import generateTopicFromContent, getVisualizeChartFromTopic
from app import db
from app.utils.default_db_session import DefaultDBSession
from ..models import model
import pandas as pd
import os 
from top2vec import Top2Vec
from sqlalchemy import text, and_
import re
import unicodedata
import multiprocessing
from gensim.models.doc2vec import TaggedDocument
from gensim.models import Doc2Vec, Word2Vec
import logging
import nltk
from nltk.util import ngrams
import pickle
from numpy import exp, log, dot, zeros, outer, random, dtype, float32 as REAL,\
    double, uint32, seterr, array, uint8, vstack, fromstring, sqrt, newaxis,\
ndarray, empty, sum as np_sum, prod, ones, ascontiguousarray
from gensim import utils,matutils

def add_and_save_changes(data):
    db.session.add(data)
    db.session.commit()


def commit_changes():
    db.session.commit()



def getAllCommentContainKeywords(model_name, field_name, condition, keyword, startDate, endDate):
    words = keyword.split(" ")
    candidate_searchs = []
    for word in words:
        candidate_condition = field_name + " like " + "'%" + word + "%'"
        candidate_searchs.append(candidate_condition)
    searchString = ' or '.join(candidate_searchs)
    searchString = '(' + searchString + ')'
    print('searchString-------------------- ', searchString)
    db_session = DefaultDBSession()
    print(model_name, '-----', field_name, '-----------', condition, '------', startDate, '-----', endDate, '----------')
    model_table = getattr(model, model_name)
    if condition == '()':
        condition = '(1)'
    field = getattr(model_table, field_name)
    if startDate != 'null' and endDate != 'null':
        data = db_session.query(model_table).filter(model_table.create_date_unix >= int(startDate), model_table.create_date_unix <= int(endDate)). \
            filter(text(condition)). \
            filter(text(searchString)). \
            order_by(model_table.create_date_unix).all()
    else:
        data = db_session.query(model_table).filter(text(condition)).filter(text(searchString)). \
            order_by(model_table.create_date_unix).all()
    data = [e for e in data]
    return data

def generateTopicsByField(model_name, field_name, condition, startDate, endDate):
    db_session = DefaultDBSession()
    print(model_name, '-----', field_name, '-----------', condition, '------', startDate, '-----', endDate, '----------')
    model_table = getattr(model, model_name)
    if condition == '()':
        condition = '(1)'
    field = getattr(model_table, field_name)
    if startDate != 'null' and endDate != 'null':
        data = db_session.query(field).filter(model_table.create_date_unix >= int(startDate), model_table.create_date_unix <= int(endDate)). \
            filter(text(condition)). \
            order_by(model_table.create_date_unix).all()
    else:
        data = db_session.query(field).filter(text(condition)). \
            order_by(model_table.create_date_unix).all()
    data = [e[0] for e in data]

    if len(data) == 0:
        return {
            "series": [],
            "topic_labeling": []
        }
    convertDataListToCSV(model_name, field_name, data)
    num_of_topics = calculateNumOfTopic(data)
    print('num_of_topics-------------- ', num_of_topics)
    fileName = 'logs/' + model_name + '_' + field_name + '.csv'
    results = generateTopicNGram2(data, num_of_topics, fileName)
    return {
        "series": [],
        "topic_labeling": results
    }

    # excel_file_path, csv_topic_file = readFileAndRunLDAScript(model_name, field_name, 15)


    #-------------------- Parameter ---------------------------------
    # data = 'logs/Comment_conten_csv_topics.csv' # The file in csv format which contains the topic terms that needs a label. 
    # #Parameters for candidate Generation of Labels
    # doc2vecmodel = "logs/doc2vec/doc2vecmodel.d2v" # Path for Doc2vec Model.
    # word2vecmodel = "logs/word2vec/word2vec" # Path for Word2vec Model.
    # num_candidates =19 # Number of candidates labels that need to generated for a topic.
    # output_filename = 'logs/' + model_name + '_' + field_name + '_' + "output_candidates" # Name of the output file that will store the candidate labels.
    # doc2vec_indices_file = "logs/doc2vec_indices" # The filtered doc2vec indices file. 
    # word2vec_indices_file = "logs/word2vec_indices" # The filtered word2vec indices file
    
    # os.system("python app/utils/get_topic_labeling_demo.py "+str(num_candidates)+" "+doc2vecmodel+" "+word2vecmodel+" "+data+" "+output_filename +" "+doc2vec_indices_file+" "+word2vec_indices_file)


    # if excel_file_path is not None and os.path.exists(excel_file_path):
    #     dataChart = getVisualizeChartFromTopic(excel_file_path, num_of_topics)
    # return dataChart
#     sentences = []
#     for docs in data:
#         word_list = []
#         found = ""
#         m = re.search(".*", docs, re.DOTALL)    # This gives the document title of Wikipedia
#         if m:
#             found = m.group(0)
#             found = found.lower()
#             found = unicodedata.normalize("NFKD", found) 
#             found = found.replace(" ","_") 
#             found = found.encode('utf-8')
#         else:
#             found = ""   
#         for word in docs.split(" "):
#             word_list.append(word.strip())
#         if found != "":
#             sentence = TaggedDocument(words = word_list, tags = [found])
#             sentences.append(sentence)
#     print('sentences------------- ', sentences)
#     cores = multiprocessing.cpu_count() 
#     doc2vecModel =Doc2Vec(size=300, window=15, min_count=20, sample=1e-5, workers=cores, hs=0, dm=0, negative=5, dbow_words=1, dm_concat=0)
#     doc2vecModel.build_vocab(sentences)

#     print('doc2vecModel.corpus_count---------- ', doc2vecModel.corpus_count)
#     print('doc2vecModel.epochs---------- ', doc2vecModel.epochs)
#     epochs = 20


#     outputDoc2Vec = 'logs/doc2vec/' + model_name + '_' + field_name + '_model.d2v'

#     #Model Training 
#     for epoch in range(epochs):
#         doc2vecModel.train(sentences, total_examples=doc2vecModel.corpus_count, epochs=doc2vecModel.epochs)
#         print("Epoch completed: ", str(epoch+1))
    
#     doc2vecModel.save(outputDoc2Vec)
# #----------------------------------------------------------------------------------------------#


# #-------------------------------------  Word2Vec Phrase -----------------------------------------
#     title_length = 8
#     doc_length = 40
#     classpath = "support_packages/stanford-parser-full-2014-08-27/stanford-parser.jar"
#     output_d2v_phrase_filename = 'logs/word2vec/' + model_name + '_' + field_name + '_' + 'word2vec_phrases_list_tokenized.txt'
#     all_w2v_docs = []

#     for docs in data:
#         word_list = []
#         found = ""
#         m = re.search(".*", docs, re.DOTALL)    # This gives the document title of Wikipedia
#         if m:
#             found = m.group(0)
#             found = unicodedata.normalize("NFKD", found) 
#             if found != "":
#                 # for word in docs.split(" "):
#                 #     word_list.append(word.strip())
#                 # temp_list= found.split(" ")
#                 # found = found.encode('utf-8')
#                 all_w2v_docs.append(found)

#     g =open("temp.txt",'w', encoding='utf-8')
#     set_docs= set(all_w2v_docs)
#     for elem in set_docs:
#         g.write(str(elem) + '\n')
#     g.close()

#     print("------------------------Tokenising-----------------------")

#     # Running query for standford parser
#     query = "java -cp "+ classpath +" edu.stanford.nlp.process.PTBTokenizer -preserveLines --lowerCase <temp.txt> " + output_d2v_phrase_filename
#     os.system(query)

#     # Deleting temporary file
#     os.remove("temp.txt")
# #----------------------------------------------------------------------------------------------


# #------------------------------------ Create N-gram --------------------------------------------
#     output_ngram_filename = 'logs/word2vec/' + model_name + '_' + field_name + '_' + 'ngram.txt'
#     h = open(output_d2v_phrase_filename, encoding='utf-8')
#     list_ngram_labels=[]
#     for line in h:
#         line = line.strip()
#         list_ngram_labels.append(line)
#     list_ngram_labels= set(list_ngram_labels)
#     print("----------------file loaded-----------------")
#     print("Converting to ngram phrases")
#     get_phrases_for_ngram(output_ngram_filename, data, list_ngram_labels)

# #--------------------------------------------------------------------------------------------------

# #------------------------------------ Train Word2Vec --------------------------------------------
#     output_word2vec = 'logs/word2vec/' + model_name + '_' + field_name + '_' + 'word2vec.model'
#     ngram = open(output_d2v_phrase_filename, encoding='utf-8')
#     list_word2vec=[]
#     for line in ngram:
#         line = line.split()
#         list_word2vec.append(line)

#     cores = multiprocessing.cpu_count()
#     word2vecModel = Word2Vec(size = 300, window=5, min_count = 20, workers = cores, sample =0.00001, negative = 5, sg =1)
#     print('list_word2vec-------------- ', list_word2vec)
#     word2vecModel.build_vocab(list_word2vec)

#     # Model Training
#     for epoch in range(50):
#         word2vecModel.train(list_word2vec, total_examples=word2vecModel.corpus_count, epochs=word2vecModel.epochs)
#         print("epoch completed: ", epoch)

#     word2vecModel.save(output_word2vec)

# #--------------------------------------------------------------------------------------------------

# #------------------------------------ Pruned Document --------------------------------------------
#     output_short_label_document = 'logs/' + model_name + '_' + field_name + '_' + 'short_label_documents.pickle'
#     ngram = open(output_d2v_phrase_filename, encoding='utf-8')
#     list_pruned=[]
#     for docs in data:
#         word_pruned_list = []
#         found = ""
#         m = re.search(".*", docs, re.DOTALL)    # This gives the document title of Wikipedia
#         if m:
#             found = m.group(0)
#             found = unicodedata.normalize("NFKD", found) 
#             found = found.replace(" ","_")
#             # found = found.encode('utf-8')
#         else:
#             found = ""   
#         if found != "":
#             # for word in docs.split(" "):
#             #     word_pruned_list.append(word.strip())
#             # temp_list= found.split(" ")

#             # if (len(word_pruned_list) > doc_length) and (len(temp_list) < title_length):
#             list_pruned.append(str(found))
#     print('list_pruned-------------------- ', list_pruned)
#     print("-----------Writng labels to picke file------------")
#     with open(output_short_label_document,'wb') as k:
#         pickle.dump(list_pruned, k)

# #--------------------------------------------------------------------------------------------------

# #------------------------------------ Create Indices w2v + d2v --------------------------------------------
#     # outputDoc2Vec = "model_run/pre_trained_models/doc2vec/docvecmodel.d2v"
#     # output_word2vec = "model_run/pre_trained_models/word2vec/word2vec" # Trained word2vec model
#     #  output_short_label_document = "short_label_documents" # The file created by pruned_documents.py. FIltering short or long title documents.
#     # output_d2v_phrase_filename = "training/additional_files/word2vec_phrases_list_tokenized.txt" #The file created by word2vec_phrases.py Removing brackets from filtered wiki titles.
#     doc2vec_indices_output = 'logs/' + model_name + '_' + field_name + '_' + 'doc2vec_indices.pickle'  # The output file which map pruned doc2vec labels to indcies from doc2vec model.
#     word2vec_indices_output ='logs/' + model_name + '_' + field_name + '_' + 'word2vec_indices.pickle' # the output file that maps short_label_word2vec_tokenised to indices from wrod2vec model.
    
#     d2vModel =Doc2Vec.load(outputDoc2Vec)
#     w2vModel = Word2Vec.load(output_word2vec)
#     print("Models loaded")

#     # Loading the pruned tiles and making a set of it
#     with open(output_short_label_document,"rb") as k:
#         short_doc_labels = pickle.load(k)
#     short_doc_labels = set(short_doc_labels)
#     print("Pruned document titles loaded")

#     # laoding thw phrasses used in training word2vec model. And then replacing space with underscore.
#     h = open(output_d2v_phrase_filename,'r', encoding='utf-8')
#     list_labels=[]
#     for line in h:
#         line = line.strip()
#         list_labels.append(line)
#     list_labels= set(list_labels)

#     word2vec_labels=[]
#     for words in list_labels:
#         new = words.split(" ")
#         temp ='_'.join(new)
#         word2vec_labels.append(temp)
#     word2vec_labels = set(word2vec_labels)
#     print("Word2vec model phrases loaded")

#     doc_indices =[]
#     word_indices =[]

#     # finds the coresponding index of the title from doc2vec model
#     for elem in short_doc_labels:
#         try:
#             val = d2vModel.docvecs.doctags[elem].offset
#             doc_indices.append(val)
#         except:
#             pass

#     # Finds the corseponding index from word2vec model
#     for elem in word2vec_labels:
#         try:
#             val = w2vModel.vocab[elem].index
#             word_indices.append(val)
#         except:
#             pass

#     # creating output indices file
#     with open(doc2vec_indices_output,'wb') as m:
#         pickle.dump(doc_indices,m)
#     with open(word2vec_indices_output,'wb') as n:
#         pickle.dump(word_indices,n)

# #--------------------------------------------------------------------------------------------------

# #------------------------------------ Candidate label generation --------------------------------------------
#     # outputDoc2Vec = "model_run/pre_trained_models/doc2vec/docvecmodel.d2v"
#     # output_word2vec = "model_run/pre_trained_models/word2vec/word2vec" # Trained word2vec model
#     #  output_short_label_document = "short_label_documents" # The file created by pruned_documents.py. FIltering short or long title documents.
#     # output_d2v_phrase_filename = "training/additional_files/word2vec_phrases_list_tokenized.txt" #The file created by word2vec_phrases.py Removing brackets from filtered wiki titles.
#     # doc2vec_indices_output = 'logs/' + model_name + '_' + field_name + '_' + 'doc2vec_indices.pickle'  # The output file which map pruned doc2vec labels to indcies from doc2vec model.
#     # word2vec_indices_output ='logs/' + model_name + '_' + field_name + '_' + 'word2vec_indices.pickle' # the output file that maps short_label_word2vec_tokenised to indices from wrod2vec model.
#     output_candidates = 'logs/' + model_name + '_' + field_name + '_' + 'output_candidates.txt'
#     with open(doc2vec_indices_output,'rb') as m:   
#         d_indices=pickle.load(m)
#     with open(word2vec_indices_output,'rb') as n:
#         w_indices =pickle.load(n)

#     # Models loaded
#     d2vModel =Doc2Vec.load(outputDoc2Vec)
#     w2vModel = Word2Vec.load(output_word2vec)
#     print("models loaded")

#     # Loading the data file
#     topics = pd.read_csv(csv_topic_file)
#     try:
#         new_frame= topics.drop('domain',1)
#         topic_list = new_frame.set_index('topic_id').T.to_dict('list')
#     except:
#         topic_list = topics.set_index('topic_id').T.to_dict('list')
#     print("Data Gathered")


#     w_indices = list(set(w_indices))
#     d_indices = list(set(d_indices))

#     # Models normalised in unit vectord from the indices given above in pickle files.
#     d2vModel.syn0norm = (d2vModel.wv.vectors / sqrt((d2vModel.wv.vectors ** 2).sum(-1))[..., newaxis]).astype(REAL)
#     d2vModel.docvecs.vectors_docs_norm =  (d2vModel.docvecs.vectors_docs / sqrt((d2vModel.docvecs.vectors_docs ** 2).sum(-1))[..., newaxis]).astype(REAL)[d_indices]
#     print("doc2vec normalized")

#     w2vModel.syn0norm = (w2vModel.wv.vectors / sqrt((w2vModel.wv.vectors ** 2).sum(-1))[..., newaxis]).astype(REAL)
#     model3 = w2vModel.syn0norm[w_indices]
#     print("word2vec normalized")

#     # This method is mainly used to remove brackets from the candidate labels.



#     cores = multiprocessing.cpu_count()
#     # pool = multiprocessing.Pool(processes=cores)
#     # result = pool.map(get_labels, range(0,len(topic_list)))
#     result=[]
#     for i in range(0,len(topic_list)):
#         result.append(get_labels(i, topic_list, d2vModel, w2vModel, model3, d_indices, w_indices, num_of_topics))

#     # The output file for candidates.
#     g = open(output_candidates,'w', encoding='utf-8')
#     for i,elem in enumerate(result):
#         val = ""
#         for item in elem:
#             val = val + " "+item[0]
#         g.write(val+"\n")
#     g.close()

#     print("Candidate labels written to ", output_candidates)

#--------------------------------------------------------------------------------------------------

def get_labels(topic_num, topic_list, d2vModel, w2vModel, model3, d_indices, w_indices, num_of_topics):
    valdoc2vec =0.0
    valword2vec =0.0
    cnt = 0
    store_indices =[]
    
    print("Processing Topic number ", str(topic_num))
    for item in topic_list[topic_num]:
        try: 
            tempdoc2vec = d2vModel.syn0norm[d2vModel.wv.vocab[item].index] # The word2vec value of topic word from doc2vec trained model
            print('item----------------- ', item)
            print('tempdoc2vec------------------ ', tempdoc2vec)
        except:
            pass
        else:
            meandoc2vec = matutils.unitvec(tempdoc2vec).astype(REAL)    # Getting the unit vector
            distsdoc2vec = dot(d2vModel.docvecs.vectors_docs_norm, meandoc2vec) # The dot product of all labels in doc2vec with the unit vector of topic word
            valdoc2vec = valdoc2vec + distsdoc2vec

        try:
            tempword2vec = w2vModel.syn0norm[w2vModel.wv.vocab[item].index]  # The word2vec value of topic word from word2vec trained model
        except:
            pass
        else:
            meanword2vec = matutils.unitvec(tempword2vec).astype(REAL) # Unit vector 

            distsword2vec = dot(model3, meanword2vec) # The dot prodiuct of all possible labels in word2vec vocab with the unit vector of topic word

            """
            This next section of code checks if the topic word is also a potential label in trained word2vec model. If that is the case, it is 
            important the dot product of label with that topic word is not taken into account.Hence we make that zero and further down the code
            also exclude it in taking average of that label over all topic words. 

            """
            
            if (w2vModel.wv.vocab[item].index) in w_indices:
                i_val = w_indices.index(w2vModel.wv.vocab[item].index)
                store_indices.append(i_val)
                distsword2vec[i_val] =0.0
            valword2vec = valword2vec + distsword2vec
            
    avgdoc2vec = valdoc2vec/float(len(topic_list[topic_num])) # Give the average vector over all topic words
    avgword2vec = valword2vec/float(len(topic_list[topic_num])) # Average of word2vec vector over all topic words

    bestdoc2vec = matutils.argsort(avgdoc2vec, topn = 100, reverse=True) # argsort and get top 100 doc2vec label indices 
    print('bestdoc2vec----------- ', bestdoc2vec)
    print('store_indices----------- ', store_indices)
    resultdoc2vec =[]
    # Get the doc2vec labels from indices
    for elem in bestdoc2vec:
        ind = d_indices[elem]
        temp = d2vModel.docvecs.index_to_doctag(ind)
        resultdoc2vec.append((temp,float(avgdoc2vec[elem])))

    # This modifies the average word2vec vector for cases in which the word2vec label was same as topic word.
    for element in store_indices:
        avgword2vec[element] = (avgword2vec[element]*len(topic_list[topic_num]))/(float(len(topic_list[topic_num])-1))
    
    bestword2vec = matutils.argsort(avgword2vec,topn=100, reverse=True) #argsort and get top 100 word2vec label indices
    print('bestword2vec------------------ ', bestword2vec)
    # Get the word2vec labels from indices
    resultword2vec =[]
    for element in bestword2vec:
        ind = w_indices[element]
        temp = w2vModel.wv.index2word[ind]
        resultword2vec.append((temp,float(avgword2vec[element])))
    
    # Get the combined set of both doc2vec labels and word2vec labels
    comb_labels = list(set([i[0] for i in resultdoc2vec]+[i[0] for i in resultword2vec]))
    newlist_doc2vec = []
    newlist_word2vec =[]
    print('comb_labels---------------------- ', comb_labels)
    # Get indices from combined labels 
    for elem in comb_labels:
        try:
            
            newlist_doc2vec.append(d_indices.index(d2vModel.docvecs.doctags[elem].offset))
            newlist_word2vec.append(w_indices.index(w2vModel.wv.vocab[elem].index))
            
        except:
            pass
    newlist_doc2vec = list(set(newlist_doc2vec))
    newlist_word2vec = list(set(newlist_word2vec))
    
    # Finally again get the labels from indices. We searched for the score from both doctvec and word2vec models
    resultlist_doc2vecnew =[(d2vModel.docvecs.index_to_doctag(d_indices[elem]),float(avgdoc2vec[elem])) for elem in newlist_doc2vec]
    resultlist_word2vecnew =[(w2vModel.wv.index2word[w_indices[elem]],float(avgword2vec[elem])) for elem in newlist_word2vec]
    
    # Finally get the combined score with the label. The label used will be of doc2vec not of word2vec. 
    new_score =[]
    for item in resultlist_word2vecnew:
        k,v =item
        for elem in resultlist_doc2vecnew:
            k2,v2 = elem
            v3 = v+v2
            new_score.append((k2,v3))
    new_score = sorted(new_score,key =lambda x:x[1], reverse =True)
    print('new_score---------------- ', new_score)
    return new_score[:(int(num_of_topics))]




def get_phrases_for_ngram(output_file, data, list_labels):
    lines =[]
    for docs in data:
        words = docs.split(" ")
        bigram = ngrams(words,2)
        trigram = ngrams(words,3)
        fourgram = ngrams(words,4)
        
        for item in fourgram:
            temp = ' '.join(item)
            if temp in list_labels:
                temp1 = '_'.join(item)
                docs  = docs.replace(temp,temp1)
                

        for item in trigram:
            temp = ' '.join(item)
            if temp in list_labels:
                temp1 = '_'.join(item)
                docs  = docs.replace(temp,temp1)
        for item in bigram:
            temp = ' '.join(item)
            if temp in list_labels:
                temp1 = '_'.join(item)
                docs  = docs.replace(temp,temp1)            

        lines.append(docs)

    
    with open(output_file,'w', encoding='utf-8') as g:
        g.write('\n'.join(lines))


def convertDataListToCSV(model_name, field_name, data):
    df = pd.DataFrame({
        field_name: data 
    })
    df.to_csv('logs/' + model_name + '_' + field_name + '.csv', index=False)

def readFileAndRunLDAScript(model_name, field_name, num_of_topics):
    fileName = 'logs/' + model_name + '_' + field_name + '.csv'
    if os.path.exists(fileName):
        pathFile = fileName
        print('num_of_topics--------------------- ', num_of_topics)
        excel_file_path = generateTopicFromContent(pathFile, field_name, 0.2, 'utf8', num_of_topics)
        # cmdScript = 'python ./covid_twitter_scripts-master/main.py ' + pathFile + ' ' + field_name + ' 0.2 utf8 ' + str(num_of_topics)
        # print('cmdScript---------------------- ', cmdScript)
        # os.system(cmdScript)
        return excel_file_path


def calculateNumOfTopic(data):
    model = Top2Vec(documents=data, speed="fast-learn", workers=5)
    num_of_topics = model.get_num_topics()
    if num_of_topics > 20:
        num_of_topics = 20
    elif num_of_topics < 3:
        num_of_topics = 3
    return num_of_topics